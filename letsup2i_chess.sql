-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 29, 2020 at 06:49 AM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `letsup2i_chess`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_sent`
--

CREATE TABLE `email_sent` (
  `email_sent_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_sent`
--

INSERT INTO `email_sent` (`email_sent_id`, `email`, `subject`, `message`) VALUES
(1, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(2, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(3, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(4, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(5, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(6, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(7, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(8, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(9, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(10, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(11, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(12, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(13, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(14, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(15, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(16, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(17, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(18, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(19, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(20, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(21, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(22, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(23, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(24, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(25, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(26, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(27, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(28, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(29, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(30, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(31, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(32, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(33, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(34, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(35, 'danish@onevoicetransmedia.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice'),
(36, 'h@gmail.com', 'Registration Successful ForMaharashtra Chess Association', 'invoice');

-- --------------------------------------------------------

--
-- Table structure for table `sms_sent`
--

CREATE TABLE `sms_sent` (
  `sms_sent_id` int(11) NOT NULL,
  `number` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_sent`
--

INSERT INTO `sms_sent` (`sms_sent_id`, `number`, `message`) VALUES
(1, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M20final_registration_entry_no  . Keep Riding! '),
(2, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M204  . Keep Riding! '),
(3, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M204  . Keep Playing! '),
(4, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(5, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(6, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(7, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(8, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(9, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(10, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(11, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(12, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(13, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(14, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(15, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(16, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(17, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(18, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(19, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(20, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(21, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(22, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(23, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(24, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(25, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(26, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(27, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(28, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(29, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(30, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(31, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(32, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(33, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(34, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(35, '9762379774', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M205  . Keep Playing! '),
(36, '8793839969', 'Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M206  . Keep Playing! ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `contact_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`contact_id`, `name`, `email`, `message`, `phone`) VALUES
(1, 'sdsdfdf', 'prajakta@onevoicetransmedia.com', 'asdasd', '9876543210'),
(2, 'sdsdfdf', 'prajakta@onevoicetransmedia.com', 'dasdsad', '9876543210'),
(3, 'sdsdfdf', 'prajakta@onevoicetransmedia.com', 'saxasd', '9876543210'),
(4, 'sdsdfdf', 'prajakta@onevoicetransmedia.com', 'dasdsad', '9876543210'),
(5, 'sdsdfdf', 'prajakta@onevoicetransmedia.com', 'assdsad', '9876543210'),
(6, 'ty', 'h@gmail.com', 'io', 'hhiuiuuiui'),
(7, 'aa', 'h@gmail.com', 'ds', 'edssfdfsfd'),
(8, 'ht', 'g@gmail.com', 'tt', 'ryttrytyyy'),
(9, 'gf', 'g@gmail.com', 'yrtttt', 'fdergrrtyr');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_players`
--

CREATE TABLE `tbl_players` (
  `player_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mno` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `district` varchar(255) NOT NULL,
  `fide_rating` varchar(255) NOT NULL,
  `fide_id_no` varchar(255) NOT NULL,
  `aicf_id_no` varchar(255) NOT NULL,
  `mca_id_no` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `titles` varchar(255) NOT NULL,
  `disciplinary_action` varchar(255) NOT NULL,
  `registration_entry_no` varchar(255) NOT NULL,
  `counter_entry_no` int(11) NOT NULL,
  `membership_date` datetime NOT NULL,
  `txnId` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_players_temp`
--

CREATE TABLE `tbl_players_temp` (
  `player_temp_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mno` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `district` varchar(255) NOT NULL,
  `fide_rating` varchar(255) NOT NULL,
  `fide_id_no` varchar(255) NOT NULL,
  `aicf_id_no` varchar(255) NOT NULL,
  `mca_id_no` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `titles` varchar(255) NOT NULL,
  `disciplinary_action` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_players_temp`
--

INSERT INTO `tbl_players_temp` (`player_temp_id`, `name`, `parent_name`, `address`, `mno`, `email`, `dob`, `district`, `fide_rating`, `fide_id_no`, `aicf_id_no`, `mca_id_no`, `image`, `titles`, `disciplinary_action`, `created_on`) VALUES
(18, 'Danish Patel', 'mushtak', 'Mukundnagar, Ahmednagar', '9762379774', 'danish@onevoicetransmedia.com', '1992-01-14', 'Ahmednagar', '2', '789', '44', '45', '', 'NA', 'no', '2020-05-21 07:31:48'),
(19, 'Danish Patel', 'mushtak', 'Mukundnagar, Ahmednagar', '9762379774', 'danish@onevoicetransmedia.com', '1992-01-14', 'Ahmednagar', '2', '789', '44', '45', '', 'NA', 'no', '2020-05-21 07:34:16'),
(20, 'Danish Patel', 'mushtak', 'Ahmednagar', '9762379774', 'danish@onevoicetransmedia.com', '1992-08-14', 'Ahmednagar', '1', '22', '44', '55', '', 'NA', 'no', '2020-05-21 07:36:44'),
(21, 'dammy patel', 'musa patel', 'Mukundnagar, Ahmednagar', '9762379774', 'danish@onevoicetransmedia.com', '1994-10-12', 'Ahmednagar', '2', '789', '78', '55', '1590056705road-1072823__340.jpg', 'champo', 'NA', '2020-05-21 10:25:05'),
(22, 'sameer', 'kisan shinde', 'Ahmednagar', '9876543210', 'ad@gmaiul.com', '1992-02-14', 'Ahmednagar', '2', '789', '78', '55', '1590124971road-1072823__340.jpg', 'NA', 'NA', '2020-05-22 05:22:51'),
(23, 's', 's', 'sa', '8793235689', 'fdg@gmail.com', '1995-04-10', 'sd', 'ds', 'sd', '23', '12d', '1590133733Desert.jpg', ' c', 'c', '2020-05-22 07:48:53'),
(24, 'Test', 'test1', 'fd', '8793839969', 'h@gmail.com', '1997-04-12', 'Ahmednagar', 'nm', 'b', '65', '565', '1590149078Lighthouse.jpg', 'dsf', 'fs', '2020-05-22 12:04:38'),
(25, 'abc', 'xyz', 'jhkh', '8793562415', 'njh@gmail.com', '1997-02-14', 'bn', 'v', '12', '11', '2121', '1590149383Lighthouse.jpg', '15gh', 'cvbc', '2020-05-22 12:09:43'),
(26, 'dammy', 'mushtak', 'ngr', '7879456200', 'ram@gmail.com', '1992-01-14', 'ngr', '2', '789', '78', '55', '1590218049images.jpg', 'champo', 'no', '2020-05-23 07:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `wwc_admin`
--

CREATE TABLE `wwc_admin` (
  `id` int(11) NOT NULL,
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1=SuperAdmin,2=Admin',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `ip_address` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL,
  `recent_login` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wwc_admin`
--

INSERT INTO `wwc_admin` (`id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `contact`, `profile_pic`, `type`, `status`, `ip_address`, `inserted_on`, `updated_on`, `deleted_on`, `recent_login`) VALUES
(2, 'Admin', NULL, NULL, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@admin.com', '1234567890', NULL, 1, 1, NULL, '0000-00-00 00:00:00', '2020-05-10 13:21:38', '0000-00-00 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email_sent`
--
ALTER TABLE `email_sent`
  ADD PRIMARY KEY (`email_sent_id`);

--
-- Indexes for table `sms_sent`
--
ALTER TABLE `sms_sent`
  ADD PRIMARY KEY (`sms_sent_id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `tbl_players`
--
ALTER TABLE `tbl_players`
  ADD PRIMARY KEY (`player_id`);

--
-- Indexes for table `tbl_players_temp`
--
ALTER TABLE `tbl_players_temp`
  ADD PRIMARY KEY (`player_temp_id`);

--
-- Indexes for table `wwc_admin`
--
ALTER TABLE `wwc_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email_sent`
--
ALTER TABLE `email_sent`
  MODIFY `email_sent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `sms_sent`
--
ALTER TABLE `sms_sent`
  MODIFY `sms_sent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_players`
--
ALTER TABLE `tbl_players`
  MODIFY `player_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `tbl_players_temp`
--
ALTER TABLE `tbl_players_temp`
  MODIFY `player_temp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `wwc_admin`
--
ALTER TABLE `wwc_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
