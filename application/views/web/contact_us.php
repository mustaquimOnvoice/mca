<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- Contact Us</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="Maharashtra Chess Association- Contact Us" />
    <meta name="keywords" content="Maharashtra Chess Association- Contact Us" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/hockey/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2> contact us</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>contact us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
    <!-- contact_icon_section start-->
    <div class="contact_icon_section float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="main rotate_blue">
                        <div class="rotate">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <h4><a href="#">Address</a></h4>
                        <p>Regd. Office address – Pratik, <br>S. No. 32/1, Plot No. 11, <br>Behind Mehendale Garage, <br>Erandawane, Karve Road, Pune – 411004 </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="main rotate_purple">
                        <div class="rotate">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <h4><a href="#">Email</a></h4>
                        <p><a href="#">maharashtrachessassociation@gmail.com </a></p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="main rotate_green">
                        <div class="rotate">
                            <i class="fa fa-phone"></i>
                        </div>
                        <h4><a href="#">Phone</a></h4>
                        <p>8007944674 <br>(Rohit Tembe, Admin office),<br> 9673003780<br> (Niranjan Godbole, Secretary)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
	 <!-- contact_wrapper start -->
    <div class="contact_wrapper float_left inner_contact_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="ft_left_heading_wraper">
                        <h1>get in touch</h1>

                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="row">
                        <form method="post" action="<?php echo base_url();?>contact_us">
                            <div class="col-md-12">
                                <div class="form-pos">
                                    <div class="form-group i-name">
                                        <label class="sr-only">Name</label>
                                        <input type="text" class="form-control require" name="full_name" required pattern="[A-Za-z]+" id="namTen-first" placeholder="Your Name*">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-12 -->
                            <div class="col-md-12">
                                <div class="form-e">
                                    <div class="form-group i-email">
                                        <label class="sr-only">Email </label>
                                        <input type="email" class="form-control require" name="email" required="" id="emailTen" placeholder="Your Email *" data-valid="email" data-error="Email should be valid.">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-12 -->
                            <div class="col-md-12">
                                <div class="form-s">
                                    <div class="form-group i-subject">
                                        <label class="sr-only">phone</label>
                                        <input type="number" max="10" class="form-control" minlength="10" maxlength="10" name="phone" required="" id="phone" placeholder="Your Phone">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-12 -->
                            <div class="col-md-12">
                                <div class="form-m">
                                    <div class="form-group i-message">
                                        <label class="sr-only">Your Message </label>
                                        <textarea class="form-control require" name="message" required="" rows="4" id="messageTen" placeholder="Your Message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-12 -->
                            <div class="col-md-12">
                                <div class="tb_es_btn_div">
                                    <div class="response"></div>
                                    <div class="tb_es_btn_wrapper">
                                        <button type="submit" name="btn_contact" id="btn_contact"  class="submitForm">send now</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="contact_right_wrapper float_left iner_contact_right_wrapper">
                        <img src="<?php echo base_url();?>images/inner/9.jpg" alt="img">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- contact_wrapper end -->

    <!-- contact_icon_section end-->
     <div class="map_wrapper float_left">
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3783.5144666451993!2d73.82869761489258!3d18.505639487417707!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTjCsDMwJzIwLjMiTiA3M8KwNDknNTEuMiJF!5e0!3m2!1sen!2sin!4v1589361992782!5m2!1sen!2sin" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>

    <!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <!-- <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- prs patner slider End -->

    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>