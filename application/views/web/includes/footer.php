<div class="footer_wrapper float_left">
        <!--newsletter wrapper start-->

        <!-- <div class="news_section">
            <div class="container">

                <div class="news_letter_wrapper float_left">

                    <div class="lr_nl_heading_wrapper">
                        <h2>Registration</h2>
                        <p>Players/ Arbiter/ Academy/ District and state level tournament registration.</p>
                    </div>

                    <div class="lr_nl_form_wrapper">
                        <input type="hidden" placeholder="Enter Your Email">
                        <button style="float: right;" type="submit"> Click here</button>
                    </div>

                </div>
            </div>
        </div> -->

        <!--newsletter wrapper end-->
        <!-- section-2 start-->
        <div class="section_2">
            <div class="section2_footer_overlay"></div>
            <div class="section2_footer_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-xs-12 col-sm-6">
                            <div class="footer_widget section2_about_wrapper">
                                <div class="wrapper_first_image">
                                    <a href="<?php base_url()?>"><img src="<?php echo base_url();?>images/chess/logo.png" class="img-responsive" alt="logo" /></a>
                                </div>
                                <div class="abotus_content">
                                    <p>The Maharashtra Chess Association came into existence in the year 1963 after the Bombay Chess Association and Southern Maratha Country Chess Association decided to merge and formed a unified Maharashtra Chess Association.</p>
                                    <p><a href="<?php echo base_url('web/about_us')?>"><button style="background-color: #313393" class="btn btn-sm btn-primary">READ MORE</button></a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-xs-12 col-sm-6">
                            <div class="footer_widget section2_useful_second_wrapper">
                                <h4>contact <span> info </span></h4>
                                <ul>
                                    <li><i class="fa fa-location-arrow"></i>Regd. Office address – Pratik, S. No. 32/1, Plot No. 11, Behind Mehendale Garage, Erandawane, Karve Road, Pune – 411004 
                                    </li>
                                    <!-- <li><i class="fa fa-flag"></i> ABN 11 119 159 741
                                    </li> -->
                                    <li><i class="fa fa-phone-square"></i>Admin office: 8007944674</li>
                                    <li><i class="fa fa-phone-square"></i>Secretary: 9673003780</li>
                                    <li><a href="#"><i class="fa fa-envelope-square"></i>maharashtrachessassociation<br>@gmail.com</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-xs-12 col-sm-6">
                            <div class="footer_widget section2_blog_wrapper">
                                <h4>Twitter</h4>
                                <a class="twitter-timeline" data-width="250" data-height="300" data-theme="light" href="https://twitter.com/MahaChess?ref_src=twsrc%5Etfw">Tweets by MahaChess</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-xs-12 col-sm-6">
                            <div class="footer_widget section2_blog_wrapper">
                                <h4>Facebook</h4>
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FMAHChessAsso%2F&tabs=timeline&width=340&height=400&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="auto" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                        <!-- <div class="col-lg-2 col-md-6 col-xs-12 col-sm-6">
                            <div class="footer_widget section2_useful_wrapper">
                                <h4>useful links </h4>
                                <ul>
                                    <li><a href="<?php echo site_url('web/about_us');?>"><i class="fa fa-angle-right"></i>About</a>
                                    </li>
                                    <li><a href="<?php echo site_url('web/events');?>"><i class="fa fa-angle-right"></i>Events</a>
                                    </li>
                                    <li><a href="<?php echo site_url('web/cis');?>"><i class="fa fa-angle-right"></i>CIS Map</a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Sponsors / Donors</a>
                                    </li>
                                    <li><a href="<?php echo site_url('web/contact_us');?>"><i class="fa fa-angle-right"></i>Contact</a></li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="section2_bottom_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="btm_foter_box">

                                <p><i class="fa fa-copyright"></i> <?=date('Y')?> Maharashtra Chess Association. Developed by <a href="http://onevoicetransmedia.com/"> OneVoice Transmedia</a></p>
                                <ul class="aboutus_social_icons">
                                    <li><a href="https://www.facebook.com/MAHChessAsso/" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="https://twitter.com/MahaChess" target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <!-- <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                                    </li>
                                    <li> <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i> </a> </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="close_wrapper">
        </div>
        <!-- section-2 end -->
    </div>
