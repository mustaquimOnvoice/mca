<div class="ft_navi_main_wrapper float_left">

    <div class="ft_menu_wrapper">

        <!-- mobile menu area start -->
        <div class="rp_mobail_menu_main_wrapper">
            <div class="row">
                <div class=" col-sm-12 col-12">
                    <div id="toggle">
                        <a href="#"><i class="fa fa-bars"></i><span>menu</span></a>
                    </div>
                </div>
            </div>

        </div>
        <!-- <div class="ft_login_wrapper">
            <a href="login.html"><i class="fa fa-sign-in"></i>
            <span>login/register</span></a>
        </div> -->
    </div>

    <div class="ft_logo_wrapper">
        <a href="<?php echo base_url()?>">
            <img width="60%" src="<?php echo base_url()?>images/chess/logo.png" alt="logo"><p style="color:black;">Affiliated to AICF</p>
        </a>
    </div>
    <div class="ft_right_wrapper">
        <ul>
            <li>
                <div class="hs_btn_wrapper d-none d-sm-none d-md-block d-lg-block d-xl-block animated bounce">
                    <ul>
                        <li><a href="<?php echo site_url('web/contact_us');?>">contact</a></li>
                    </ul>
                </div>
            </li>
            <li>                
                <!-- extra nav -->
                <!-- <div class="extra-nav">
                    <div class="extra-cell">
                        <button id="quik-search-btn" type="button" class="site-button radius-xl"><i class="flaticon-search"></i></button>
                    </div>
                </div> -->

                <!-- Quik search -->
                <!-- <div class="dez-quik-search bg-primary-dark">
                    <form action="#">
                        <input name="search" value="" type="text" class="form-control" placeholder="Type to search...">
                        <span id="quik-search-remove"><i class="fa fa-remove"></i></span>
                    </form>
                </div> -->
            </li>
        </ul>
    </div>
</div>