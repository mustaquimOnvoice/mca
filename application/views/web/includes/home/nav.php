    <div class="ft_navi_main_wrapper float_left">

        <div class="ft_logo_wrapper">
            <!-- <div id="search_open" class="lv_search_box float_left d-none d-sm-none d-md-none d-lg-block d-xl-block">
                <input type="text" placeholder="Search here...">
                <button><i class="flaticon-magnifier"></i></button>
            </div> -->
            <div class="resp_logo_wrapper d-block d-sm-block d-md-block d-lg-none d-xl-none">
                <a href="<?php echo base_url();?>">
                    <img src="images/chess/logo.png" alt="logo"> <p style="color:white;">Affiliated to AICF</p>
                </a>
                <div class="resp_menu">

                    <div class="menu_toggle rotate" id="menu_toggle_icon">
                        <i class="fa fa-angle-down"></i>
                    </div>
                    <div id="menu_open" class="menu_dropdown">

                        <ul class="menu_wrapper menu_wrapper_2 menu_wrapper_resp">

                            <li><a href="<?php echo base_url();?>" title="">Home</a></li>                   
                            <li><a href="<?php echo site_url('web/about_us');?>" title="about">About MCA</a></li>
                               <li><a href="<?php echo site_url('registration');?>" title="Player Registrations">Player Registrations</a></li>
                            <li><a href="<?php echo site_url('web/committee');?>" title="Office Bearers">Office Bearers</a></li>
                            <li class="common_dropdown_wrapper float_left">
                                <a href="#" title="MCAs initiatives">MCAs initiatives<i class="fa fa-angle-right"></i></a>
                                <ul>
                                    <!-- <li><a href="#">Get your school registered</a></li>
                                    <li><a href="#">Monitor your school</a></li> -->
                                    <li><a href="<?php echo site_url('web/cis');?>">Chess In School Map</a></li>
                                    <li><a href="<?php echo site_url('web/train_the_trainers');?>">Train The Trainers</a></li>
                                    <li><a href="<?php echo site_url('web/champions');?>">Honoring Our Champions</a></li>
                                    <!-- <li><a href="<?php // echo site_url('web/cis_success_stories');?>">CIS Success Stories </a></li> -->
                                    <li><a href="<?php echo site_url('web/chess_league');?>" title="Chess League">Chess League</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo site_url('web/international_chess_calendar');?>" title="International Chess Calendar">International Chess Calendar</a></li>
                            <li><a href="<?php echo site_url('web/wjcc');?>" title="WJCC 2014">WJCC 2014</a></li>
                            <!-- <li class="common_dropdown_wrapper float_left">
                                <a href="#" title="Events">Events<i class="fa fa-angle-right"></i></a>
                                <ul>
                                    <li><a href="<?php // echo site_url('web/events');?>">Indian Chess Tournament Calendar</a></li>
                                    <li><a href="<?php // echo site_url('web/events');?>">World Wide Chess Calendar</a></li>								
                                </ul>
                            </li>  -->
                            <!-- <li class="common_dropdown_wrapper float_left">
                                <a href="#" title="Train the Trainers Program">Train the Trainers Program <i class="fa fa-angle-right"></i></a>
                                <ul>
                                    <li><a href="#">Register for the program</a></li>
                                    <li><a href="#">Online tutorials (Link)</a></li>								
                                    <li><a href="#">Payment link</a></li>								
                                    <li><a href="#">Share your experience</a></li>								
                                </ul>
                            </li> 
                            <li class="common_dropdown_wrapper float_left">
                                <a href="#" title="Contact">Others<i class="fa fa-angle-right"></i></a>
                                <ul>
                                    <li><a href="#" title="News and Media Corner">News and Media Corner</a></li>                        			
                                    <li><a href="#" title="List of affiliated members">List of affiliated members</a></li>
                                    <li><a href="#" title="Chess Center of Excellence">Chess Center of Excellence</a></li>
                                    <li><a href="#" title="Chess Blog">Chess Blog</a></li>
                                    <li><a href="#" title="Calendar">Calendar</a></li>                                        			
                                </ul>
                            </li> -->
                            <!-- <li class="common_dropdown_wrapper float_left">
                                <a href="#" title="Registration">Registration<i class="fa fa-angle-right"></i></a>
                                <ul>
                                    <li><a href="<?php // echo site_url('registration')?>">Players</a></li>							
                                    <li><a href="#">Arbiter</a></li>
                                    <li><a href="#" title="Download players’ ID card">Download players’ ID card</a></li>					
                                    <li><a href="#">Academy</a></li>
                                    <li><a href="#">State level tournament</a></li>							
                                </ul>
                            </li> -->
                            <!-- <li class="common_dropdown_wrapper float_left">
                                <a href="#" title="Contact">Contact<i class="fa fa-angle-right"></i></a>
                                <ul>
                                    <li><a href="<?php //echo site_url('web/contact_us');?>" title="Contact Us">Contact Us</a></li>							
                                    <li><a href="<?php //echo site_url('web/committee');?>" title="Office Bearers- Contact Details">Office Bearers- Contact Details</a></li>							
                                </ul>
                            </li>           -->
                            <li><a href="<?php echo site_url('web/sponsors');?>" title="Our Supporters / Donor">Our Supporters</a></li>                  
                            <li><a href="<?php echo site_url('web/press_media');?>" title="Media">Media</a></li> 
                            <li><a href="<?php echo site_url('web/sponsor_a_tournament');?>" title="Sponsor a tournament">Sponsor a tournament</a></li>                 
                            <li><a href="<?php echo site_url('web/jewels');?>" title="Our Jewels">Our Jewels</a></li>          
                            <li><a href="<?php echo site_url('web/contact_us');?>" title="Contact Us">Contact Us</a></li>       
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="ft_middle_wrapper d-none d-sm-none d-md-none d-lg-block d-xl-block  animate__animated animate__fadeInDown animate__delay-3s animate__repeat-1">
            <a href="<?php echo base_url();?>">
                <img width="50%" src="images/chess/logo.png" alt="logo"><p style="color:white;">Affiliated to AICF</p>
            </a>
        </div>
        <div class="ft_right_wrapper">
            <ul>
                <!-- <li class="language d-none d-sm-block d-md-block d-lg-block d-xl-block">
                    <select class="myselect">
                        <option>EN</option>
                        <option>RO</option>
                        <option>IT</option>
                    </select> <img src="images/hockey/icon.png" alt="logo">
                </li>
                <li>
                    <div class="hs_navi_cart_wrapper d-none d-sm-block d-md-block d-lg-block d-xl-block">
                        <div class="dropdown-wrapper menu-button"> <a class="menu-button" href="#"><i class="flaticon-shopping-cart"></i><span>3</span></a>
                            <div class="drop-menu">
                                <div class="cc_cart_wrapper1 menu-button">
                                    <div class="cc_cart_img_wrapper">
                                        <img src="images/hockey/ft1.jpg" alt="cart_img" />
                                    </div>
                                    <div class="cc_cart_cont_wrapper">
                                        <h4><a href="#">product name</a></h4>
                                        <p>Quantity : 2 × $45</p>
                                        <h5>$90</h5>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                </div>

                                <div class="cc_cart_wrapper1 menu-button">
                                    <div class="cc_cart_img_wrapper">
                                        <img src="images/hockey/ft2.jpg" alt="cart_img" />
                                    </div>
                                    <div class="cc_cart_cont_wrapper">
                                        <h4><a href="#">product name</a></h4>
                                        <p>Quantity : 2 × $45</p>
                                        <h5>$90</h5>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                </div>
                                <div class="cc_cart_wrapper1 menu-button">
                                    <div class="cc_cart_img_wrapper">
                                        <img src="images/hockey/ft3.jpg" alt="cart_img" />
                                    </div>
                                    <div class="cc_cart_cont_wrapper">
                                        <h4><a href="#">product name</a></h4>
                                        <p>Quantity : 2 × $45</p>
                                        <h5>$90</h5>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                </div>
                                <div class="cc_cart_wrapper1 menu-button">
                                    <div class="cart_wrapper">
                                        <h2>total <br>$180.00</h2>
                                        <div class="hs_btn_wrapper cart_btn">
                                            <a href="#" class="hocky_btn ckeck_btn">
                                                <div class="btn-front">checkout</div>
                                                <div class="btn-back">checkout</div>

                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="hs_btn_wrapper spp_btn_wrapper">
                        <ul>
                            <li>
                                <a href="<?php //echo site_url('web/international_chess_calendar');?>" class="hocky_btn">
                                    <div class="btn-front">Upcomming Events</div>
                                    <div class="btn-back">Upcomming Events</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li> -->
                <li class="animated bounce">
                    <a class="btn btn-md btn-primary animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" href="<?php echo site_url('web/international_chess_calendar');?>">Upcomming Events</a>
                </li>
            </ul>
        </div>
    </div>