<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-166962900-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-166962900-1');
</script>
<style>
    .animated {
        -webkit-animation-duration: .5s;
        animation-duration: .5s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-timing-function: linear;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        -webkit-animation-iteration-count: infinite;
    }
    @-webkit-keyframes bounce {
        0%, 100% {
            -webkit-transform: translateY(0);
        }
        70% {
            -webkit-transform: translateY(-5px);
        }
    }
    @keyframes bounce {
        0%, 100% {
            transform: translateY(0);
        }
        70% {
            transform: translateY(-5px);
        }
    }
    .bounce {
        -webkit-animation-name: bounce;
        animation-name: bounce;
    }
    
</style>
