<div id="preloader">
    <div id="status">
        <img src="<?php echo base_url()?>images/chess/loader.gif" id="preloader_image" alt="loader">
    </div>
</div>

<div class="cursor cursor-shadow"></div>
<div class="cursor cursor-dot"></div>

<!-- Top Scroll Start -->
<a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>
<!-- Top Scroll End -->