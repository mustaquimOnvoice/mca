<div id="sidebar" class="bounce-to-right">

    <div id="toggle_close">&times;</div>
    <div id='cssmenu'>
        <a href="<?php echo base_url()?>"><img src="<?php echo base_url()?>images/chess/logo.png" alt="logo"></a>
        <ul class="sidebb">
            <li><a href='<?php echo base_url()?>'>Home</a></li>
            <li><a href="<?php echo site_url('web/about_us');?>" title="about">About MCA</a></li>
               <li><a href="<?php echo site_url('registration');?>" title="Player Registrations">Player Registrations</a></li>
            <li><a href="<?php echo site_url('web/committee');?>" title="Office Bearers">Office Bearers</a></li>
            <li class='has-sub'><a href='#'>MCAs initiatives</a>
                <ul>
                    <!-- <li><a href="#">Get your school registered</a></li>
                    <li><a href="#">Monitor your school</a></li> -->
                    <li><a href="<?php echo site_url('web/cis');?>">Chess In School Map</a></li>
                    <li><a href="<?php echo site_url('web/train_the_trainers');?>">Train The Trainers</a></li>
                    <li><a href="<?php echo site_url('web/champions');?>">Honoring Our Champions</a></li>
                    <!-- <li><a href="<?php // echo site_url('web/cis_success_stories');?>">CIS Success Stories </a></li> -->
                    <li><a href="<?php echo site_url('web/chess_league');?>" title="Chess League">Chess League</a></li>
                </ul>
            </li>
            <li><a href="<?php echo site_url('web/international_chess_calendar');?>" title="International Chess Calendar">International Chess Calendar</a></li>
            <li><a href="<?php echo site_url('web/wjcc');?>" title="WJCC 2014">WJCC 2014</a></li>
            <!-- <li class='has-sub'><a href='#'>Events</a>
                <ul>
                    <li><a href="<?php // echo site_url('web/events');?>">Indian Chess Tournament Calendar</a></li>
                    <li><a href="<?php // echo site_url('web/events');?>">World Wide Chess Calendar</a></li>
                </ul>
            </li> -->
            <!-- <li class='has-sub'><a href='#'>Train the Trainers Program</a>
                <ul>
                    <li><a href="#">Register for the program</a></li>
                    <li><a href="#">Online tutorials (Link)</a></li>								
                    <li><a href="#">Payment link</a></li>								
                    <li><a href="#">Share your experience</a></li>
                </ul>
            </li>
            <li class='has-sub'><a href='#'>Others</a>
                <ul>
                    <li><a href="#" title="News and Media Corner">News and Media Corner</a></li>  
                    <li><a href="#" title="List of affiliated members">List of affiliated members</a></li>
                    <li><a href="#" title="Chess Center of Excellence">Chess Center of Excellence</a></li>
                </ul>
            </li> -->
            <!-- <li class='has-sub'><a href='#'>Registration</a>
                <ul>
                    <li><a href="<?php // echo site_url('registration')?>">Players</a></li>							
                    <li><a href="#">Arbiter</a></li>                      			
                    <li><a href="#" title="Download players’ ID card">Download players’ ID card</a></li>							
                    <li><a href="#">Academy</a></li>							
                    <li><a href="#">State level tournament</a></li>
                </ul>
            </li> -->
            <!-- <li class='has-sub'><a href='#'>Contact</a>
                <ul>
                    <li><a href="<?php // echo site_url('web/contact_us');?>" title="Contact Us">Contact Us</a></li>							
                    <li><a href="<?php // echo site_url('web/committee');?>" title="Office Bearers- Contact Details">Office Bearers- Contact Details</a></li>
                </ul>
            </li> -->
            <!-- <li><a href="#" title="Chess Blog">Chess Blog</a></li> -->
            <!-- <li><a href="#" title="Calendar">Calendar</a></li>     -->
            <li><a href="<?php echo site_url('web/sponsors');?>" title="Our Supporters / Donor">Our Supporters</a></li> 
            <li><a href="<?php echo site_url('web/press_media');?>" title="Media">Media</a></li>
            <li><a href="<?php echo site_url('web/sponsor_a_tournament');?>" title="Sponsor a tournament">Sponsor a tournament</a></li>
            <li><a href="<?php echo site_url('web/jewels');?>" title="Our Jewels">Our Jewels</a></li>
            <li><a href="<?php echo site_url('web/contact_us');?>" title="Contact Us">Contact Us</a></li>
        </ul>
    </div>
    <div class="btm_foter_box sidebar_btm_txt">
        <ul class="aboutus_social_icons">
            <li><a href="https://www.facebook.com/MAHChessAsso/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/MahaChess" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <!-- <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            </li>
            <li> <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i> </a> </li> -->
        </ul>
        <!-- <p><i class="fa fa-copyright"></i> y <a href="#"> webstrot .</a></p> -->
    </div>
</div>