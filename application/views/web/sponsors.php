<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- Sponsors</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/hockey/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
	<?php require('includes/head.php');?>
	<style>
		.iner_committee_wrapper{
			padding-bottom: 0px;
			background: #fff;
			text-align: center;
		}
	</style>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2>Supporters</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>Our Supporters</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
   <!-- inner welcome Wrapper Start -->
    <div class="iner_committee_wrapper float_left">
        <div class="container">
            <div class="row team-section text-center my-5">
				<h2 class="h1-responsive font-weight-bold my-5">Our Supporters</h2>
					<div class="row animate__animated animate__fadeInLeft animate__delay-1s animate__repeat-1">
						<div class="col-lg-6 col-md-6 mb-lg-0 mb-5">
							<div class="avatar mx-auto" style="margin-top: 20%;">
								<img width="50%" src="<?php echo base_url();?>images/inner/sponsors/amanora.jpg" class="rounded-circle z-depth-1"  alt="Sample avatar">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 mb-lg-0 mb-5">
							<p class="grey-text" style="text-align:justify;">Since its inception in 1996, the communities developed by the company founder and Managing Director Aniruddha Deshpande have consistently outpaced the expectations of
							the marketplace and achieved the goals of the company&#39;s investors. He is also the Vice President of MCA.</p>
							<p class="grey-text" style="text-align:justify;">Over the past 10 years, City Group has built a reputation for impeccable design detail and
							skilled construction practices. The company is known and well respected for its
							perseverance to produce a product that surpasses the needs of clients and provides
							effective long-term investment value.</p>
							<p class="grey-text" style="text-align:justify;">Ten years ago, Amanora Park Town was conceived as India’s 1st fully developed integrated
							township.</p>
							<p class="grey-text" style="text-align:justify;">It was also the first Smart City of its kind in the country:<br>
							1. State-of-the-art infrastructure came together with the latest technology – from
							smart access cards to sustainable electricity and social infrastructure.<br>
							2. International architects brought in iconic facades and smart design concepts.<br>
							3. From a fire-station to a school and hospital, Amanora introduced the idea of living
							away from everything you need.</p>

						</div>
					</div>
					<hr style="margin-top: 2rem;  margin-bottom: 2rem;  border: 0;  border-top: 1px solid rgba(0, 0, 0, 0.1);">
					<div class="row animate__animated animate__fadeInRight animate__delay-2s animate__repeat-1">
						<div class="col-lg-6 col-md-6 mb-lg-0 mb-5">
							<p class="grey-text" style="text-align:justify;">Chitale Dairy was founded by the great visionary, Late Shri Bhaskar Ganesh alias Babasaheb
							Chitale (B. G. Chitale). A journey that began in 1939, in the small town of Bhilawadi set the
							stage for a revolution in the country&#39;s dairy industry.</p>
							<p class="grey-text" style="text-align:justify;">Today, the company has one of the most technologically advanced dairy plants in the
							country, with a processing capacity of 3 lakh litres of milk per day. Every day,about 3 lakh
							litres of milk is standardized, pasteurized, packaged and dispatched for sale in Pune and
							other cities. Apart from Milk, Butter, Ghee, Skimmed Milk Powder, Khoa, Chakka, Paneer,
							Processed Cheese, Shrikhand, Lassi and casein form part of a formidable line-up of Chitale
							Dairy products.</p>
							<p class="grey-text" style="text-align:justify;">Over the last half century, the enterprise has distributed about 13,881 buffaloes to farmers
							across Maharashtra and granted subsidy payments exceeding Rs.30 million. This initiative
							has provided employment to large unemployed sections of the district and also enabled
							them to earn supplementary income.</p>
							<p class="grey-text" style="text-align:justify;">He is also the Vice President of MCA.</p>
						</div>
						<div class="col-lg-6 col-md-6 mb-lg-0 mb-5">
							<div class="avatar mx-auto" style="margin-top: 10%;">
							<img width="50%" src="<?php echo base_url();?>images/inner/sponsors/chitale.jpg" class="rounded-circle z-depth-1"  alt="Sample avatar">
							</div>
						</div>
					</div>
					<hr style="margin-top: 2rem;  margin-bottom: 2rem;  border: 0;  border-top: 1px solid rgba(0, 0, 0, 0.1);">
					<div class="row animate__animated animate__fadeInLeft animate__delay-3s animate__repeat-1">
						<div class="col-lg-6 col-md-6 mb-lg-0 mb-5">
							<div class="avatar mx-auto" style="margin-top: 5%;">
							<img width="50%" src="<?php echo base_url();?>images/inner/sponsors/narendra.png" class="rounded-circle z-depth-1"  alt="Sample avatar">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 mb-lg-0 mb-5">
							<p class="grey-text" style="text-align:justify;">Telecom Tycoon, Real Estate Wizard, Impregnable Leader, and Innate Social Worker,
							Narendra Firodia has earned many sobriquets since his birth in Ahmednagar to the
							illustrious Firodia clan.</p>
							<p class="grey-text" style="text-align:justify;">His ancestors were staunch Gandhians, social activists, well-known industrialists, and
							Members of Parliament. Taking inspiration from his predecessors, Mr. Firodia has achieved
							great success in various sectors like film, telecommunication, hospitality, and
							manufacturing, through discipline and perseverance.</p>
							<p class="grey-text" style="text-align:justify;">He is also the Vice President of MCA.</p>
						</div>
					</div>
					<hr style="margin-top: 2rem;  margin-bottom: 2rem;  border: 0;  border-top: 1px solid rgba(0, 0, 0, 0.1);">
					<div class="row animate__animated animate__fadeInRight animate__delay-4s animate__repeat-1">
						<div class="col-lg-6 col-md-6 mb-lg-0 mb-5">
							<p class="grey-text" style="text-align:justify;">Shri Ashok Bhau Jain is the former President of the MCA. The MCA achieved different levels
							of success under his leadership. Under his guidance the MCA started the Maharashtra Chess
							League, the only franchise based team event in chess and organized the World Junior Chess
							Championships in 2014 in Pune which is the second biggest event in chess. We also started
							with the Chess in School initiative under his presidency.

							</p>
							<p class="grey-text" style="text-align:justify;">Shri Ashok Bhau Jain is the Executive Chairman of the Company. He holds a graduate degree
							in commerce from Pune University, India. He joined the JISL management team in 1982 and
							was in charge of marketing and extension services in Maharashtra and other States. In 1993
							he became Director and was responsible for Corporate Administration, Corporate Image
							and Relationships, Events Management, Personnel/Human Resource Development,
							Communication, Public Relations, Art and Publicity. At present he also acts as director in
							charge of the Solar and TC Business.</p>
							<p class="grey-text" style="text-align:justify;">His company, Jain Irrigation Systems Ltd. has sponsored many events for the MCA. Jain
							Irrigation Systems Ltd is a diversified entity with turnover in excess of one billion dollars. It
							has a global presence with 33 manufacturing bases spread over four continents. Its products
							are supplied to 126+ countries with able assistance from 11,000+ dealers and distributors
							worldwide and have reached over 8.5 million farmers.</p>
						</div>
						<div class="col-lg-6 col-md-6 mb-lg-0 mb-5">
							<div class="avatar mx-auto" style="margin-top: 25%;">
							<img width="50%" src="<?php echo base_url();?>images/inner/sponsors/jain.jpg" class="rounded-circle z-depth-1"  alt="Sample avatar">
							</div>
						</div>						
					</div>
					<!-- Grid row -->
			</div>
        </div>
    </div>
    <!-- inner welcome Wrapper end -->
    
	<!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <!-- <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php // echo base_url();?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- prs patner slider End -->

    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>