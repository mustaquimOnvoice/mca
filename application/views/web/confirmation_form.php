<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>MCA- Player Registration</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="sport,sporteyz" />
    <meta name="keywords" content="sport,sporteyz" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/inner/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2> Confirmation Player Registration</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url()?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li> Confirmation Player Registration</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
    <!-- latest result wrapper start-->
    <!-- <div class="latest_result_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <h1>latest result</h1>

                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="upcoming_matches_wrapper float_left">
                        <div class="row">

                            <div class="col-md-3 col-sm-12 col-12">
                                <div class="match_list_wrapper as">
                                    <div class="match_list_img">
                                        <img src="<?php echo base_url()?>images/inner/team1.png" class="img-responsive" alt="logo">
                                        <h4>india</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="latest_match_box float_left">
                                    <p>Asian Cup 2018</p>
                                    <h1>INDIA VS pakistan, FINAL</h1>
                                    <h2>Dubai International Cricket Stadium, Dubai</h2>
                                    <h3>223/7 (50.0) - 222 (48.3)</h3>
                                    <h4>INDIA BEAT pakistan BY 3 WICKETS</h4>
                                </div>

                            </div>
                            <div class="col-md-3 col-sm-12 col-12">
                                <div class="match_list_wrapper match_wraper_2">
                                    <div class="match_list_img">
                                        <img src="<?php echo base_url()?>images/inner/team2.png" class="img-responsive" alt="logo">
                                        <h4>pakistan</h4>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> -->
    <!-- latest result wrapper end-->

    <!--register area start here-->
    <div class="login_section float_left">
      <div class="login_back_img register_back"></div>
        <div class="login_form_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="ft_left_heading_wraper gallery_heading_center text-center login_head">
                            <h1>Confirmation Player Registration</h1>
                             <p></p> 

                        </div>
                    </div>
                </div>
                <div class="col-md-8 offset-md-2 col-sm-12">
                 
                    <div class="login_wrapper">


                            <div class="jp_regiter_top_heading">
                               <!--  <p>Fields with * are mandetory </p> -->
                                <p class="all_err text-center" style="display:none;color:red;"><b>* Marked Fields Are Mandatory!</b></p>
           
                            </div>
                             <div class="row clearfix">
                                
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" id="name" value="<?php echo $records['name'];?>" placeholder="Name* " readonly="readonly">
                                     <p class="name_err" style="display:none;color:red;"><b>Name readonly..!</b></p>
                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                   <label>Son/Daughter Of</label>
                                    <input type="text" class="form-control" name="parent_name" id="parent_name" value="<?php echo $records['parent_name'];?>" placeholder="Son/Daughter Of* " readonly="readonly">
                                     <p class="parent_name_err" style="display:none;color:red;"><b>Son/Daughter Of readonly..!</b></p>
                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-12 col-sm-12 col-12">
                                       <label>Address</label>
                                    <input type="text" class="form-control" name="address" id="address" value="<?php echo $records['address'];?>" placeholder="Address*" readonly="readonly">
                                     <p class="address_err" style="display:none;color:red;"><b>Address readonly..!</b></p>
                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                       <label>Mobile</label>
                                    <input type="number" class="form-control" name="mno" id="mno" value="<?php echo $records['mno'];?>" placeholder="Mobile Number*" readonly="readonly">
                                    <p class="mno_err" style="display:none;color:red;"><b>Mobile Number readonly..!</b></p>
                                   <p class="mno_lengtherr" style="display:none;color:red;"><b>Enter A Valid Mobile Number..!</b></p>
                                   <p class="validmno_err" style="display:none;color:red;"><b>Mobile Number Already Exist..!</b></p>
                                </div>

                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                       <label>Email</label>
                                    <input type="text" class="form-control" name="email" id="email" value="<?php echo $records['email'];?>" placeholder="Email* " readonly="readonly">
                                    <p class="email_err" style="display:none;color:red;"><b>Email readonly..!</b></p>
                                    <p class="invalidemail_err" style="display:none;color:red;"><b>Valid Email readonly..!</b></p>
                                   <p class="validemail_err" style="display:none;color:red;"><b>Email Already Exist..!</b></p>
                                </div>

                              

                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                       <label>Birthdate</label>
                                    <input type="date" class="form-control" name="dob" id="dob" value="<?php echo $records['dob'];?>" placeholder="Birthdate" readonly="readonly">
                                      <p class="dob_err" style="display:none;color:red;"><b>Date of Birth readonly..!</b></p>

                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                       <label>District</label>
                                    <input type="text" class="form-control" name="district"  id="district" value="<?php echo $records['district'];?>" placeholder="District* " readonly="readonly">
                                      <p class="district_err" style="display:none;color:red;"><b>District readonly..!</b></p>
                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                       <label>FIDE Rating</label>
                                    <input type="text" class="form-control" name="fide_rating" id="fide_rating" value="<?php echo $records['fide_rating'];?>" placeholder="FIDE Rating* " readonly="readonly">
                                   
                                      <p class="fide_rating_err" style="display:none;color:red;"><b>FIDE Rating readonly..!</b></p>
                                </div>

                                 <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                       <label>FIDE ID NO</label>
                                    <input type="text" class="form-control" name="fide_id_no" id="fide_id_no" value="<?php echo $records['fide_id_no'];?>" placeholder="FIDE ID NO* " readonly="readonly">
                                     <p class="fide_id_no_err" style="display:none;color:red;"><b>FIDE ID Number readonly..!</b></p>
                                </div>

                                  <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                       <label>AICF ID NO</label>
                                    <input type="text" class="form-control" name="aicf_id_no" id="aicf_id_no" value="<?php echo $records['aicf_id_no'];?>" placeholder="AICF ID NO* " readonly="readonly">
                                     <p class="aicf_id_no_err" style="display:none;color:red;"><b>AICF ID Number readonly..!</b></p>
                                </div>

                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                       <label>MCA ID NO</label>
                                    <input type="text" class="form-control" name="mca_id_no" id="mca_id_no" value="<?php echo $records['mca_id_no'];?>" placeholder="MCA ID NO* " readonly="readonly">
                                     <p class="mca_id_no_err" style="display:none;color:red;"><b>MCA ID Number readonly..!</b></p>
                                </div>
                                 <div class="form-group col-md-6 col-sm-6 col-12">
                                    <label>Titles If any</label> 
                                    <input type="text" class="form-control"  readonly="readonly" name="titles" value="<?php echo $records['titles'];?>" placeholder="Titles If any">
                                  </div>  
                                <div class="form-group col-md-6 col-sm-6 col-12"> 
                                       <label>Was any disciplinary action taken against you?</label>
                                    <input type="text" class="form-control" name="disciplinary_action" value="<?php echo $records['disciplinary_action'];?>"  readonly="readonly" placeholder=" Was any disciplinary action taken against you? If yes, furnish details:">
                                 </div>   
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="name" id="name" value="<?php echo $records['price'];?>" placeholder="Name* " readonly="readonly">
                                     <p class="name_err" style="display:none;color:red;"><b>Name readonly..!</b></p>
                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                    <label>Photo </label> <br>
                                   <img src="<?php echo base_url()?>assets/player_photos/<?php echo $records['image'];?>" width="200px" height="200px">
                                  </div>
                            </div>
                         </div>
         <?php
              $price=$records['price'];
             
              // Merchant key here as provided by Payu
              $MERCHANT_KEY = "WDgFVtXO";
              // Merchant Salt as provided by Payu
              $SALT =  "MBx3tQv3DB";
              $txnid =  mt_rand(100000, 999999); // six digit random no
              $maxid = $this->db->query('SELECT MAX(player_temp_id) AS `maxid` FROM `tbl_players_temp`')->row()->maxid;
              $tempid=  $maxid +1;
              $user_id = $tempid;
              $hash_string = $MERCHANT_KEY."|".$txnid."|".$price."|Player Registration Fees|".$records['name']."|".$records['email']."|".$user_id."|".$records['email']."|".$records['mno']."||||||||".$SALT;
              $hash = hash('sha512', $hash_string);
              ?>
              <div class="col-sm-12" style="padding:8%; font-family: georgia; font-size: 18px;">
            <!--   <form method="POST" action="https://sandboxsecure.payu.in/_payment"> -->
              <form method="POST" action="https://secure.payu.in/_payment"> 
              <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY; ?>" />
              <input type="hidden" name="hash" value="<?php echo $hash; ?>"/>
              <input type="hidden" name="txnid" value="<?php echo $txnid; ?>" />
              <input type="hidden" name="amount" value="<?php echo $price;?>" />
              <input type="hidden" name="firstname" id="firstname" value="<?php echo $records['name'];?>" >
              <input type="hidden"  name="email" id="email" value="<?php echo $records['email'];?>"  />
               <input type="hidden" name="phone" value="<?php echo $records['mno'];?>" />
              <input type="hidden"  name="productinfo" value="Player Registration Fees">
              <input type="hidden"  name="surl" value="<?php echo base_url();?>registration/payment_success" size="64" />
              <input type="hidden"  name="furl" value="<?php echo base_url();?>registration/payment_fail" size="64" />
              <input type="hidden"   name="service_provider" value="payu_paisa" size="64" />
              <input  type="hidden"  name="udf1" value="<?php echo $user_id;?>">
              <input  type="hidden"  name="udf2" value="<?php echo $records['email'];?>">
              <input  type="hidden"  name="udf3" value="<?php echo $records['mno'];?>">
              <input type="submit" value="Continue" class="btn btn-success" >
              <input  onclick="goBack()"; class="btn btn-info" value="Back">
              </form>
          </div>
                   
                  <!--   <p class="btm_txt_register_form">In case you are using a public/shared computer we recommend that you logout to prevent any un-authorized access to your account</p> -->
                </div>
            </div>
        </div>
          <div class="login_back_img22 register_back22"></div>
    </div>
    <!--register area start end-->
   
   

    <!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Chitale.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Jain.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Amanora.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/nf.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs patner slider End -->
    
    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->

    <script>
          function validation(){
              var name  = $("#name").val();
              var parent_name  = $("#parent_name").val();
              var address = $("#address").val();
              var email = $("#email").val();
              var dob = $("#dob").val();
             var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
              var mno   = $("#mno").val();
              var mnolength = mno.length;
               var district   = $("#district").val();
               var fide_rating   = $("#fide_rating").val();
               var fide_id_no   = $("#fide_id_no").val();
               var aicf_id_no   = $("#aicf_id_no").val();
               var mca_id_no   = $("#mca_id_no").val();
              
             
             if(name ==''){
                   $('.name_err').show();
                   $('.all_err').show();
                  setTimeout(function() {
                    $('.name_err').fadeOut('slow');
                      }, 2000);
                  setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);
                   return false;
                }
              else if(parent_name ==''){
                   $('.parent_name_err').show();
                   $('.all_err').show();
                  setTimeout(function() {
                    $('.parent_name_err').fadeOut('slow');
                      }, 2000);
                  setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);
                   return false;
                }
                else if(address == ''){
                    $('.address_err').show();
                    $('.all_err').show();
                        setTimeout(function() {
                          $('.address_err').fadeOut('slow');
                      }, 2000);
                         setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);
                         return false; 
                }
               
              else if(mno =='' || mno < '0'){
                    $('.mno_err').show();
                      $('.all_err').show();
                       setTimeout(function() {
                    $('.mno_err').fadeOut('slow');
                      }, 2000);
                     setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);   
                      return false;
                } else if(mnolength != 10){
                     $('.mno_lengtherr').show();
                      $('.all_err').show();
                       setTimeout(function() {
                    $('.mno_lengtherr').fadeOut('slow');
                      }, 2000);
                     setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);      
                      return false;
                 } 
                  else if(email ==''){
                    $('.email_err').show();
                      $('.all_err').show();
                       setTimeout(function() {
                    $('.email_err').fadeOut('slow');
                      }, 2000);
                     setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);   
                     return false;
                }else  
                if(!email_regex.test(email)){ 
                    $('.invalidemail_err').show();
                     $('.all_err').show();
                       setTimeout(function() {
                    $('.invalidemail_err').fadeOut('slow');
                      }, 2000);
                     setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);    
                   return false;  
              } else if(dob == ''){
                    $('.dob_err').show();
                      $('.all_err').show();
                        setTimeout(function() {
                          $('.dob_err').fadeOut('slow');
                      }, 2000);
                       setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);  
                        return false;
                }
                else if(district == ''){
                    $('.district_err').show();
                      $('.all_err').show();
                        setTimeout(function() {
                          $('.district_err').fadeOut('slow');
                      }, 2000);
                       setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);  
                        return false;
                }
                 else if(fide_rating == ''){
                    $('.fide_rating_err').show();
                      $('.all_err').show();
                        setTimeout(function() {
                          $('.fide_rating_err').fadeOut('slow');
                      }, 2000);
                       setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);  
                        return false;
                }
                 else if(fide_id_no == ''){
                    $('.fide_id_no_err').show();
                      $('.all_err').show();
                        setTimeout(function() {
                          $('.fide_id_no_err').fadeOut('slow');
                      }, 2000);
                       setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);  
                        return false;
                }
                 else if(aicf_id_no == ''){
                    $('.aicf_id_no_err').show();
                      $('.all_err').show();
                        setTimeout(function() {
                          $('.aicf_id_no_err').fadeOut('slow');
                      }, 2000);
                       setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);  
                        return false;
                }
                 else if(mca_id_no == ''){
                    $('.mca_id_no_err').show();
                      $('.all_err').show();
                        setTimeout(function() {
                          $('.mca_id_no_err').fadeOut('slow');
                      }, 2000);
                       setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);  
                        return false;
                }
                 else if($('input[type=checkbox]:checked').length == 0){
                    $('.termsandcondition_err').show();
                      $('.all_err').show();
                        setTimeout(function() {
                          $('.termsandcondition_err').fadeOut('slow');
                      }, 2000);
                       setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);  
                        return false;
                }
           }
    </script>
</body>

</html>