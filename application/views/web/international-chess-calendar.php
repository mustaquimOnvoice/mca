<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- International Chess Calendar</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/inner/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
    <?php require('includes/head.php');?>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2> International Chess Calendar</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li> International Chess Calendar</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->

    <!-- result wrapper start -->
    <div class="latest_result_wrappwer float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <!-- <h1>All Events</h1> -->
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="table_next_race result_match_wrapper overflow-scroll">
                        <table>
                            <tr>
                                <th>Name</th>
                                <th>Place</th>
                                <th>Start</th>
                                <th>End</th>
                            </tr>
                            <tr>
                                <td>World School Individual Championships 2020</td>
                                <td>Panama City, Panama</td>
                                <td>2020</td>
                                <td>2020</td>
                            </tr>                            
                            <tr>
                                <td>World Youth U14, U16, U18 Championships 2020</td>
                                <td>Mamaia, Romania</td>
                                <td>07 Sep 2020</td>
                                <td>20 Sep 2020</td>
                            </tr>                            
                            <tr>
                                <td>Women's World Chess Cup 2020</td>
                                <td>Minsk, Belarus</td>
                                <td>10 Sep 2020</td>
                                <td>03 Oct 2020</td>
                            </tr>                            
                            <tr>
                                <td>World Youth U16 Chess Olympiad 2020</td>
                                <td>Nakhchivan, Azerbaijan</td>
                                <td>02 Oct 2020</td>
                                <td>12 Oct 2020</td>
                            </tr>                            
                            <tr>
                                <td>1st FIDE World Corporate Championship</td>
                                <td>Barcelona, Spain</td>
                                <td>16 Oct 2020</td>
                                <td>18 Oct 2020</td>
                            </tr>                            
                            <tr>
                                <td>World Cadet U8, U10, U12 Championships 2020</td>
                                <td>Batumi, Georgia</td>
                                <td>18 Oct 2020</td>
                                <td>31 Oct 2020</td>
                            </tr>                            
                            <tr>
                                <td>World Amateur Chess Championship 2020</td>
                                <td>Heraklion, Crete, Greece</td>
                                <td>21 Oct 2020</td>
                                <td>31 Oct 2020</td>
                            </tr>                            
                            <tr>
                                <td>World Senior Championship 2020</td>
                                <td>Assisi, Italy</td>
                                <td>05 Nov 2020</td>
                                <td>18 Nov 2020</td>
                            </tr>                            
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- result wrapper end -->
    <!--trophy wrapper start-->
    <!-- <div class="trophy_wrapper float_left">
        <div class="dream_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="trophy_slider">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url()?>images/inner/tp1.png" alt="img" class="img-responsive">
                                    <p>2016</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url()?>images/inner/tp2.png" alt="img" class="img-responsive">
                                    <p>2017-2018</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url()?>images/inner/tp3.png" alt="img" class="img-responsive">
                                    <p>2014</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url()?>images/inner/tp4.png" alt="img" class="img-responsive">
                                    <p>2012</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!--trophy wrapper end-->

    <!--patner slider Start -->
    <!-- <div class="patner_main_section_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- prs patner slider End -->

    <div class="patner_main_section_wrapper float_left"></div>
    
    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>