<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="images/hockey/favicon.png" />
	<!-- Bootstrap core css-->
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fonts.css">
	<link rel="stylesheet" href="flaticon/hockey/flaticon.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.default.css">
	<link rel="stylesheet" href="css/select2.min.css">
	<link rel="stylesheet" href="css/customScrollbar.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="js/plugin/rs_slider/layers.css">
	<link rel="stylesheet" href="js/plugin/rs_slider/navigation.css">
	<link rel="stylesheet" href="js/plugin/rs_slider/settings.css">
	<link rel="stylesheet" href="css/hockey_style.css">
    <?php require('includes/head.php');?>
    <style>
        @media all and (min-width:0px) and (max-width: 480px) {
            .tp-splitted {
                font-size: 30px;
                line-height: 35px;            
                letter-spacing: 0px;
                font-weight: 300;
            }
        }
        /* @media all and (min-width:801px) and (max-width: 1024px) {
            .tp-parallax-wrap{
                top: -100px !important;
            }
        } */
    </style>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <!-- navi wrapper Start -->    
    <?php require('includes/home/nav.php')?>
    <!-- navi wrapper End -->

    <!-- slider wrapper start -->
    <?php require('includes/home/sidebar.php');?>
    <!-- slider wrapper end -->
    
    
    <!-- <div class="best_player_wrapper float_left">
        <div class="container">
            <div class="news_letter_wrapper float_left">

                <div class="lr_nl_heading_wrapper">
                    <h2>Registration</h2>
                    <p>Player's registration.</p>
                </div>

                <div class="lr_nl_form_wrapper">
                    <input type="hidden" placeholder="Enter Your Email">
                    <button style="float: right;" type="submit" onclick="location.href = '<?php // echo base_url()?>registration';" > Click here</button>
                </div>

            </div>
        </div>
    </div> -->
    
    <!-- Top players -->
    <div class="upcoming_match_wrapper float_left" style="margin-top: 0px; padding-top: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="col-md-12">
                        <div class="heading_left ">
                            <h1 class="animate__animated animate__fadeInLeft animate__delay-5s animate__repeat-1">Grandmaster's From Maharashtra</h1>
                            <img src="images/hockey/heading_icon.png" alt="icon">
                        </div>
                    </div>
                    <div class="tg-upcomingmatch animate__animated animate__fadeInUp animate__delay-5s animate__repeat-1">
                        <div class="tg-match">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>NAME</th>
                                        <th>TITLE</th>
                                        <th>DISTRICT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Pravin Thipsay</td>
                                        <td>GRANDMASTER</td>
                                        <td>MUMBAI</td>
                                    </tr>
                                    <tr>
                                        <td>Abhijit Kunte</td>
                                        <td>GRANDMASTER</td>
                                        <td>PUNE</td>
                                    </tr>
                                    <tr>
                                        <td>Vidit Gujrathi</td>
                                        <td>GRANDMASTER</td>
                                        <td>NASHIK</td>
                                    </tr>
                                    <tr>
                                        <td>Akshayraj Kore</td>
                                        <td>GRANDMASTER</td>
                                        <td>PUNE</td>
                                    </tr>
                                    <tr>
                                        <td>Swapnil Dhopade</td>
                                        <td>GRANDMASTER</td>
                                        <td>AMRAVATI</td>
                                    </tr>
                                    <tr>
                                        <td>Shardul Gagare</td>
                                        <td>GRANDMASTER</td>
                                        <td>AHMEDNAGAR</td>
                                    </tr>
                                    <tr>
                                        <td>Abhimanyu Puranik</td>
                                        <td>GRANDMASTER</td>
                                        <td>PUNE</td>
                                    </tr>
                                    <tr>
                                        <td>Raunak Sadhwani</td>
                                        <td>GRANDMASTER</td>
                                        <td>NAGPUR</td>
                                    </tr>
                                    <tr>
                                        <td>Soumya Swaminathan</td>
                                        <td>WOMAN GRANDMASTER</td>
                                        <td>PUNE</td>
                                    </tr>
                                    <tr>
                                        <td>Eesha Karavade</td>
                                        <td>WOMAN GRANDMASTER</td>
                                        <td>PUNE</td>
                                    </tr>
                                    <tr>
                                        <td>Swati Ghate</td>
                                        <td>WOMAN GRANDMASTER</td>
                                        <td>PUNE</td>
                                    </tr>
                                    <tr>
                                        <td>Krutika Nadig</td>
                                        <td>WOMAN GRANDMASTER</td>
                                        <td>PUNE</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="col-md-12">
                        <div class="heading_left">
                            <h1 class="animate__animated animate__fadeInRight animate__delay-5s animate__repeat-1">India Top Players</h1>
                            <img src="images/hockey/heading_icon.png" alt="icon">
                        </div>
                    </div>
                    <div class="tg-upcomingmatch animate__animated animate__fadeInUp animate__delay-5s animate__repeat-1">
                        <div class="tg-match">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>NAME</th>
                                        <th>RATING</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Anand, Viswanathan</td>
                                        <td>2753</td>
                                    </tr>
                                    <tr>
                                        <td>Vidit, Santosh Gujrathi</td>
                                        <td>2726</td>
                                    </tr>
                                    <tr>
                                        <td>Harikrishna, Pentala</td>
                                        <td>2719</td>
                                    </tr>
                                    <tr>
                                        <td>Adhiban, B.</td>
                                        <td>2659</td>
                                    </tr>
                                    <tr>
                                        <td>Sasikiran, Krishnan</td>
                                        <td>2647</td>
                                    </tr>
                                    <tr>
                                        <td>Sethuraman, S.P.</td>
                                        <td>2644</td>
                                    </tr>
                                    <tr>
                                        <td>Aravindh, Chithambaram VR</td>
                                        <td>2641</td>
                                    </tr>
                                    <tr>
                                        <td>Negi, Parimarjan</td>
                                        <td>2639</td>
                                    </tr>
                                    <tr>
                                        <td>Ganguly, Surya Shekhar</td>
                                        <td>2635</td>
                                    </tr>
                                    <tr>
                                        <td>Nihal Sarin</td>
                                        <td>2620</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">                    
                </div> -->
                <!-- <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="tg-upcomingmatch">
                        <div class="tg-match">
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h3><strong>NAME</strong></h3> </div>
                                <div class="tg-box"> <h3><strong>TITLE</strong></h3> </div>
                                <div class="tg-box"> <h3><strong>DISTRICT</strong></h3> </div>
                            </div>                        
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Aditya Udeshi</h5> </div>
                                <div class="tg-box"> <h3> International Master</h3> </div>
                                <div class="tg-box"> <h3> Mumbai</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Sameer Kathmale</h5> </div>
                                <div class="tg-box"> <h3> International Master</h3> </div>
                                <div class="tg-box"> <h3> Sangli</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Rakesh Kulkarni</h5> </div>
                                <div class="tg-box"> <h3> International Master</h3> </div>
                                <div class="tg-box"> <h3> Mumbai</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Sammed Shete</h5> </div>
                                <div class="tg-box"> <h3> International Master</h3> </div>
                                <div class="tg-box"> <h3> Kolhapur</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Nubairshah Shaikh</h5> </div>
                                <div class="tg-box"> <h3> International Master</h3> </div>
                                <div class="tg-box"> <h3> Mumbai</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Shashikant Kutwal</h5> </div>
                                <div class="tg-box"> <h3> International Master</h3> </div>
                                <div class="tg-box"> <h3> Pune</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Harshit Raja</h5> </div>
                                <div class="tg-box"> <h3> International Master</h3> </div>
                                <div class="tg-box"> <h3> Pune</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Soumya Swaminathan</h5> </div>
                                <div class="tg-box"> <h3> Woman Grandmaster</h3> </div>
                                <div class="tg-box"> <h3> Pune</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Eesha Karavade</h5> </div>
                                <div class="tg-box"> <h3> Woman Grandmaster</h3> </div>
                                <div class="tg-box"> <h3> Pune</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Swati Ghate</h5> </div>
                                <div class="tg-box"> <h3> Woman Grandmaster</h3> </div>
                                <div class="tg-box"> <h3> Pune</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Krutika Nadig</h5> </div>
                                <div class="tg-box"> <h3> Woman Grandmaster</h3> </div>
                                <div class="tg-box"> <h3> Pune</h3> </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div> -->
                <!-- <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="col-md-12">
                        <div class="heading_left">
                            <h1>India Top Players</h1>
                            <img src="images/hockey/heading_icon.png" alt="icon">
                        </div>
                    </div>
                    <div class="tg-upcomingmatch">
                        <div class="tg-match">
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h3><strong>NAME</strong></h3> </div>
                                <div class="tg-box"> <h3><strong>RATING</strong></h3> </div>
                            </div>                        
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Anand, Viswanathan</h5> </div>
                                <div class="tg-box"> <h3> 2753</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Vidit, Santosh Gujrathi</h5> </div>
                                <div class="tg-box"> <h3> 2726</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Harikrishna, Pentala</h5> </div>
                                <div class="tg-box"> <h3> 2719</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Adhiban, B.</h5> </div>
                                <div class="tg-box"> <h3> 2659</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Sasikiran, Krishnan</h5> </div>
                                <div class="tg-box"> <h3> 2647</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Sethuraman, S.P.</h5> </div>
                                <div class="tg-box"> <h3> 2644</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Aravindh, Chithambaram VR.</h5> </div>
                                <div class="tg-box"> <h3> 2641</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Negi, Parimarjan</h5> </div>
                                <div class="tg-box"> <h3> 2639</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Ganguly, Surya Shekhar</h5> </div>
                                <div class="tg-box"> <h3> 2635</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Nihal Sarin</h5> </div>
                                <div class="tg-box"> <h3> 2620</h3> </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="col-md-12">
                        <div class="heading_left">
                            <h1>Top Women Players</h1>
                            <img src="images/hockey/heading_icon.png" alt="icon">
                        </div>
                    </div>
                    <div class="tg-upcomingmatch">
                        <div class="tg-match">
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h3><strong>NAME</strong></h3> </div>
                                <div class="tg-box"> <h3><strong>RATING</strong></h3> </div>
                            </div>                        
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Koneru, Humpy</h5> </div>
                                <div class="tg-box"> <h3> 2586</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Harika, Dronavalli</h5> </div>
                                <div class="tg-box"> <h3> 2515</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Vaishali R</h5> </div>
                                <div class="tg-box"> <h3> 2393</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Tania, Sachdev</h5> </div>
                                <div class="tg-box"> <h3> 2392</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Kulkarni Bhakti</h5> </div>
                                <div class="tg-box"> <h3> 2391</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Padmini, Rout</h5> </div>
                                <div class="tg-box"> <h3> 2370</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Nandhidhaa, P V</h5> </div>
                                <div class="tg-box"> <h3> 2365</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Gomes, Mary Ann</h5> </div>
                                <div class="tg-box"> <h3> 2354</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Soumya, Swaminathan</h5> </div>
                                <div class="tg-box"> <h3> 2351</h3> </div>
                            </div>
                            <div class="tg-matchdetail" style="padding-bottom: 0px;">
                                <div class="tg-box"> <h5>Karavade, Eesha</h5> </div>
                                <div class="tg-box"> <h3> 2345</h3> </div>
                            </div>
                        </div>
                    </div>
                </div>                 -->
            </div>
        </div>
    </div>
    <!--top players-->

    
    <!--Social start-->
    <div class="upcoming_match_wrapper float_left"style="margin-top: 0px; padding-top: 30px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading_left" style="text-align: center;">
                        <h1 style="h1:after{left: 50%;}" class="tg-upcomingmatch animate__animated animate__heartBeat animate__delay-5s animate__repeat-3">Social Media </h1>
                        <img  style="text-align: center;" src="images/hockey/heading_icon.png" alt="icon">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="heading_left" style="text-align: center;">
                        <!-- <script src="https://assets.juicer.io/embed.js" type="text/javascript"></script>
                        <link href="https://assets.juicer.io/embed.css" media="all" rel="stylesheet" type="text/css" />
                        <ul class="juicer-feed" data-feed-id="mahchessasso">
                        <h1 class="referral">
                            <a href="https://www.juicer.io">Powered by Juicer.io</a>
                        </h1>
                        </ul> -->
                        <!-- <iframe allowfullscreen id="wallsio-iframe" src="https://walls.io/m3bdv?theme=kiosk&amp;show_header=0" style="border:0;height:600px;width:100%" title="My social wall"></iframe> -->
                        <iframe src='https://www.juicer.io/api/feeds/mahchessasso/iframe' frameborder='0' width='100%' height='600px' style='display:block;margin:0 auto;'></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="heading_left" style="text-align: center;">
                        <!-- Place <div> tag where you want the feed to appear -->
                        <!-- <div id="curator-feed-default-layout"><a href="https://curator.io" target="_blank" class="crt-logo crt-tag">Powered by Curator.io</a></div> -->
                        <!-- The Javascript can be moved to the end of the html page before the </body> tag -->
                        <script type="text/javascript">
                        /* curator-feed-default-layout */
                        // (function(){
                        // var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
                        // i.src = "https://cdn.curator.io/published/951ffabf-1c0f-4842-b0ab-cdaec3167990.js";
                        // e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);
                        // })();
                        </script>

                        <iframe allowfullscreen id="wallsio-iframe" src="https://walls.io/m3bdv?colorscheme=dynamic&amp;theme=kiosk&amp;show_header=0" style="border:0;height:600px;width:100%" title="My social wall"></iframe>
                    </div>
                </div>
                <!-- <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 animated bounce">
                    <div class="upcoming_match_img float_left" style="text-align: center;">
                        <h4>Twitter:-</h4>
                        <a class="twitter-timeline" data-width="60%" data-height="400" data-theme="light" href="https://twitter.com/MahaChess?ref_src=twsrc%5Etfw">Tweets by MahaChess</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 animated bounce">
                    <div class="upcoming_match_img float_left" style="text-align: center;">
                        <h4>Facebook:-</h4>
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FMAHChessAsso%2F&tabs=timeline&width=340&height=400&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="auto" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                    </div>
                </div> -->

            </div>
        </div>
    </div>
    <!--Social end-->

    <!--add wrapper start-->
    <!-- <div class="next_match_new float_left">
        <div class="next_match_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading_left heading_white">
                        <h1>Add</h1>
                        <img src="images/hockey/heading_icon_white.png" alt="icon">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="tg-upcomingmatch next_macth_pader">
                        <div class="tg-match">
                            <div class="next_match_deatils">
                                <div class="tg-box next_box">
                                    <strong class="tg-teamlogo">
                                        <img src="images/hockey/gm5.png" alt="image description">
                                    </strong>
                                    <h3> india</h3>
                                </div>
                                <div class="tg-box next_box_green">
                                    <h4>Jan16, 15:30 PM Soccer Stadium, Dubai</h4>
                                </div>
                                <div class="tg-box next_box">
                                    <strong class="tg-teamlogo2">
                                        <img src="images/hockey/gm6.png" alt="image description">
                                    </strong>
                                    <h3>china </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="playboard_wrapper float_left text-center">
                        <img src="images/chess/play_board.jpg" alt="image" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!--add wrapper end-->

    <!--news wrapper start-->
    <!-- <div class="latest_news_wraper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading_left heading_special">
                        <h1>In News & Media</h1>
                        <img src="images/hockey/heading_icon.png" alt="icon">
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="ne_recent_left_side_wrapper float_left">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="ne_re_left_bottom_main_wrapper aa">
                                    <div class="ne_re_bottom_img">
                                        <div class="news_overlay"></div>
                                        <img src="images/hockey/ne1.jpg" alt="rs_img">
                                        <div class="lest_news_date_wrapper">
                                            <ul>
                                                <li>15</li>
                                                <li>feb</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="ne_re_bottom_img_cont">
                                        <h2>live</h2>
                                        <h1>the world championship</h1>
                                        <p><i class="fa fa-map-marker"></i>New Salford Road, Uk, M6 7AF
                                        </p>
                                        <p class="cntnt_pp">Atletico had some success in friendlies in 2012 and 2013. The team won 1–0 in Italy on February 29, 2012, the team's first ever win over Italy. On June 2, 2013, the U.S. played a friendly against Germany at a sold out RFK Stadium.
                                        </p>

                                        <div class="news_bottom_text float_left">
                                            <div class="news_text_left">
                                                <div class="adminActions">
                                                    <input type="checkbox" name="adminToggle" class="adminToggle" />
                                                    <a class="adminButton" href="#!"><i class="fa fa-share-alt"></i></a>
                                                    <div class="adminButtons">
                                                        <a href="#" title="Add Company"><i class="fa fa-facebook-f"></i></a>

                                                        <a href="#" title="Add User"><i class="fa fa-linkedin"></i></a>
                                                        <a href="#" title="Edit Company"><i class="fa fa-twitter"></i></a>
                                                        <a href="#" title="Edit User"><i class="fa fa-instagram"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="news_text_center">
                                                <p>MONDAY 08:00 PM</p>
                                            </div>
                                            <div class="news_text_right">
                                                <div class="hs_btn_wrapper cart_btn news_btn">
                                                    <a href="#" class="hocky_btn ckeck_btn">
                                                        <div class="btn-front">read more</div>
                                                        <div class="btn-back">read more</div>

                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="ne_re_left_bottom_main_wrapper">
                                    <div class="ne_re_bottom_img">
                                        <div class="news_overlay"></div>
                                        <img src="images/hockey/ne2.jpg" alt="rs_img">
                                        <div class="lest_news_date_wrapper">
                                            <ul>
                                                <li>05</li>
                                                <li>feb</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="ne_re_bottom_img_cont">
                                        <h1>EXTREME LEVEL OF SPORTS</h1>
                                        <p><i class="fa fa-map-marker"></i>New Road London, Uk, M6 AF
                                        </p>
                                        <p class="cntnt_pp">Atletico had some success in friendlies in 2012 and 2013. The team won 1–0 in Italy on February 29, 2012, the team's first ever win over Italy. On June 2, 2013, the U.S. played a friendly against Germany at a sold out RFK Stadium.
                                        </p>

                                        <div class="news_bottom_text float_left">
                                            <div class="news_text_left">
                                                <div class="adminActions">
                                                    <input type="checkbox" name="adminToggle" class="adminToggle" />
                                                    <a class="adminButton" href="#!"><i class="fa fa-share-alt"></i></a>
                                                    <div class="adminButtons">
                                                        <a href="#" title="Add Company"><i class="fa fa-facebook-f"></i></a>

                                                        <a href="#" title="Add User"><i class="fa fa-linkedin"></i></a>
                                                        <a href="#" title="Edit Company"><i class="fa fa-twitter"></i></a>
                                                        <a href="#" title="Edit User"><i class="fa fa-instagram"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="news_text_center">
                                                <p>MONDAY 08:00 PM</p>
                                            </div>
                                            <div class="news_text_right">
                                                <div class="hs_btn_wrapper cart_btn news_btn">
                                                    <a href="#" class="hocky_btn ckeck_btn">
                                                        <div class="btn-front">read more</div>
                                                        <div class="btn-back">read more</div>

                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="ne_re_left_bottom_main_wrapper">
                                    <div class="ne_re_bottom_img">
                                        <div class="news_overlay"></div>
                                        <img src="images/hockey/ne3.jpg" alt="rs_img">
                                        <div class="lest_news_date_wrapper">
                                            <ul>
                                                <li>15</li>
                                                <li>may</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="ne_re_bottom_img_cont">
                                        <h2>live</h2>
                                        <h1>hockey world cup 2018</h1>
                                        <p><i class="fa fa-map-marker"></i> Salford Road London, M6 7AF
                                        </p>
                                        <p class="cntnt_pp">Atletico had some success in friendlies in 2012 and 2013. The team won 1–0 in Italy on February 29, 2012, the team's first ever win over Italy. On June 2, 2013, the U.S. played a friendly against Germany at a sold out RFK Stadium.
                                        </p>

                                        <div class="news_bottom_text float_left">
                                            <div class="news_text_left">
                                                <div class="adminActions">
                                                    <input type="checkbox" name="adminToggle" class="adminToggle" />
                                                    <a class="adminButton" href="#!"><i class="fa fa-share-alt"></i></a>
                                                    <div class="adminButtons">
                                                        <a href="#" title="Add Company"><i class="fa fa-facebook-f"></i></a>

                                                        <a href="#" title="Add User"><i class="fa fa-linkedin"></i></a>
                                                        <a href="#" title="Edit Company"><i class="fa fa-twitter"></i></a>
                                                        <a href="#" title="Edit User"><i class="fa fa-instagram"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="news_text_center">
                                                <p>MONDAY 08:00 PM</p>
                                            </div>
                                            <div class="news_text_right">
                                                <div class="hs_btn_wrapper cart_btn news_btn">
                                                    <a href="#" class="hocky_btn ckeck_btn">
                                                        <div class="btn-front">read more</div>
                                                        <div class="btn-back">read more</div>

                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="hs_btn_wrapper cart_btn news_btn awerer">
                                   <a href="#" class="hocky_btn ckeck_btn">
                                       <div class="btn-front">read more</div>
                                       <div class="btn-back">read more</div>
                                   </a>
                               </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> -->
    <!--news wrapper end-->

    <!--initiatives start-->
    <!-- <div class="latest_news_wraper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading_left heading_special">
                        <h1>MCAs initiatives</h1>
                        <img src="images/hockey/heading_icon.png" alt="icon">
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="tab-content">
                        <div id="home" class="tab-pane active">
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 pd1">
                                    <div class="offer_main_boxes_wrapper float_left hvr-wobble-horizontal">
                                        <div class="ft_offer_img float_left">
                                            <img src="images/hockey/on1.png" alt="img">
                                        </div>
                                        <div class="ft_offer_heading float_left">
                                            <h2><a href="#">Get your school registered </a></h2>
                                        </div>
                                        <div class="cart_btn_shop float_left">
                                            <div class="hs_btn_wrapper btn_rsss">
                                                <a href="#" class="hocky_btn ckeck_btn">
                                                    <div class="btn-front">Read more</div>
                                                    <div class="btn-back">Read more</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 pd1">
                                    <div class="offer_main_boxes_wrapper float_left hvr-wobble-horizontal">
                                        <div class="ft_offer_img float_left">
                                            <img src="images/hockey/on2.png" alt="img">
                                        </div>
                                        <div class="ft_offer_heading float_left">
                                            <h2><a href="#">Monitor your school</a></h2>
                                        </div>
                                        <div class="cart_btn_shop float_left">
                                            <div class="hs_btn_wrapper btn_rsss">
                                                <a href="#" class="hocky_btn ckeck_btn">
                                                    <div class="btn-front">Read more</div>
                                                    <div class="btn-back">Read more</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 pd1">
                                    <div class="offer_main_boxes_wrapper float_left hvr-wobble-horizontal">
                                        <div class="ft_offer_img float_left">
                                            <img src="images/hockey/on3.png" alt="img">
                                        </div>
                                        <div class="ft_offer_heading float_left">
                                            <h2><a href="#">CIS Map</a></h2>
                                        </div>
                                        <div class="cart_btn_shop float_left">
                                            <div class="hs_btn_wrapper btn_rsss">
                                                <a href="#" class="hocky_btn ckeck_btn">
                                                    <div class="btn-front">Read more</div>
                                                    <div class="btn-back">Read more</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 pd1">
                                    <div class="offer_main_boxes_wrapper float_left hvr-wobble-horizontal">
                                        <div class="ft_offer_img float_left">
                                            <img src="images/hockey/on4.png" alt="img">
                                        </div>
                                        <div class="ft_offer_heading float_left">
                                            <h2><a href="#">CIS Success Stories</a></h2>
                                        </div>
                                        <div class="cart_btn_shop float_left">
                                            <div class="hs_btn_wrapper btn_rsss">
                                                <a href="#" class="hocky_btn ckeck_btn">
                                                    <div class="btn-front">Read more</div>
                                                    <div class="btn-back">Read more</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div> -->
    <!-- End Inetiatives-->

    <!--event start-->
    <!-- <div class="best_player_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="heading_left">
                        <h1>Upcomming Events</h1>
                        <img src="images/hockey/heading_icon.png" alt="icon">
                    </div>
                    <div id="tg-playerscrollbar" class="tg-players tg-playerscrollbar">
                        <div class="tg-player">
                            <div class="col-xl-6 col-lg-6 col-sm-6 col-12 pull-right">
                                <div class="tg-playcontent">
                                    <a class="tg-theme-tag" href="#">Indian</a>
                                    <h3><a href="#">Indian Chess Tournament</a></h3>
                                    <div class="cart_btn_shop float_left">
                                        <div class="hs_btn_wrapper btn_rsss">
                                            <a href="<?php echo site_url('web/events')?>" class="hocky_btn ckeck_btn">
                                                <div class="btn-front">Read more</div>
                                                <div class="btn-back">Read more</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tg-description">
                                        <p>Incididunt utia labore et dolore siti magna aliqua adinim lipat</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-sm-6 col-12">
                                <figure>
                                    <a href="#">
                                        <img src="images/chess/international-chess-calendar.jpg" alt="image description">
                                    </a>
                                </figure>
                            </div>
                        </div>
                        <div class="tg-player">
                            <div class="col-xl-6 col-lg-6 col-sm-6 col-12 pull-right">
                                <div class="tg-playcontent">
                                    <a class="tg-theme-tag" href="#">Indian</a>
                                    <h3><a href="#">Indian Chess Tournament</a></h3>
                                    <div class="cart_btn_shop float_left">
                                        <div class="hs_btn_wrapper btn_rsss">
                                            <a href="<?php echo site_url('web/events')?>" class="hocky_btn ckeck_btn">
                                                <div class="btn-front">Read more</div>
                                                <div class="btn-back">Read more</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tg-description">
                                        <p>Incididunt utia labore et dolore siti magna aliqua adinim lipat</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-sm-6 col-12">
                                <figure>
                                    <a href="#">
                                        <img src="images/chess/bx1.jpg" alt="image description">
                                    </a>
                                </figure>
                            </div>
                        </div>
                        <div class="tg-player">
                            <div class="col-xl-6 col-lg-6 col-sm-6 col-12 pull-right">
                                <div class="tg-playcontent">
                                    <a class="tg-theme-tag" href="#">World</a>
                                    <h3><a href="#">World Wide Chess Tournament</a></h3>
                                    <div class="cart_btn_shop float_left">
                                        <div class="hs_btn_wrapper btn_rsss">
                                            <a href="<?php echo site_url('web/events')?>" class="hocky_btn ckeck_btn">
                                                <div class="btn-front">Read more</div>
                                                <div class="btn-back">Read more</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tg-description">
                                        <p>Incididunt utia labore et dolore siti magna aliqua adinim lipat</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-sm-6 col-12">
                                <figure>
                                    <a href="#">
                                        <img src="images/chess/bx2.jpg" alt="image description">
                                    </a>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!--Event end-->

    <!-- <div class="latest_news_wraper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading_left heading_special">
                        <h1>Upcomming Events</h1>
                        <img src="images/hockey/heading_icon.png" alt="icon">
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tg-upcomingmatch next_macth_pader">
                                <div class="tg-match">
                                    <div class="next_match_deatils">
                                        <div class="tg-box next_box">
                                            <strong class="tg-teamlogo">
                                                <img src="images/hockey/gm5.png" alt="image description">
                                            </strong>
                                            <h3> india</h3>
                                        </div>
                                        <div class="tg-box next_box_green">
                                            <h4>Jan16, 15:30 PM Soccer Stadium, Dubai</h4>
                                        </div>
                                        <div class="tg-box next_box">
                                            <strong class="tg-teamlogo2">
                                                <img src="images/hockey/gm6.png" alt="image description">
                                            </strong>
                                            <h3>china </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="playboard_wrapper float_left text-center">
                                <img src="images/chess/international-chess-640.jpg" alt="image" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="heading_right text-center">
                                <h1>international chess calendar</h1>
                                <img src="images/hockey/heading_icon_white.png" alt="icon">
                            </div>
                        </div> 
                    </div>
                </div>

            </div>
        </div>
    </div> -->
    <div class="next_match_new float_left">
        <div class="next_match_overlay"  style="background: linear-gradient(to bottom right, blue, white);"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading_left heading_white">
                        <h1 class="animate__animated animate__heartBeat animate__delay-5s animate__repeat-3">Upcomming Events</h1>
                        <img src="images/hockey/heading_icon_white.png" alt="icon">
                    </div>
                </div>
                <div class="col-md-12 animate__animated animate__swing animate__delay-5s animate__repeat-3">
                    <div class="playboard_wrapper float_left text-center">
                        <img src="images/chess/international-chess-640.jpg" alt="image" class="img-responsive animated bounce">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Donar start-->
    <div class="counter_section float_left">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="heading_left" style="text-align: center;">
                        <h1 style="h1:after{left: 50%;}" class="animate__animated animate__heartBeat animate__delay-5s animate__repeat-3">Our Supporters </h1>
                        <img  style="text-align: center;" src="images/hockey/heading_icon.png" alt="icon">
                    </div>
                </div>
            </div>
        </div>
        <div class="wrap-album-slider">
            <ul class="album-slider">               

                <!-- End album slider item -->
                <li class="album-slider__item">
                    <figure class="album">
                        <div class="prs_upcom_movie_box_wrapper">
                            <div class="prs_upcom_movie_img_box">
                                <!-- <div class="prs_upcom_movie_img_box_overlay"></div> -->
                                <!-- <h1 class="match_result">Argentina  <span>vs</span> england<br> Argentina  won the final</h1> -->
                                <img src="images/chess/Chitale.jpg" alt="movie_img" style="width: 60%;"/>
                                <div class="prs_upcom_movie_img_overlay"></div>
                                <div class="hs_btn_wrapper slider_bx_btn">
                                    <ul>
                                        <li>
                                            <a href="#" class="hocky_btn">
                                                <div class="btn-front">view details</div>
                                                <div class="btn-back">view details</div>

                                            </a>
                                        <li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- End album body -->
                    </figure>
                    <!-- End album -->
                </li>
                                
                <!-- End album slider item -->
                <li class="album-slider__item">
                    <figure class="album">
                        <div class="prs_upcom_movie_box_wrapper">
                            <div class="prs_upcom_movie_img_box">
                                <!-- <div class="prs_upcom_movie_img_box_overlay"></div> -->
                                <!-- <h1 class="match_result">Argentina  <span>vs</span> india<br> india  won the final</h1> -->
                                <img src="images/chess/Jain.jpg" alt="movie_img" style="width: 60%;" />
                                <div class="prs_upcom_movie_img_overlay"></div>
                                <div class="hs_btn_wrapper slider_bx_btn">
                                    <ul>
                                        <li>
                                            <a href="#" class="hocky_btn">
                                                <div class="btn-front">view details</div>
                                                <div class="btn-back">view details</div>

                                            </a>
                                            <li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- End album body -->
                    </figure>
                    <!-- End album -->
                </li>
                                    
                <!-- End album slider item -->
                <li class="album-slider__item">
                    <figure class="album">
                        <div class="prs_upcom_movie_box_wrapper">
                            <div class="prs_upcom_movie_img_box">
                                <!-- <div class="prs_upcom_movie_img_box_overlay"></div> -->
                                <!-- <h1 class="match_result">france  <span>vs</span> india<br> france  won the final</h1> -->
                                <img src="images/chess/Amanora.jpg" alt="movie_img" style="width: 60%;" />
                                <div class="prs_upcom_movie_img_overlay"></div>
                                <div class="hs_btn_wrapper slider_bx_btn">
                                    <ul>
                                        <li>
                                            <a href="#" class="hocky_btn">
                                                <div class="btn-front">view details</div>
                                                <div class="btn-back">view details</div>

                                            </a>
                                            <li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- End album body -->
                    </figure>
                    <!-- End album -->
                </li>
                                        
                <!-- End album slider item -->
                <li class="album-slider__item">
                    <figure class="album">
                        <div class="prs_upcom_movie_box_wrapper">
                            <div class="prs_upcom_movie_img_box">
                                <!-- <div class="prs_upcom_movie_img_box_overlay"></div> -->
                                <!-- <h1 class="match_result">france  <span>vs</span> paries<br> paries  won the final</h1> -->
                                <img src="images/chess/nf.png" alt="movie_img" style="width: 60%;" />
                                <div class="prs_upcom_movie_img_overlay"></div>
                                <div class="hs_btn_wrapper slider_bx_btn">
                                    <ul>
                                        <li>
                                            <a href="#" class="hocky_btn">
                                                <div class="btn-front">view details</div>
                                                <div class="btn-back">view details</div>

                                            </a>
                                        <li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- End album body -->
                    </figure>
                    <!-- End album -->
                </li>

            </ul>
        </div>

    </div>
    <!--Donar end-->

    <!-- Next Match wrapper start-->
    <div class="ticket_slider float_left" hidden>
        <div class="container">
            <div class="next_match_wrapper float_left">
                <div class="row">

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="upcoming_matches_wrapper float_left">
                            <div class="row">

                                <div class="col-md-5 col-sm-5 col-4">
                                    <div class="match_list_wrapper as">
                                        <div class="match_list_img">
                                            <img src="images/hockey/team1.png" class="img-responsive" alt="logo">
                                            <h4>france</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-4">

                                    <div class="new">
                                        <a href="#">
                                            <span>VS</span>
                                        </a>
                                    </div>

                                </div>
                                <div class="col-md-5 col-sm-5 col-4">
                                    <div class="match_list_wrapper match_wraper_2">
                                        <div class="match_list_img">
                                            <img src="images/hockey/team2.png" class="img-responsive" alt="logo">
                                            <h4>germany</h4>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="next_match_count float_left">

                            <div id="clockdiv">
                                <div><span class="days"></span>
                                    <div class="smalltext">Days</div>
                                </div>
                                <div><span class="hours"></span>
                                    <div class="smalltext">Hours</div>
                                </div>
                                <div><span class="minutes"></span>
                                    <div class="smalltext">Minutes</div>
                                </div>
                                <div><span class="seconds"></span>
                                    <div class="smalltext">Seconds</div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Next Match wrapper end-->

    <!--gallery wrapper start-->
    <div class="portfolio_grid float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="heading_left heading_special">
                        <h1>media gallery</h1>
                        <img src="images/hockey/heading_icon.png" alt="icon">
                    </div> -->
                    <div class="heading_left" style="text-align: center;">
                        <h1 style="h1:after{left: 50%;}"  class="animate__animated animate__heartBeat animate__delay-5s animate__repeat-3">press & media</h1>
                        <img  style="text-align: center;" src="images/hockey/heading_icon.png" alt="icon">
                    </div>

                    <ul class="protfoli_filter">
                        <li class="active" data-filter="*"><a href="#"> all</a></li>

                        <li data-filter=".press"><a href="#">press</a></li>
                        <li data-filter=".media"><a href="#">media</a></li>
                        <!-- <li data-filter=".ux_ui"><a href="#">bowler</a></li> -->
                    </ul>
                </div>
            </div>
            <div class="row portfoli_inner animate__animated animate__fadeInRight animate__delay-5s animate__repeat-2">
                <!-- Items -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 press">
                    <div class="portfolio_item">
                        <img src="images/press/1.jpeg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/press/1-full.jpeg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 press">
                    <div class="portfolio_item">
                        <img src="images/press/2.jpeg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/press/2-full.jpeg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 press">
                    <div class="portfolio_item">
                        <img src="images/press/3.jpeg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/press/3.jpeg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 press">
                    <div class="portfolio_item">
                        <img src="images/press/4.jpeg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/press/4-full.jpeg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row portfoli_inner animate__animated animate__fadeInLeft animate__delay-5s animate__repeat-2">
                <!-- Items -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 media">
                    <div class="portfolio_item">
                        <img src="images/gallery/1.jpg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/gallery/1.jpg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Items -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 media">
                    <div class="portfolio_item">
                        <img src="images/gallery/2.jpg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/gallery/2.jpg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Items -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 media">
                    <div class="portfolio_item">
                        <img src="images/gallery/3.jpg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/gallery/3.jpg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Items -->
                <!-- <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 media">
                    <div class="portfolio_item">
                        <img src="images/gallery/4.jpg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/gallery/4.jpg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- Items -->
                <!-- <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 media">
                    <div class="portfolio_item">
                        <img src="images/gallery/5.jpg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/gallery/5.jpg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 media">
                    <div class="portfolio_item">
                        <img src="images/gallery/6.jpg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> MCL</a>
                            <div class="zoom_popup">
                                <a class="img-link" href="images/gallery/6.jpg"> <i class="flaticon-magnifier"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="hs_btn_wrapper cart_btn news_btn awerer">
                    <a href="#" class="hocky_btn ckeck_btn">
                        <div class="btn-front" onclick="location.href = '<?php echo site_url('web/press_media')?>'">read more</div>
                        <div class="btn-back" onclick="location.href = '<?php echo site_url('web/press_media')?>'">read more</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--gallery wrapper end-->

    <!-- counter wrapper start-->
    <!-- <div class="counter_section float_left">
        <div class="counter-section">
            <div class="container text-center">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12 pddddd">
                        <div class="counter_cntnt_box float_left">
                            <div class="tb_icon">
                                <div class="icon"> <a href="#"><i class="flaticon-cricket"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="count-description"><span class="timer">230</span>+
                            <h5 class="con1"> Matches Played</h5>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12 pddddd">
                        <div class="counter_cntnt_box float_left">
                            <div class="tb_icon">
                                <div class="icon"> <a href="#"><i class="flaticon-hockey-helmet"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="count-description"> <span class="timer">89</span>+
                            <h5 class="con2">Touchdowns</h5>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12 pddddd">
                        <div class="counter_cntnt_box float_left">
                            <div class="tb_icon">
                                <div class="icon"> <a href="#"><i class="flaticon-cricket-2"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="count-description"> <span class="timer">60</span>+
                            <h5 class="con2">team members</h5>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12 pddddd">
                        <div class="counter_cntnt_box float_left">
                            <div class="tb_icon">
                                <div class="icon"> <a href="#"><i class="flaticon-trophy"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="count-description"> <span class="timer">27</span>+
                            <h5 class="con4">awards won</h5>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- counter wrapper end-->
    
    <!--store wrapper start-->
    <div class="official_store float_left"></div>
    <!--store wrapper end-->
    
    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/customScrollbar.min.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/plugin/rs_slider/jquery.themepunch.revolution.min.js"></script>
    <script src="js/plugin/rs_slider/jquery.themepunch.tools.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.addon.snow.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.actions.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.carousel.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.kenburn.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.layeranimation.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.migration.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.navigation.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.parallax.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.slideanims.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.extension.video.min.js"></script>
    <script src="js/plugin/rs_slider/revolution.addon.snow.min.js"></script>
	<script src="js/cursor.js"></script>
    <script src="js/hockey.js"></script>
    <!-- custom js-->
    <script>
        $('.zoom_popup').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function(item) {
                    return item.el.attr('title') + '<small></small>';
                }
            }
        });
    </script>
    

</body>

</html>