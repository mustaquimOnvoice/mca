<!DOCTYPE html>
<html lang="zxx">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- Press & Media</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/inner/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
</head>

<body>
    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2>press - media</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url()?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>press - media</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->

	<!--gallery wrapper start-->
    <div class="portfolio_gridIII float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper left_gallery_heading">
                        <h1>press - media</h1>

                    </div>

                     <ul class="protfoli_filter III_filter">
                        <li class="active" data-filter="*"><a href="#"> all</a></li>

                        <li data-filter=".press"><a href="#">press</a></li>
                        <li data-filter=".media"><a href="#">media</a></li>
                        <!-- <li data-filter=".ux_ui"><a href="#">stadium</a></li> -->
                    </ul>
                </div>
            </div>

        <div class="row portfoli_inner">
            <!-- Items -->
            <?php for($i=1;$i<=6;$i++){?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 press">
                    <div class="portfolio_item">
                        <img src="<?php echo base_url()?>images/gallery/<?php echo $i?>-full.jpeg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> Press</a>
                            <div class="zoom_popup masonry_btn">
                                <a class="img-link" href="<?php echo base_url()?>images/gallery/<?php echo $i?>-full.jpeg"> <i class="flaticon-search"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>

            <?php for($i=1;$i<=10;$i++){?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 media">
                    <div class="portfolio_item">
                        <img src="<?php echo base_url()?>images/gallery/<?php echo $i?>.jpg" alt="">
                        <div class="portfolio_hover">
                            <a href="#"> Media</a>
                            <div class="zoom_popup masonry_btn">
                                <a class="img-link" href="<?php echo base_url()?>images/gallery/<?php echo $i?>.jpg"> <i class="flaticon-search"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
			<!-- <div class="hs_btn_wrapper match_btn gallery_Btn">
                <ul>
                    <li><a href="#">load more</a></li>
                </ul>
           </div> -->
        </div>
    </div>
    </div>
    <!--gallery wrapper end-->
    
    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
	  <script>
        $('.zoom_popup').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function(item) {
                    return item.el.attr('title') + '<small></small>';
                }
            }
        });
    </script>
</body>

</html>