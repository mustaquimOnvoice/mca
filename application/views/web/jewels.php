<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- Jewels</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/hockey/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
	<?php require('includes/head.php');?>
	<style>
		.iner_committee_wrapper{
			padding-bottom: 0px;
			background: #fff;
			text-align: center;
		}
	</style>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2>Our Jewels</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>Our Jewels</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
   <!-- inner welcome Wrapper Start -->
    <div class="iner_committee_wrapper float_left">
        <div class="container">
            <div class="row team-section text-center my-5">
				<h2 class="h1-responsive font-weight-bold my-5">Our Jewels</h2>
						<div class="row">
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/GM Abhijit Kunte-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Abhijit Kunte</h5>
							  <p class="text-uppercase blue-text"><strong>GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">sid@mayurs.in</p> -->
							  <!-- <p class="grey-text">9890297575</p> -->
							  <p class="grey-text">PUNE</p>
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/GM Abhimanyu Puranik-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Abhimanyu Puranik</h5>
							  <p class="text-uppercase blue-text"><strong>GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">abhyak@yahoo.com</p> -->
							  <!-- <p class="grey-text">9422306236</p> -->
							  <p class="grey-text">PUNE</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/GM Akshayraj Kore-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Akshayraj Kore</h5>
							  <p class="text-uppercase blue-text"><strong>GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">girishvyas5327@yahoo.co.in</p> -->
							  <!-- <p class="grey-text">9373222215</p> -->
							  <p class="grey-text">PUNE</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/GM Pravin Thipsay-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Pravin Thipsay</h5>
							  <p class="text-uppercase blue-text"><strong>GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">girishchitale@gmail.com</p> -->
							  <!-- <p class="grey-text">9860191405</p> -->
							  <p class="grey-text">MUMBAI</p>
							 
							</div>
							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/GM Raunak Sadhwani-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Raunak Sadhwani</h5>
							  <p class="text-uppercase blue-text"><strong>GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">aniruddha@amanora.com</p> -->
							  <!-- <p class="grey-text">9822063288</p> -->
							  <p class="grey-text">Nagpur</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/GM Shardul Gagare-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shardul Gagare</h5>
							  <p class="text-uppercase blue-text"><strong>GRANDMASTER </strong></p>
							  <!-- <p class="grey-text">narendrafirodia@gmail.com</p> -->
							  <!-- <p class="grey-text">9890048567</p> -->
							  <p class="grey-text">Ahmednagar</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/GM Swapnil Dhopade-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Swapnil Dhopade</h5>
							  <p class="text-uppercase blue-text"><strong>GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">contactndca@gmail.com </p> -->
							  <!-- <p class="grey-text">9822501811 </p> -->
							  <p class="grey-text">AMRAVATI </p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/GM Vidit Gujrathi-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Vidit Gujrathi  </h5>
							  <p class="text-uppercase blue-text"><strong>GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">pbbhilare@gmail.com</p> -->
							  <!-- <p class="grey-text">9869017221</p> -->
							  <p class="grey-text">NASHIK</p>							  
							</div><br>

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/WGM Eesha Karavade-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Eesha Karavade </h5>
							  <p class="text-uppercase blue-text"><strong>WOMEN GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">niranjan_godbole@yahoo.in</p> -->
							  <!-- <p class="grey-text">9325228261</p> -->
							  <p class="grey-text">PUNE</p>							  
							</div>
							<!-- Grid column -->
							
							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/WGM Krutika Nadig-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Krutika Nadig</h5>
							  <p class="text-uppercase blue-text"><strong>WOMEN GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">faruksk.hockey@gmail.com</p> -->
							  <!-- <p class="grey-text">9423185786</p> -->
							  <p class="grey-text">PUNE</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/WGM Soumya Swaminathan-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Soumya Swaminathan</h5>
							  <p class="text-uppercase blue-text"><strong>WOMEN GRANDMASTER</strong></p>
							  <!-- <p class="grey-text">pratikpghogare@gmail.com </p> -->
							  <!-- <p class="grey-text">9423154300 </p> -->
							  <p class="grey-text">PUNE </p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/jewels/WGM Swati Ghate-min.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Swati Ghate</h5>
							  <p class="text-uppercase blue-text"><strong>WOMEN GRANDMASTER </strong></p>
							  <!-- <p class="grey-text">ankushraktade@gmail.com </p> -->
							  <!-- <p class="grey-text">9405777784 </p> -->
							  <p class="grey-text">PUNE </p>
							  
							</div>
							<!-- Grid column -->
							
							
						</div>
						<!-- Grid row -->
						
						
					
			</div>
        </div>
    </div>
    <!-- inner welcome Wrapper end -->
    
	<!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <!-- <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- prs patner slider End -->

    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>