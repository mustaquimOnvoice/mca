<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>MCA- Search Player </title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="sport,sporteyz" />
    <meta name="keywords" content="sport,sporteyz" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/inner/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2> Search Player </h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url()?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>Search Player </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
    <!-- latest result wrapper start-->
    <!-- <div class="latest_result_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <h1>latest result</h1>

                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="upcoming_matches_wrapper float_left">
                        <div class="row">

                            <div class="col-md-3 col-sm-12 col-12">
                                <div class="match_list_wrapper as">
                                    <div class="match_list_img">
                                        <img src="<?php echo base_url()?>images/inner/team1.png" class="img-responsive" alt="logo">
                                        <h4>india</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="latest_match_box float_left">
                                    <p>Asian Cup 2018</p>
                                    <h1>INDIA VS pakistan, FINAL</h1>
                                    <h2>Dubai International Cricket Stadium, Dubai</h2>
                                    <h3>223/7 (50.0) - 222 (48.3)</h3>
                                    <h4>INDIA BEAT pakistan BY 3 WICKETS</h4>
                                </div>

                            </div>
                            <div class="col-md-3 col-sm-12 col-12">
                                <div class="match_list_wrapper match_wraper_2">
                                    <div class="match_list_img">
                                        <img src="<?php echo base_url()?>images/inner/team2.png" class="img-responsive" alt="logo">
                                        <h4>pakistan</h4>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> -->
    <!-- latest result wrapper end-->

    <!--register area start here-->
    <div class="login_section float_left">
      <div class="login_back_img register_back"></div>
        <div class="login_form_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="ft_left_heading_wraper gallery_heading_center text-center login_head">
                            <h1>Search Player here</h1>
                             
                        </div>
                    </div>
                </div>
                <div class="col-md-8 offset-md-2 col-sm-12">
                    <form name="registration" action="#" method="post" enctype="multipart/form-data">   
                       
                    <div class="login_wrapper">
                             <div class="row clearfix">
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-12">
                                    <input type="text" class="form-control" name="search_field" id="search_field" value="" placeholder="Enter Name/Registration Number " required="required">
                                     <p class="name_err" style="display:none;color:red;"><b>Name/Registration Number  Required..!</b></p>
        
                                </div>
                                 <div class="form-group col-md-6 col-sm-6 col-12">
                                    <input   onclick="return validation();" class="btn btn-primary" name="submit" value="Search">
                                </div>
                               
                            </div>


                        </div>

                    </form>
                      <div style="display:none"  id="oldadatares">
                            
                    </div>
                  <!--   <p class="btm_txt_register_form">In case you are using a public/shared computer we recommend that you logout to prevent any un-authorized access to your account</p> -->
                </div>
            </div>
        </div>
          <div class="login_back_img22 register_back22"></div>
    </div>
    <!--register area start end-->
   
    <!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs patner slider End -->
    
    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->

    <script>
     
      
        


          function validation(){

              var search_field  = $("#search_field").val();
            
             
             if(search_field ==''){
                   $('.name_err').show();
                   $('.all_err').show();
                  setTimeout(function() {
                    $('.name_err').fadeOut('slow');
                      }, 2000);
                  setTimeout(function() {
                    $('.all_err').fadeOut('slow');
                      }, 4000);
                   return false;
                }
             else{ 
                  $.ajax({
                 type: "POST",
                 url: "<?php echo base_url()?>Search/isrecord",
                 data: {'search':search_field},
                 success: function(result)
                 {
                    $('#oldadatares').css('display','block');
                    $('#oldadatares').html(result);
                            
                 } 
                   });
                 }  
               
           }
    </script>
</body>

</html>