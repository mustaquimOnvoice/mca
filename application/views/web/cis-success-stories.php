<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- CIS Success Stories</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/inner/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
    <?php require('includes/head.php');?>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2> CIS Success Stories</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url()?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li> CIS Success Stories</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->

    <!-- result wrapper start -->
    <div class="latest_result_wrappwer float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <!-- <h1>All Events</h1> -->
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="table_next_race result_match_wrapper overflow-scroll">
                        <table>
                            <tr>
                                <th>name</th>
                                <th>title</th>
                                <th>district</th>
                            </tr>
                            <tr>
                                <td>Pravin Thipsay</td>
                                <td>Grandmaster</td>
                                <td>Mumbai</td>
                            </tr>
                            <tr>
                                <td>Abhijit Kunte</td>
                                <td>Grandmaster</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Vidit Gujrathi</td>
                                <td>Grandmaster</td>
                                <td>Nashik</td>
                            </tr>
                            <tr>
                                <td>Akshayraj Kore</td>
                                <td>Grandmaster</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Swapnil Dhopade</td>
                                <td>Grandmaster</td>
                                <td>Amravati</td>
                            </tr>
                            <tr>
                                <td>Shardul Gagare</td>
                                <td>Grandmaster</td>
                                <td>Ahmednagar</td>
                            </tr>
                            <tr>
                                <td>Abhimanyu Puranik</td>
                                <td>Grandmaster</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Raunak Sadhwani</td>
                                <td>Grandmaster</td>
                                <td>Nagpur</td>
                            </tr>
                            <tr>
                                <td>Abhishek Kelkar</td>
                                <td>International Master</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Vikramaditya Kulkarn</td>
                                <td>International Master</td>
                                <td>Mumbai</td>
                            </tr>
                            <tr>
                                <td>Sagar Shah</td>
                                <td>International Master</td>
                                <td>Mumbai</td>
                            </tr>
                            <tr>
                                <td>Aditya Udeshi</td>
                                <td>International Master</td>
                                <td>Mumbai</td>
                            </tr>
                            <tr>
                                <td>Sameer Kathmale</td>
                                <td>International Master</td>
                                <td>Sangli</td>
                            </tr>
                            <tr>
                                <td>Rakesh Kulkarni</td>
                                <td>International Master</td>
                                <td>Mumbai</td>
                            </tr>
                            <tr>
                                <td>Sammed Shete</td>
                                <td>International Master</td>
                                <td>Kolhapur</td>
                            </tr>
                            <tr>
                                <td>Nubairshah Shaikh</td>
                                <td>International Master</td>
                                <td>Mumbai</td>
                            </tr>
                            <tr>
                                <td>Shashikant Kutwal</td>
                                <td>International Master</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Harshit Raja</td>
                                <td>International Master</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Soumya Swaminathan</td>
                                <td>Woman Grandmaster</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Eesha Karavade</td>
                                <td>Woman Grandmaster</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Swati Ghate</td>
                                <td>Woman Grandmaster</td>
                                <td>Pune</td>
                            </tr>
                            <tr>
                                <td>Krutika Nadig</td>
                                <td>Woman Grandmaster</td>
                                <td>Pune</td>
                            </tr>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- result wrapper end -->

    <div class="patner_main_section_wrapper float_left"></div>
    
    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>