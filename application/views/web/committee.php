<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- Committee</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/hockey/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
	<?php require('includes/head.php');?>
	<style>
		.iner_committee_wrapper{
			padding-bottom: 0px;
			background: #fff;
			text-align: center;
		}
	</style>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2>Our Committee</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>Our Committee</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
   <!-- inner welcome Wrapper Start -->
    <div class="iner_committee_wrapper float_left">
        <div class="container">
            <div class="row team-section text-center my-5">
				<h2 class="h1-responsive font-weight-bold my-5">Our Committee</h2>
						<div class="row">
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Siddharth.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Siddharth Mayur</h5>
							  <p class="text-uppercase blue-text"><strong>President</strong></p>
							  <p class="grey-text">sid@mayurs.in</p>
							  <!-- <p class="grey-text">9890297575</p> -->
							  <p class="grey-text">Jalgaon</p>
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Abhijit.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Abhijit Kunte   </h5>
							  <p class="text-uppercase blue-text"><strong>Senior Vice President </strong></p>
							  <p class="grey-text">abhyak@yahoo.com</p>
							  <!-- <p class="grey-text">9422306236</p> -->
							  <p class="grey-text">Pune</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Girish.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Girish Chitale   </h5>
							  <p class="text-uppercase blue-text"><strong>Vice President</strong></p>
							  <p class="grey-text">girishchitale@gmail.com</p>
							  <!-- <p class="grey-text">9373222215</p> -->
							  <p class="grey-text">Sangali</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Vyas.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Girish Vyas</h5>
							  <p class="text-uppercase blue-text"><strong>Vice President </strong></p>
							  <p class="grey-text">girishvyas5327@yahoo.co.in</p>
							  <!-- <p class="grey-text">9860191405</p> -->
							  <p class="grey-text">Nagpur</p>
							 
							</div>
							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Aniruddha.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Aniruddha Deshpande</h5>
							  <p class="text-uppercase blue-text"><strong>Vice President</strong></p>
							  <p class="grey-text">aniruddha@amanora.com</p>
							  <!-- <p class="grey-text">9822063288</p> -->
							  <p class="grey-text">Pune</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Narendra.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Narendra Firodia</h5>
							  <p class="text-uppercase blue-text"><strong>Vice President </strong></p>
							  <p class="grey-text">narendrafirodia@gmail.com</p>
							  <!-- <p class="grey-text">9890048567</p> -->
							  <p class="grey-text">Ahmednagar</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Vinay.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Vinay Bele</h5>
							  <p class="text-uppercase blue-text"><strong>Vice President</strong></p>
							  <p class="grey-text">contactndca@gmail.com </p>
							  <!-- <p class="grey-text">9822501811 </p> -->
							  <p class="grey-text">Nashik </p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 animate__animated animate__fadeInRight animate__delay-3s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Bhilare.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri P.B. Bhilare  </h5>
							  <p class="text-uppercase blue-text"><strong>Vice President</strong></p>
							  <p class="grey-text">pbbhilare@gmail.com</p>
							  <!-- <p class="grey-text">9869017221</p> -->
							  <p class="grey-text">Mumbai</p>
							  
							</div><br>

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Niranjan.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Niranjan Godbole </h5>
							  <p class="text-uppercase blue-text"><strong>Hon. Gen. Secretary</strong></p>
							  <p class="grey-text">niranjan_godbole@yahoo.in</p>
							  <!-- <p class="grey-text">9325228261</p> -->
							  <!-- <p class="grey-text">Aurangabad</p> -->							  
							</div>
							<!-- Grid column -->
							
							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Faruk.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Faruk Shaikh</h5>
							  <p class="text-uppercase blue-text"><strong>Hon. Treasurer</strong></p>
							  <p class="grey-text">faruksk.hockey@gmail.com</p>
							  <!-- <p class="grey-text">9423185786</p> -->
							  <p class="grey-text">Jalgaon</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Pratik.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Pratik Ghogare</h5>
							  <p class="text-uppercase blue-text"><strong>Joint Secretary</strong></p>
							  <p class="grey-text">pratikpghogare@gmail.com </p>
							  <!-- <p class="grey-text">9423154300 </p> -->
							  <p class="grey-text">Amravati </p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Ankush.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Ankush Raktade</h5>
							  <p class="text-uppercase blue-text"><strong>Joint Secretary </strong></p>
							  <p class="grey-text">ankushraktade@gmail.com </p>
							  <!-- <p class="grey-text">9405777784 </p> -->
							  <p class="grey-text">Buldhana </p>
							  
							</div><br>
							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5  animate__animated animate__fadeInRight animate__delay-5s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Yashwant.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Yashwant Bapat </h5>
							  <p class="text-uppercase blue-text"><strong>Joint Secretary</strong></p>
							  <p class="grey-text">bapatyashwant@yahoo.co.in </p>
							  <!-- <p class="grey-text">9326092501 </p> -->
							  <p class="grey-text">Ahmednagar </p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInRight animate__delay-5s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Chandrakant.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Chandrakant Walavade</h5>
							  <p class="text-uppercase blue-text"><strong>Joint Secretary</strong></p>
							  <p class="grey-text">chanduwalawade@gmail.com</p>
							  <!-- <p class="grey-text">9975959055</p> -->
							  <p class="grey-text">Sangli</p>
							  
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-md-0 mb-5 animate__animated animate__fadeInRight animate__delay-5s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/Hemendra.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Hemendra Patel </h5>
							  <p class="text-uppercase blue-text"><strong>Joint Secretary</strong></p>
							  <p class="grey-text">aurangabadchess@gmail.com</p>
							  <!-- <p class="grey-text">9325228261</p> -->
							  <p class="grey-text">Aurangabad</p>
							  
							</div>
							<!-- Grid column -->
							
							<!-- Grid column -->
							<div class="col-lg-3 col-md-6 mb-lg-0 mb-5 animate__animated animate__fadeInRight animate__delay-5s animate__repeat-1" style="margin-top:30px;">
							  <div class="avatar mx-auto">
								<img src="<?php echo base_url();?>images/inner/committee/ninant.jpg" class="rounded-circle z-depth-1"
								  alt="Sample avatar">
							  </div>
							  <h5 class="font-weight-bold mt-4 mb-3">Shri Ninant Pednekar</h5>
							  <p class="text-uppercase blue-text"><strong>JOINT SECRETARY</strong></p>
							  <p class="grey-text">ninant561968@gmail.com </p>
							  <!-- <p class="grey-text">8999907282 </p> -->
							  <p class="grey-text">Thane </p>
							  
							</div>
							<!-- Grid column -->
							
						</div>
						<!-- Grid row -->
						
						
					
			</div>
        </div>
    </div>
    <!-- inner welcome Wrapper end -->
    
	<!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <!-- <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- prs patner slider End -->

    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>