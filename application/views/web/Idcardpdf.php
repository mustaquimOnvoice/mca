<html><body>
       <!-- <link rel="stylesheet" href="<?php echo base_url()?>pdf_assets/css/mycss.css" />-->
<style>
.login-box, .register-box{
margin: 0% 0% 3% 0%;
width:auto;
}

 .footerpglinks center	{
	display: inline-block;
} 

.footerpglinks a{
	float: right;
}

.algnright{
	text-align: right;
}

.register-box{
	width:100%;
}

.jsize input{
display: inline-block;
	width: 25%;}

.main-header .navbar .nav .open > a{
background-color: #56585d !important;
}

.login-page .main-footer, .register-page .main-footer{
	margin-left:0px;
}

.logoimg{
	width:25%;
	display:block;
}


.navbar-custom-menu{
	display: inline-block !important;
position: initial !important;
}

/* .rightmenu .open .dropdown-menu{
	position: inherit !important;
} */


.vieworder .box{
padding:0px;
}

.vieworder .form-group{
	padding-left:5px;
	padding-right:5px;
}

.mysubmenu2 i{
	font-size:10px;
}

.mainmenu, .usermainmenu{
	padding-left:30px;
}

#myorderdet .table-responsive{
	float:right;
}

.mainorder table{
	text-align:center;
}

.homeimg{
width:19%;
border: 12px double #8c8c8c;	
}


.logoimg{
	width:6%;
}

.logoname{
	display:inline-block;
	margin-top:0px;
	font-size: 22px;
}

.centerdata{
	text-align:center;
}


.viewplayerphoto img,.viewpayphoto img{
	max-width:200px;
}


.player_name{
	font-size: 25px;
}

.player_dist{
	text-transform: capitalize;
}

.headlabel{
	
text-transform: uppercase;
font-size: 17px;
display: inline-block;
color: #959494;	
line-height: 1.5em;
}	


.datafld
{
	display: inline-block;
	font-size: 16px;
	line-height: 1.5em;
}

.player_idno{
	font-size: 18px;
}

.bgdiv{
	background-image:url('../../images/chess1.jpg');
	background-repeat: no-repeat;
background-size: cover;
}

/*id card css*/
.idbodyborder{
 border: 15px solid #50a7ae;
padding: 10px;
margin: 20px auto;
background-color: #fff;
}


#idModal .logo img
{
    width:130px;
}

.idbody .title
{
    font-size: 22px;
color: #191c86;
font-weight: 600;
}

.idplayerphoto img
{
    border-radius:12px;
    width: 127px;
    /*height: 148px;*/
    display:block;
}

.idplydata p
{
    margin: 0 0 15px 0;
line-height: 1.5;
}

.idplydata
{
    font-weight: 600;
}

.idrow1 .r
{
     padding-left: 16px;   
}

.signatr
{
display: block;
position: absolute;
bottom: 16%;
right: 7%;
width: 147px;
}

.signatr img
{
    width:100%;
}


.logo, .chsdata,.idplayerphoto 
{
    text-align:center;
}



/*****************************************/
@media only screen and (min-width: 650px){
	.headlabel{
		width: 25%;
	}
	
}
@media only screen and (max-width: 991px){

/*login page*/

 .footerpglinks center	{
	display: block;
} 

.footerpglinks span{
	display: none;
}

.navbar-custom-menu > .navbar-nav > li > .dropdown-menu{
	right:0%;
}

.signatr
{
    right: 0.5%;
}

}

@media only screen and (max-width: 350px)
{
    .signatr
    {
        bottom: -6%;
    width: 121px;
    }
}


</style>       
        <link rel="stylesheet" href="<?php echo base_url()?>pdf_assets/css/bootstrap.min.css">
        <div class="modal-body idbody" id="idbody">
            <div class="idbodyborder">
                <div class="row idrow1">
                                                     <?php
 $path2 =  'pdf_assets/images/logo.png';
 $type2 = pathinfo($path2, PATHINFO_EXTENSION);
 $data2 = file_get_contents($path2);
 $base642 = 'data:image/' . $type2 . ';base64,' . base64_encode($data2);
?>
                    <div class="col-md-12 col-xs-12 col-sm-3 l">
                        <div class="logo"><img src="<?php echo $base642; ?>" /></div>
                    </div>

                    <div class="col-md-12 col-xs-12 col-sm-9 chsdata r">
                        <h2 class="title">MAHARASHTRA CHESS ASSOCIATION</h2>
                        <p>Affiliated To All India Chess Federation</p>

                        <p>Regd. Office address � Pratik, S. No. 32/1, Plot No. 11, Behind Mehendale Garage,<br> Erandawane, Karve Road, Pune � 411004</p>
                        <p>Phone: +91 8007944674, Email: <a href="#">maharashtrachessassociation@gmail.com</a></p>
                    </div>
                </div>

                <div class="row ">
                 <!-- <div class="col-xs-1 col-sm-1 col-md-3" >
                      
                    </div>-->
                                       <?php
                $dpimg = $data[0]['image'];                  
 $path1 =  "assets/player_photos/$dpimg";
 $type1 = pathinfo($path1, PATHINFO_EXTENSION);
 $data1 = file_get_contents($path1);
 $base641 = 'data:image/' . $type1 . ';base64,' . base64_encode($data1);
?>
                    <div class="col-xs-5 col-sm-5 col-md-5" >
                       <div class="logo"> <img src="<?php echo $base641; ?>" width="147px"></div

                    </div>
                      <div class="idplayerphoto col-xs-3 col-sm-3 col-md-3" >
                        <b> <p>Name: <?php  echo strtoupper($data[0]['name']);?></p>
                        <p>MCA ID: <?php  echo $data[0]['registration_entry_no'];?></p>
                        <p>District: <?php  echo strtoupper($data[0]['district']);?></p>
                        <p>Valid upto : MARCH 2021</p>
                        <p>Type: PLAYER</p></b>
                        <br />
                      
                    </div>
                   <!--    <div class="col-xs-1 col-sm-1 col-md-3" >
                      
                    </div>-->
                     
            <div class="row idplydet">
               <hr />
               </div>
                <div class="col-xs-4 col-sm-5 col-md-5" >
                      
                    </div>
                    <?php
 $path =  'pdf_assets/images/GodboleSignature.jpg';
 $type = pathinfo($path, PATHINFO_EXTENSION);
 $data = file_get_contents($path);
 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
?>

                    <div class="col-xs-7 col-md-7 col-sm-7">
                       
                          <div class="logo">  <img src="<?php echo $base64; ?>" width="147px" />
                            <br>
                            <span>
                                Shri Niranjan Godbole<br />
                                Hon. Gen. Secretary <br />
                                Maharashtra Chess Association
                            </span>
                        </div>

                     
                    </div>
                     
                </div>
            </div>
        </div>
    </body></html>
