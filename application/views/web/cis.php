<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- Chess In School</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/hockey/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
    <?php require('includes/head.php');?>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2>Chess In School</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>Chess In School</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
   <!-- inner welcome Wrapper Start -->
     <div class="iner_about_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="about_slider_wrapper float_left">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/inner/cis1.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/inner/cis2.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/inner/cis3.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
							<div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/inner/cis4.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
							<div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/inner/cis13.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
							<div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/inner/cis14.jpg" class="img-responsive" alt="">
                                </div>
                            </div><div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/inner/cis3.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <!-- inner welcome Wrapper end -->
    <div class="latest_result_wrappwer float_left animate__animated animate__fadeInRight animate__delay-2s animate__repeat-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="ft_left_heading_wraper">
                        <br><h1>Chess In School</h1>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
					<p style="text-align:justify;">The chess in school program was started in the year 2012 by the Maharashtra Chess Association. 25 districts from Maharashtra have started with the CIS program. The CIS program helps in developing the game of chess at the grassroot level. It enables one entire school to get informed about the game.</p><br>
					<p><h5>Advantages of the CIS program:</h5></p><br>
					<p><ul>
						<li>1.&nbsp;Children become self-confident, there level of patience increases and there is improvement in decision making.</li>
						<li>2.&nbsp;Children develop qualities like sportsmanship and respect for others.</li>
						<li>3.&nbsp;For schools, children with talent and right guidance can bring laurels to the school.</li>
						<li>4.&nbsp;The school will be responsible for creating a “Smarter Generation”.</li>
						<li>5.&nbsp;As many students learn chess through the CIS program, chess clubs or academies can be setup in districts which will lead to more development of players.</li>
						<li>6.&nbsp;The chess clubs and academies can organize various tournaments and events which help greatly in talent identification.</li>
						<li>7.&nbsp;Because of the CIS programs, the number of participation of players will increase substantially for a district.</li>
						<li>8.&nbsp;CIS program helps in creating a chess culture for a district which will help increasing the number of players and quality of chess.</li>
						<li>9.&nbsp;CIS program not only helps in increasing the number of players but also helps in increasing in a number of trainers and coaches. </li>
					</ul>
					</p>
					<br>
					<p><h5>Features of CIS program:</h5></p>
					<p><ul>
						<li>1.&nbsp;The trainers of this program use a universal syllabus developed by FIDE.</li>
						<li>2.&nbsp;Certificates are given to students and schools registered under the CIS program.</li>
						<li>3.&nbsp;A train the trainers program is also conducted which helps in increasing the number of chess coaches in a district.</li>
						<li>4.&nbsp;The CIS program also has a digital presence. It helps in parents and schools to find out the progress of their children.</li>
						
					</ul></p>
	
				</div>
			</div>
        </div>
    </div>
	
	<!--our history wrapper start-->
    <div class="our_history_wrapper float_left animate__animated animate__fadeInLeft animate__delay-3s animate__repeat-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper gallery_heading_center text-center">
                        <h1>Information</h1>

                    </div>
                </div>
				<div class="col-md-12 col-sm-12">
                    <div class="table_next_race overflow-scroll">
                        <table style="border:1px solid black;">
                            <tr>
								<th>No.</th>
                                <th>Year</th>
                                <th>No. of Schools</th>
                                <th>No. of Districts</th>
                                <th>No. of Trainers</th>
                                
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>2012-13</td>
                                <td>72</td>
                                <td>20</td>
                                <td>53</td>
                                
                            </tr>
							<tr>
                                <td>1</td>
                                <td>2013-14</td>
                                <td>186</td>
                                <td>22</td>
                                <td>53</td>
                                
                            </tr>
							<tr>
                                <td>1</td>
                                <td>2014-15</td>
                                <td>235</td>
                                <td>24</td>
                                <td>70</td>
                                
                            </tr>
                        </table>

                    </div>

                </div>
            
                <div class="col-md-12 pdtpp">
                    <div class="welcome_tab">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home"> 2012-13</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link " data-toggle="tab" href="#menu1"> 2013-14</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#menu2"> 2014-15</a>
                            </li>
                            
                        </ul>
                    </div>
                    <div class="tab-content about_tab_content">
                        <div id="home" class="tab-pane active">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <p style="text-align:justify;"><span class="commentery_box">The CIS website and the demo boards to be distributed to schools were launched.<br><br>The first train the trainers program was held from 26th to 28th October 2012 at Jalgaon. Mr. Kevin O’Conell, Executive Secretary, FIDE-CIS Commission headed the program.<br><br>Mr. Ali Nihat Yazici, Vice President of FIE and Chairman of FIDE-CIS Program visited Pune on 17th and 18th February 2013 to promote and exchange ideas about the CIS program.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <p style="text-align:justify;"><span class="commentery_box">The annual report of 2012-13 was released by Shri Ashok Jain at a meeting in Jalgaon.<br><br>Given below are few photos of promotional activities done in different districts<br><br>Maharashtra Chess Day was celebrated in the fond memory of chess stalwart Shri Bhausaheb Padsalgikar in Jnana Prabodini Prashala in Pune on 7th September 2013. A simul was played by WGM Soumya Swaminathan, WGM Eesha Karavade, WGM Swati Ghate and Supriya Joshi against 60 students from different schools.<br>The Pune district CIS team organized an Inter School Team Chess Tournament amongst the schools registered under the CIS program.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <p style="text-align:justify;"><span class="commentery_box">Maharashtra Chess Day was celebrated in the fond memory of chess stalwart Shri Bhausaheb Padsalgikar in Pune on 7th September 2014. A simultaneous event was held in which 4 international players played against 100 students from CIS registered schools. The event was inaugurated by Mrs. Pushpa Ekbote, daughter of Late Shri Bhausaheb Padsalgikar.<br><br>A train the trainers program was held in Pune</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!--our history wrapper end-->
	
     <!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <!-- <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- prs patner slider End -->

    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>