<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- Events</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/inner/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
    <?php require('includes/head.php');?>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2> All Events</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li> All Events</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
    <!-- latest result wrapper start-->
    <!-- <div class="latest_result_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <h1>latest result</h1>

                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="upcoming_matches_wrapper float_left">
                        <div class="row">

                            <div class="col-md-3 col-sm-12 col-12">
                                <div class="match_list_wrapper as">
                                    <div class="match_list_img">
                                        <img src="<?php echo base_url()?>images/inner/team1.png" class="img-responsive" alt="logo">
                                        <h4>india</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="latest_match_box float_left">
                                    <p>Asian Cup 2018</p>
                                    <h1>INDIA VS pakistan, FINAL</h1>
                                    <h2>Dubai International Cricket Stadium, Dubai</h2>
                                    <h3>223/7 (50.0) - 222 (48.3)</h3>
                                    <h4>INDIA BEAT pakistan BY 3 WICKETS</h4>
                                </div>

                            </div>
                            <div class="col-md-3 col-sm-12 col-12">
                                <div class="match_list_wrapper match_wraper_2">
                                    <div class="match_list_img">
                                        <img src="<?php echo base_url()?>images/inner/team2.png" class="img-responsive" alt="logo">
                                        <h4>pakistan</h4>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> -->
    <!-- latest result wrapper end-->

    <!-- result wrapper start -->
    <div class="latest_result_wrappwer float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <!-- <h1>All Events</h1> -->
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="table_next_race result_match_wrapper overflow-scroll">
                        <table>
                            <tr>
                                <th>title</th>
                                <th>date</th>
                                <th>location</th>
                            </tr>
                            <tr>
                                <td>World School</td>
                                <td>01 May - 09 May</td>
                                <td>N/A	</td>
                            </tr>
                            <tr>
                                <td>PR Chess Academies 1st below 1600 fide rating Chess Tournament - 215835 / MAH / 2020</td>
                                <td>02 May - 04 May</td>
                                <td>Jilha parishad karmchari bhavan civil lines akola 444001</td>
                            </tr>
                            <tr>
                                <td>31st Cusat FIDE Rating Chess Tournament - 2020 - 245056 / KER / 2020</td>
                                <td>08 May - 11 May</td>
                                <td>Cusat Campus,Kochi ,KL</td>
                            </tr>
                            <tr>
                                <td>Lakecity Summer below 1600 FIDE Rating 2020 - 237118 / RAJ / 2020</td>
                                <td>09 May - 11 May</td>
                                <td>ORBIT RESORT , NEW BHOPALPURA, NEAR MIRAJ MALAHAR, RK CIRCLE, UDAIPUR (RAJASTHAN)</td>
                            </tr>
                            <tr>
                                <td>National Rapid 2020</td>
                                <td>12 May - 14 May	</td>
                                <td>N/A	</td>
                            </tr>
                            <tr>
                                <td>Maharashtra State Selection Under 25 Youth FIDE Rating Chess Championship 2020 - 249042 / MAH(S) / 2020</td>
                                <td>14 May - 17 May</td>
                                <td>Indira Gandhi Sankul,Korit Naka Road,Nandurbar,MH,425412</td>
                            </tr>
                            <tr>
                                <td>Late Sou Meenatai Shirgaokar FIDE Rating Open women's Chess Tournament - 246249 / MAH / 2020</td>
                                <td>14 May - 17 May</td>
                                <td>Dr.Bapat Bal Shikshan Mandir,Shivaji Mandal,Sangli,MH</td>
                            </tr>
                            <tr>
                                <td>Maharashtra State Selection Under - 13 Open & Girls FIDE Rating Chess Championship 2020 - 240834 /240835 / MAH(S) / 2020</td>
                                <td>14 May - 17 May</td>
                                <td>Indira Gandhi Sankul,Korit Naka Road,Nandurbar,MH,425412</td>
                            </tr>
                            <tr>
                                <td>National Blitz 2020</td>
                                <td>15 May - 15 May</td>
                                <td>N/A	</td>
                            </tr>
                            <tr>
                                <td>National Under-9 Boys & girls at Haryana</td>
                                <td>16 May - 24 May</td>
                                <td>N/A	</td>
                            </tr>
                            <tr>
                                <td>SHILLONG OPEN FIDE RATING CHESS TOURNAMENT 2020 - 245648 / MEG / 2020</td>
                                <td>18 May - 23 May</td>
                                <td>Adress - Laban community and sports complex Shillong - 793004 Meghalaya India</td>
                            </tr>
                            <tr>
                                <td>1st Time 2 Chess Rapid Rated Chess Tournament 2020 - 247642 / TN(R) / 2020</td>
                                <td>23 May - 24 May</td>
                                <td>D.G.Vaishnav College, No,883, Gokul Bagh, Poonamalle High Road, Arumbakkam, Chennai - 58</td>
                            </tr>
                            <tr>
                                <td>1st Time 2 Chess Blitz Rated Chess Tournament 2020 - 247643 / TN(B) / 2020</td>
                                <td>23 May - 24 May</td>
                                <td>D.G.Vaishnav College, No,883, Gokul Bagh, Poonamalle High Road, Arumbakkam, Chennai - 58</td>
                            </tr>
                            <tr>
                                <td>Tamilnadu State Open Chess Championship 2020 - 245134 / TN(S) / 2020</td>
                                <td>25 May - 29 May</td>
                                <td>Kongu Arts & Science College, Erode</td>
                            </tr>
                            <tr>
                                <td>National Under-17 Boys & Girls 2020</td>
                                <td>06 Jun - 14 Jun</td>
                                <td>N/A</td>
                            </tr>
                            <tr>
                                <td>1st SportsIndia - TKSS Rapid Rating Chess tmt - 2020 - 248536 / TN(R) / 2020</td>
                                <td>07 Jun - 07 Jun</td>
                                <td>Jawaharlal Nehru Stadium,Periamet, Chennai,600003</td>
                            </tr>
                            <tr>
                                <td>Mayors' Cup Chess Tmt 2020</td>
                                <td>07 Jun - 15 Jun</td>
                                <td>N/A</td>
                            </tr>
                            <tr>
                                <td>National Under-13 Boys & Girls 2020</td>
                                <td>16 Jun - 24 Jun</td>
                                <td>N/A</td>
                            </tr>
                            <tr>
                                <td>Commonwealth Chess Championship Boys & Girls</td>
                                <td>26 Jun - 05 Jul</td>
                                <td>N/A</td>
                            </tr>
                            <tr>
                                <td>LATE RAMESH VINAYAKRAO KOTWAL MEMORAIL OPEN RAPID CHESS CHAMPIONSHIP (FIDE RATING) 2020 - 249382 / 2020</td>
                                <td>27 Jun - 28 Jun</td>
                                <td>Shri Bhavbhuti Rang Mandir,Opp Saraswati Girls High School,Poona Toli,Gondia,MH,441601</td>
                            </tr>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- result wrapper end -->
    <!--trophy wrapper start-->
    <!-- <div class="trophy_wrapper float_left">
        <div class="dream_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="trophy_slider">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url()?>images/inner/tp1.png" alt="img" class="img-responsive">
                                    <p>2016</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url()?>images/inner/tp2.png" alt="img" class="img-responsive">
                                    <p>2017-2018</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url()?>images/inner/tp3.png" alt="img" class="img-responsive">
                                    <p>2014</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url()?>images/inner/tp4.png" alt="img" class="img-responsive">
                                    <p>2012</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!--trophy wrapper end-->

    <!--patner slider Start -->
    <!-- <div class="patner_main_section_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- prs patner slider End -->

    <div class="patner_main_section_wrapper float_left"></div>
    
    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>