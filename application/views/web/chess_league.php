<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- Chess League</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/hockey/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
    <?php require('includes/head.php');?>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2>Chess League</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp; </li>
                            <li>Chess League</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
   
	<!--gallery wrapper start-->
    <div class="portfolio_grid3 float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper left_gallery_heading">
                        <h1>media gallery</h1>

                    </div>

                     <ul class="protfoli_filter III_filter">
                        <li class="active" data-filter="*"><a href="#"> all</a></li>
                        <li data-filter=".chess_league"><a href="#">Chess League</a></li>
                        <li data-filter=".media"><a href="#">Media</a></li>
                    </ul>
                </div>
            </div>
        <div class="row portfoli_inner">
            <!-- Items -->
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/1.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/1.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Items -->
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/2.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/2.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/3.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/3.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/4.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/4.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/5.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/5.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/6.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/6.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/7.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/7.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/8.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/8.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/9.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/9.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/10.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/10.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/11.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/11.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/12.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/12.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/11MCL.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/11MCL.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/12MCL.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/12MCL.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/13MCL.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/13MCL.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/14MCL.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/14MCL.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/15MCL.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/15MCL.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/16MCL.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/16MCL.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<!--<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/13.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/13.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/14.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/14.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/15.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/15.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/16.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/16.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/17.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/17.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/18.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/18.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/19.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/19.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/20.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/20.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/21.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/21.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/22.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/22.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div><div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 chess_league *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/23.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/23.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- Items -->
             <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 media *">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/24.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/24.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Items -->
             <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 * media">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/25.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/25.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 * media">
                <div class="portfolio_item">
                    <img src="<?php echo base_url();?>images/inner/chess_league/26.jpg" alt="">
                    <div class="portfolio_hover">
                        <div class="zoom_popup">
                            <a class="img-link" href="<?php echo base_url();?>images/inner/chess_league/26.jpg"> <i class="flaticon-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Items -->
        </div>
    </div>
    </div>

    <!--gallery wrapper end-->
    <!-- inner welcome Wrapper Start -->
    <div class="iner_about_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="about_welcome_content float_left">
                        <h1>Welcome to Maharashta Chess League</h1>
                        <p style="text-align:justify;">The Maharashtra Chess League is the marquee tournament of the Maharashtra Chess Association. From the year 2013 onwards, MCA started with the Maharashtra Chess League. It is the only league format chess tournament in India. The idea to start this event was twofold; 1) To bring national level players in both, men and women section so that junior players are exposed to high level chess. 2) To put a fair amount of money in the pockets of the national level players to help them further their career. It has attracted many strong GM’s, WGM’s and IM’s to participate in this one-of-a-kind event. Male players such as GM Surya Shekhar Ganguly, GM K Sasikiran, GM S.P. Sethuraman, GM Vidit Gujrathi, GM B Adhiban, GM Abhijit Kunte, GM Abhijeet Gupta, etc. have competed for the best male player prize in MCL. In the female category, GM Koneru Humpy, IM Tania Sachdev, IM Eesha Karavade, WGM Soumya Swaminathan, WGM Mary Ann Gomes have fought to be crowned the best female player in MCL.</p>
                        <p style="text-align:justify;">The aim of organizing the Maharashtra Chess League is to grow and popularize the game of chess and the chess players in Maharashtra. Future champions are made through the MCL. As the League a high number of participation of GM’s, WGM’s and IM’s from around the country, young and budding players can improve their game just by watching these GM’s, WGM’s and IM’s in action. </p>
                        <p style="text-align:justify;">6 teams from six cities take part in the league. Each team consists of minimum 6 players which includes GM’s, WGM’s, IM’s and Rated players. The tournament is played in a round robin format after which a semi final and final is played over a period of 5 days. An auction of players is held at the beginning of every MCL to determine who will play for which team.</p>
                        <p style="text-align:justify;">The games of the MCL are broadcasted on its website and it has been receiving a significant number of online traffic since the day the league began. The MCL is considered by many players as the most professionally organized chess tournament in India.</p>
                        <p style="text-align:justify;">In the next few pages, the detailed history of the Maharashtra Chess League is given along with few photos from the past events.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- inner welcome Wrapper end -->
	<!-- result wrapper start -->
    <div class="latest_result_wrappwer float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <br><h1>MCL from 2013-2016</h1>
                    </div>
                </div>
				<div class="col-md-2 col-sm-2"></div>
                <div class="col-md-8 col-sm-8">
                    <div class="table_next_race result_match_wrapper overflow-scroll">
						<h2>MCL 2013</h2>
						<p>The 1st edition of the MCL was held from 24th April 2013 to 28th April 2013. Given below are the team rosters of the teams which took part in the inaugural edition.</p>
                        <hr>
						<h6>AHMEDNAGAR CHECKERS</h6>
						<h6>Owners – RBS Sports Ethix</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Gopal G N</td>
                                <td>Grand Master</td>
                                <td>Kerala</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>M R Venkatesh</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Tania Sachdev</td>
                                <td>Women Grand Master</td>
                                <td>Delhi</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Shardul Gagare</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Sagar Shah</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Mithil Ajgaonkar</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Rucha Pujari</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>JALGAON BATTLERS</h6>
						<h6>Owners – Jain Irrigation Systems</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Vidit Gujrathi</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Eesha Karavade</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>S Meenakshi</td>
                                <td>Women Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Narayanan Srinath</td>
                                <td>International Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Pratik Patil</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>S L Narayanan</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Sameer Kathmale</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>MUMBAI MOVERS</h6>
						<h6>Owners – Rishi Gupta</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S Arun Prasad</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>RR Laxman</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Kiran Manisha Mohanty</td>
                                <td>Women Grand Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Kruttika Nadig</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Ashwin Jayaram</td>
                                <td>International Master</td>
                                <td>Karnataka</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Anup Deshmukh</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Rakesh Kulkarni</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>8</td>
                                <td>Shashikant Kutwal</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>NAGPUR ROYALS</h6>
						<h6>Owners – Gaikwad Patil Group</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Tejas Bakre</td>
                                <td>Grand Master</td>
                                <td>Gujarat</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Sahaj Grover</td>
                                <td>Grand Master</td>
                                <td>Delhi</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Soumya Swaminathan</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Swapnil Dhopade</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Chinmay Kulkarni</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Abhishek Kelkar</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Shweta Gole</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>PUNE ATTACKERS</h6>
						<h6>Owners – Goel Ganga Group</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>M R Lalith Babu</td>
                                <td>Grand Master</td>
                                <td>Andhra Pradesh</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Padmini Rout</td>
                                <td>Women Grand Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Swathi Ghate</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Akshayraj Kore</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Himanshu Sharma</td>
                                <td>International Master</td>
                                <td>Haryana</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Amardeep Bartakke/td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							
						</table>
						<h6>THANE COMBATANTS</h6>
						<h6>Owners – MEP Infrastructures Developers Pvt. Ltd.</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Surya Shekhar Ganguly</td>
                                <td>Grand Master</td>
                                <td>West Bengal</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Mary Ann Gomes</td>
                                <td>Women Grand Master</td>
                                <td>West Bengal</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>S Vijaylaksmhi</td>
                                <td>Women Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Aditya Udeshi</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Prathamesh Mokal</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Abhimanyu Puranik</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Parnali Dharia</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>Final Standings</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Team</th>
                                <th>Points</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td><center>Pune Attackers</center></td>
                                <td>24.5</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td><center>Jalgaon Battlers</center></td>
                                <td>23.5</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td><center>Ahmednagar Checkers</center></td>
                                <td>21.5</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td><center>Nagpur Royals</center></td>
                                <td>17.5</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td><center>Thane Combatants</center></td>
                                <td>13.5</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td><center>Mumbai Movers</center></td>
                                <td>11.5</td>
                            </tr>
							
						</table>

                    </div>

                </div>
				<div class="col-md-2 col-sm-2"></div>
				
				<div class="col-md-2 col-sm-2"></div>
                <div class="col-md-8 col-sm-8">
                    <div class="table_next_race result_match_wrapper overflow-scroll">
						<h2>MCL 2014</h2>
						<p>The 2nd edition of the MCL was held from 11th June to 14th June 2014. Given below are the team rosters of the teams which took part in the inaugural edition.</p>
                        <hr>
						<h6>AHMEDNAGAR CHECKERS</h6>
						<h6>Owners – RBS Sports Ethix</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Abhijit Kunte</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Tejas Bakare</td>
                                <td>Grand Master</td>
                                <td>Gujarat</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Padmini Rout</td>
                                <td>Women Grand Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Shardul Gagare</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>N.R. Vignesh</td>
                                <td>Rated Player</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Abhishek Kelkar</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Aakanksha Hagawane</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>JALGAON BATTLERS</h6>
						<h6>Owners – Jain Irrigation Systems</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Vidit Gujrathi</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>B Adhiban</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Kiran Manisha Mohanty	</td>
                                <td>Women Grand Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Narayanan Srinath</td>
                                <td>International Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Pratik Patil</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Shashikant Kutwal</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Rucha Pujari</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>MUMBAI MOVERS</h6>
						<h6>Owners – Rishi Gupta</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Parimarjan Negi</td>
                                <td>Grand Master</td>
                                <td>New Delhi</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Eesha Karavade</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Bhakti Kulkarni</td>
                                <td>Women Grand Master</td>
                                <td>Goa</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Himanshu Sharma</td>
                                <td>International Master</td>
                                <td>Haryana</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Anup Deshmukh</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Rakesh Kulkarni</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Chinmay Kulkarni</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							
						</table>
						<h6>PUNE TRUMASTERS</h6>
						<h6>Owners - TruSpace</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S.P. Sethuraman</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>V Vishnu Prasanna</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Swathi Ghate</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Mary Ann Gomes</td>
                                <td>Women Grand Master</td>
                                <td>West Bengal</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Aditya Udeshi</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Sameer Kathmale</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Pawan Dodeja</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>PUNE ATTACKERS</h6>
						<h6>Owners – Goel Ganga Group</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>M R Lalith Babu</td>
                                <td>Grand Master</td>
                                <td>Andhra Pradesh</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Soumya Swaminathan</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Diptayan Ghosh</td>
                                <td>International Master</td>
                                <td>West Bengal</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Swapnil Dhopade</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>M.S. Thejkumar</td>
                                <td>International Master</td>
                                <td>Karnataka</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Sagar Shah</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Mithil Ajgaonkar</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>8</td>
                                <td>Supriya Joshi</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>THANE COMBATANTS</h6>
						<h6>Owners – MEP Infrastructures Developers Pvt. Ltd.</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Abhijeet Gupta</td>
                                <td>Grand Master</td>
                                <td>Rajasthan</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Tania Sachdev</td>
                                <td>Women Grand Master</td>
                                <td>New Delhi</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Prasanna Rao</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Vikramaditya Kulkarni</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>S L Narayanan</td>
                                <td>International Master</td>
                                <td>Kerala</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Abhimanyu Puranik</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Parnali Dharia</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>Final Standings</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Team</th>
                                <th>Points</th>
                            </tr>
                            
							<tr>
                                <td>1</td>
                                <td><center>Jalgaon Battlers</center></td>
                                <td>19.5</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td><center>Thane Combatants</center></td>
                                <td>15</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td><center>Ahmednagar Checkers</center></td>
                                <td>14.5</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td><center>Mumbai Movers</center></td>
                                <td>14.5</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td><center>Pune Attackers</center></td>
                                <td>13.5</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td><center>Pune TruMasters</center></td>
                                <td>13</td>
                            </tr>
							
							
							
						</table>

                    </div>

                </div>
				<div class="col-md-2 col-sm-2"></div>
				
				<div class="col-md-2 col-sm-2"></div>
                <div class="col-md-8 col-sm-8">
                    <div class="table_next_race result_match_wrapper overflow-scroll">
						<h2>MCL 2015</h2>
						<p>The 3rd edition of the MCL was held from 11th June to 14th June 2015. Given below are the team rosters of the teams which took part in the inaugural edition.</p>
                        <hr>
						<h6>AHMEDNAGAR CHECKERS</h6>
						<h6>Owners – RBS Sports Ethix</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>M Shyamsundar</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Abhijeet Gupta</td>
                                <td>Grand Master</td>
                                <td>Rajasthan</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>S L Narayanan</td>
                                <td>International Master</td>
                                <td>Kerala</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Shardul Gagare</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>N.R. Vignesh</td>
                                <td>Rated Player</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Pratik Patil</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Rucha Pujari</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>8</td>
                                <td>Aakanksha Hagawane</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>JALGAON BATTLERS</h6>
						<h6>Owners – Jain Irrigation Systems</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Vidit Gujrathi</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>B Adhiban</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Kiran Manisha Mohanty	</td>
                                <td>Women Grand Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Narayanan Srinath</td>
                                <td>International Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Rutuja Bakshi</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Nubairshah Shaikh</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							
						</table>
						<h6>MUMBAI MOVERS</h6>
						<h6>Owners – Rishi Gupta</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Koneru Humpy</td>
                                <td>Grand Master</td>
                                <td>Andhra Pradesh</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Vaibhav Suri</td>
                                <td>Grand Master</td>
                                <td>New Delhi</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Diptayan Ghosh</td>
                                <td>International Master</td>
                                <td>West Bengal</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Vikramaditya Kulkarni</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Rakesh Kulkarni</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Shalmali Gagare</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							
						</table>
						<h6>PUNE TRUMASTERS</h6>
						<h6>Owners - TruSpace</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S.P. Sethuraman</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Abhijit Kunte</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Swathi Ghate</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Mary Ann Gomes</td>
                                <td>Women Grand Master</td>
                                <td>West Bengal</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Swayams Mishra</td>
                                <td>International Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Abhishek Kelkar</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Shashikant Kutwal</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>PUNE ATTACKERS</h6>
						<h6>Owners – Goel Ganga Group</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>M R Venkatesh</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Padmini Rout</td>
                                <td>Women Grand Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Swapnil Dhopade</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>M.S. Thejkumar</td>
                                <td>International Master</td>
                                <td>Karnataka</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Parnali Dharia</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Aniruddha Deshpande</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							
						</table>
						<h6>THANE COMBATANTS</h6>
						<h6>Owners – MEP Infrastructures Developers Pvt. Ltd.</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Aravindh Chitambaram</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>M R Lalith Babu</td>
                                <td>Grand Master</td>
                                <td>Andhra Pradesh</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>K Rathanakaran</td>
                                <td>International Master</td>
                                <td>Kerala</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Soumya Swaminathan</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Eesha Karavade</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Abhimanyu Puranik</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Chinmay Kulkarni</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>Final Standings</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Team</th>
                                <th>Points</th>
                            </tr>
                            
							
							<tr>
                                <td>1</td>
                                <td><center>Thane Combatants</center></td>
                                <td>23</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td><center>Ahmednagar Checkers</center></td>
                                <td>21</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td><center>Pune TruMasters</center></td>
                                <td>21</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td><center>Jalgaon Battlers</center></td>
                                <td>17.5</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td><center>Mumbai Movers</center></td>
                                <td>13.5</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td><center>Pune Attackers</center></td>
                                <td>12.5</td>
                            </tr>
							
							
							
						</table>

                    </div>

                </div>
				<div class="col-md-2 col-sm-2"></div>
				
				<div class="col-md-2 col-sm-2"></div>
                <div class="col-md-8 col-sm-8">
                    <div class="table_next_race result_match_wrapper overflow-scroll">
						<h2>MCL 2016</h2>
						<p>The 4th edition of the MCL was held from 11th June to 14th June 2016. Given below are the team rosters of the teams which took part in the inaugural edition.</p>
                        <hr>
						<h6>AHMEDNAGAR CHECKERS</h6>
						<h6>Owners – RBS Sports Ethix</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Shardul Gagare</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Krishnan Sasikiran</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Bhakti Kulkarni</td>
                                <td>Women Grand Master</td>
                                <td>Goa</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Tania Sachedv</td>
                                <td>International Master</td>
                                <td>New Delhi</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Sagar Shah</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Shashikant Kutwal</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Pawan Dodeja</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>8</td>
                                <td>Sankarsha Shelke</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>JALGAON BATTLERS</h6>
						<h6>Owners – Jain Irrigation Systems</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S L Narayanan</td>
                                <td>Grand Master</td>
                                <td>Kerala</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Koneru Humpy</td>
                                <td>Grand Master</td>
                                <td>Andhra Pradesh</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Swapnil Dhopade	</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Narayanan Srinath</td>
                                <td>International Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Aakanksha Hagawane</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Pratik Patil</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Rutuja Bakshi</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>8</td>
                                <td>Nubairshah Shaikh</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>MUMBAI MOVERS</h6>
						<h6>Owners – Rishi Gupta</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Abhijeet Gupta</td>
                                <td>Grand Master</td>
                                <td>Rajasthan</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Diptayan Ghosh</td>
                                <td>Grand Master</td>
                                <td>West Bengal</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Swathi Ghate</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Vikramaditya Kulkarni</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Rakesh Kulkarni</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>R Vaishali</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Saloni Sapale</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>8</td>
                                <td>Raunak Sadhwani</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>PUNE TRUMASTERS</h6>
						<h6>Owners - TruSpace</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S.P. Sethuraman</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>V Vishnu Prasanna</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Eesha Karavade</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Abhishek Kelkar</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Swayams Mishra</td>
                                <td>International Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Harshit Raja</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>Rucha Pujari</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>PUNE NOK-99 NAVIGATORS</h6>
						<h6>Owners – Hemangi Gupta, Girish Chitale and Ashish Desai</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>B Adhiban</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>Abhijit Kunte</td>
                                <td>Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Mary Ann Gomes</td>
                                <td>Women Grand Master</td>
                                <td>West Bengal</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Sameer Kathmale</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Aditya Udeshi</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Akash Thakur</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>7</td>
                                <td>V Varshini</td>
                                <td>Rated Player</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>8</td>
                                <td>Ananya Gupta</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
						</table>
						<h6>THANE COMBATANTS</h6>
						<h6>Owners – MEP Infrastructures Developers Pvt. Ltd.</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Player</th>
                                <th>Category</th>
                                <th>State</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Aravindh Chitambaram</td>
                                <td>Grand Master</td>
                                <td>Tamil Nadu</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td>M R Lalith Babu</td>
                                <td>Grand Master</td>
                                <td>Andhra Pradesh</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td>Abhimanyu Puranik</td>
                                <td>International Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td>Padmini Rout</td>
                                <td>International Master</td>
                                <td>Orissa</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td>Soumya Swaminathan</td>
                                <td>Women Grand Master</td>
                                <td>Maharashtra</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td>Aniruddha Deshpande</td>
                                <td>Rated Player</td>
                                <td>Maharashtra</td>
                            </tr>
							
						</table>
						<h6>Final Standings</h6><br>
						<table>
                            <tr>
                                <th>No</th>
                                <th>Team</th>
                                <th>Points</th>
                            </tr>
							<tr>
                                <td>1</td>
                                <td><center>Pune Nok-99 Navigators</center></td>
                                <td>27.5</td>
                            </tr>
							<tr>
                                <td>2</td>
                                <td><center>Jalgaon Battlers</center></td>
                                <td>24</td>
                            </tr>
							<tr>
                                <td>3</td>
                                <td><center>Pune TruMasters</center></td>
                                <td>20.5</td>
                            </tr>
							<tr>
                                <td>4</td>
                                <td><center>Mumbai Movers</center></td>
                                <td>17</td>
                            </tr>
							<tr>
                                <td>5</td>
                                <td><center>Ahmednagar Checkers</center></td>
                                <td>12.5</td>
                            </tr>
							<tr>
                                <td>6</td>
                                <td><center>Thane Combatants</center></td>
                                <td>12.5</td>
                            </tr>
							
							
						</table>

                    </div>

                </div>
				<div class="col-md-2 col-sm-2"></div>
			
			</div>
        </div>
    </div>
    <!-- result wrapper end -->
    
     <!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <!-- <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo01.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo02.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo03.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo04.png" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url();?>images/inner/logo05.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- prs patner slider End -->

    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
	<script>
        $('.zoom_popup').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function(item) {
                    return item.el.attr('title') + '<small></small>';
                }
            }
        });
    </script>
</body>

</html>