<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>Maharashtra Chess Association- About Us</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="The Maharashtra Chess Association came into existence in 1963. Maharashtra Chess Association is the apex governing body for Chess in Maharashtra. ">
    <meta name="keywords" content="Maharashtra Chess Association, MCA, Chess in Maharashtra, India Chess Federation, Chess Ahmednagar, Tournament calendar, maharashtra chess association registration, maharashtra state chess association, all marathi chess association maharashtra">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="4 days">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/hockey/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
    <?php require('includes/head.php');?>
    <style>
        .iner_about_wrapper {
            padding-top: 30px;
        }
    </style>
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2>About Us</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>About Us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
   <!-- inner welcome Wrapper Start -->
    <div class="iner_about_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="about_slider_wrapper float_left animate__animated animate__fadeInLeft animate__delay-2s animate__repeat-1">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/gallery/1.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/inner/18.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="about_image">
                                    <img src="<?php echo base_url();?>images/gallery/2.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="about_welcome_content float_left animate__animated animate__fadeInRight animate__delay-2s animate__repeat-1">
                        <h1>Welcome to Maharashta Chess</h1>
                        <p style="text-align:justify;">The Maharashtra Chess Association came into existence in the year 1963 after the Bombay Chess Association and Southern Maratha Country Chess Association decided to merge and formed a unified Maharashtra Chess Association. <br>Maharashtra Chess Association (MCA) is the apex governing body for Chess in Maharashtra. It is affiliated to All India Chess Federation, which is recognized as the National Sports Federation by Government of India.<br>MCA aims to spread the game all over Maharashtra to make it bigger and better.
                        To achieve the aim, MCA organizes and conducts various Chess Events and Activities all over Maharashtra.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- inner welcome Wrapper end -->

    <!--our history wrapper start-->
    <!-- <div class="our_history_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper gallery_heading_center text-center">
                        <h1>our history</h1>

                    </div>
                </div>
                <div class="col-md-12 pdtpp">
                    <div class="welcome_tab">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#home"> year 2013</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#menu1"> year 2014</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#menu2"> year 2015</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#menu3">  year 2016</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#menu4">  year 2017</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#menu5">  year 2018</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#menu6">  year 2019</a>
                            </li>
                            
                        </ul>
                    </div>
                    <div class="tab-content about_tab_content">
                        <div id="home" class="tab-pane active">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <div class="ft_offer_img float_left">
                                            <img src="<?php echo base_url();?>images/inner/21.jpg" alt="img" class="img-responsive">
                                        </div>
                                        <p><span class="commentery_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <div class="ft_offer_img float_left">
                                            <img src="<?php echo base_url();?>images/inner/9.jpg" alt="img" class="img-responsive">
                                        </div>
                                        <p><span class="commentery_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <div class="ft_offer_img float_left">
                                            <img src="<?php echo base_url();?>images/inner/9.jpg" alt="img" class="img-responsive">
                                        </div>
                                        <p><span class="commentery_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <div class="ft_offer_img float_left">
                                            <img src="<?php echo base_url();?>images/inner/14.jpg" alt="img" class="img-responsive">
                                        </div>
                                        <p><span class="commentery_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu4" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <div class="ft_offer_img float_left">
                                            <img src="<?php echo base_url();?>images/inner/19.jpg" alt="img" class="img-responsive">
                                        </div>
                                        <p><span class="commentery_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu5" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <div class="ft_offer_img float_left">
                                            <img src="<?php echo base_url();?>images/inner/20.jpg" alt="img" class="img-responsive">
                                        </div>
                                        <p><span class="commentery_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu6" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <div class="ft_offer_img float_left">
                                            <img src="<?php echo base_url();?>images/inner/22.jpg" alt="img" class="img-responsive">
                                        </div>
                                        <p><span class="commentery_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu7" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="offer_main_boxes_wrapper float_left">

                                        <div class="ft_offer_img float_left">
                                            <img src="<?php echo base_url();?>images/inner/10.jpg" alt="img" class="img-responsive">
                                        </div>
                                        <p><span class="commentery_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!--our history wrapper end-->
	
	
	<!-- result wrapper start -->
    <div class="iner_about_wrapper float_left">
        <div class="container">
            <div class="row team-section text-center my-5">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <h1 class="animate__animated animate__fadeInLeft animate__delay-3s animate__repeat-1">Our Champions</h1>
                    </div>
                </div>
				<!-- Grid row -->
				  <div class="row text-center">

					<!-- Grid column -->
					<div class="col-md-4 mb-md-0 mb-5  animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1">
					  <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/pravin.png" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">GM Pravin Thipsay</h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Mumbai</strong></h6>
					  <h6 class="grey-text mb-3"><strong>1996</strong></h6>
					</div>
					<!-- Grid column -->

					<!-- Grid column -->
					 <div class="col-md-4 mb-md-0 mb-5  animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1">
					   <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/abhijit.png" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">GM Abhijit Kunte</h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Pune </strong></h6>
					  <h6 class="grey-text mb-3"><strong>2000</strong></h6>
					</div>
					<!-- Grid column -->

					<!-- Grid column -->
					 <div class="col-md-4 mb-md-0 mb-5  animate__animated animate__fadeInLeft animate__delay-4s animate__repeat-1">
					  <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/vidit.jpg" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">GM Vidit Gujrathi </h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Nashik</strong></h6>
					  <h6 class="grey-text mb-3"><strong>2013</strong></h6>
					</div>
					<!-- Grid column -->
					<!-- Grid column -->
					 <div class="col-md-4 mb-md-0 mb-5 animate__animated animate__fadeInRight animate__delay-4s animate__repeat-1">
					  <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/soumya.jpg" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">WGM Soumya Swaminathan</h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Pune</strong></h6>
					  <h6 class="grey-text mb-3"><strong>2013</strong></h6>
					</div>
					<!-- Grid column -->
					<!-- Grid column -->
					 <div class="col-md-4 mb-md-0 mb-5 animate__animated animate__fadeInRight animate__delay-4s animate__repeat-1">
					  <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/swapnil.jpg" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">GM Swapnil Dhopade</h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Amravati</strong></h6>
					  <h6 class="grey-text mb-3"><strong>2015</strong></h6>
					</div>
					<!-- Grid column --><!-- Grid column -->
					 <div class="col-md-4 mb-md-0 mb-5 animate__animated animate__fadeInRight animate__delay-4s animate__repeat-1">
					  <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/shardul.jpg" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">GM Shardul Gagare</h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Ahmednagar</strong></h6>
					  <h6 class="grey-text mb-3"><strong>2016</strong></h6>
					</div>
					<!-- Grid column -->
					<!-- Grid column -->
					 <div class="col-md-4 mb-md-0 mb-5  animate__animated animate__fadeInLeft animate__delay-5s animate__repeat-1">
					  <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/abhimanyu.jpg" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">GM Abhimanyu Puranik</h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Pune</strong></h6>
					  <h6 class="grey-text mb-3"><strong>2017</strong></h6>
					</div>
					<!-- Grid column -->
					<!-- Grid column -->
					 <div class="col-md-4 mb-md-0 mb-5  animate__animated animate__fadeInLeft animate__delay-5s animate__repeat-1">
					  <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/swathi.jpg" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">WGM Swathi Ghate </h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Pune</strong></h6>
					  <h6 class="grey-text mb-3"><strong>2003</strong></h6>
					</div>
					<!-- Grid column -->
					<!-- Grid column -->
					 <div class="col-md-4 mb-md-0 mb-5  animate__animated animate__fadeInLeft animate__delay-5s animate__repeat-1">
					  <div class="avatar mx-auto">
						<img src="<?php echo base_url();?>images/inner/champions/esha.jpg" class="rounded z-depth-1-half" alt="Sample avatar">
					  </div>
					  <h4 class="font-weight-bold dark-grey-text my-4">WGM Eesha Karavade</h4>
					  <h6 class="text-uppercase grey-text mb-3"><strong>Pune</strong></h6>
					  <h6 class="grey-text mb-3"><strong>2005</strong></h6>
					</div>
					<!-- Grid column -->

				  </div>
				  <!-- Grid row -->
					<!-- Grid row -->
				  
            </div>
        </div>
    </div>

    <!-- result wrapper end -->
   
	
	
    <!-- counter wrapper start-->
    <!-- <div class="counter_section float_left">
        <div class="dream_overlay"></div>
        <div class="counter-section">
            <div class="container text-center">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="counter_cntnt_box">
                            <div class="tb_icon">
                                <div class="icon"> <a href="#"><i class="fa fa-star-o"></i></a>
                                </div>
                            </div>
                            <div class="count-description"><span class="timer">230</span>+
                                <h5 class="con1"> Matches Played</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="counter_cntnt_box">
                            <div class="tb_icon">
                                <div class="icon"> <a href="#"><i class="fa fa-futbol-o"></i></a>
                                </div>
                            </div>
                            <div class="count-description"> <span class="timer">89</span>+
                                <h5 class="con2">Touchdowns</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="counter_cntnt_box">
                            <div class="tb_icon">
                                <div class="icon"> <a href="#"><i class="fa fa-users"></i></a>
                                </div>
                            </div>
                            <div class="count-description"> <span class="timer">60</span>+
                                <h5 class="con2">team members</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="counter_cntnt_box">
                            <div class="tb_icon">
                                <div class="icon"> <a href="#"><i class="flaticon-trophy"></i></a>
                                </div>
                            </div>
                            <div class="count-description"> <span class="timer">27</span>+
                                <h5 class="con4">awards won</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- counter wrapper end-->
	
    <!-- testimonial section Start -->
    <div class="testi_main_section_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper animate__animated animate__bounce animate__delay-5s animate__repeat-3">
                        <h1>our testimonial</h1>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="testi_slider_wrapper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="testi_slider_cont_main_wrapper">
                                    <div class="testi_slider_img">
                                        <img src="<?php echo base_url();?>images/chess/anand.jpg" alt="Vanand">
                                    </div>
                                    <div class="testi_slider_img_cont">
                                        <!-- <h1>“ Lorem ipsum dolor sit amet ”</h1> -->
                                        <p>
                                            “ There are a lot Chess in School Programs in India, but it is nice to hear that in Maharashtra this program is being
                                            taken beyond the urban strongholds and into the rural areas. Imagine if all starts to flow smoothly, you get into chess when you
                                            are in the school, then you get a chance to play in some of the leagues, it gives you a pathway to success and also supports the
                                            development of the game. I only have words of praise for the activities done here in Maharashtra!!!”
                                        </p>
                                        <h2><span>- Extract from V. Anand’s Speech regarding CIS at Maharashtra Chess League’s Launch in 2013</span>&nbsp;&nbsp;</h2>
                                        <!-- <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        </ul> -->
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testi_slider_cont_main_wrapper">
                                    <div class="testi_slider_img">
                                        <img src="<?php echo base_url();?>images/chess/AliNihatYazici.jpg" alt="AliNihatYazici">
                                    </div>
                                    <div class="testi_slider_img_cont">
                                        <!-- <h1>“ Lorem ipsum dolor sit amet ”</h1> -->
                                        <p>
                                            “The CIS model here is one of the best in the world. The CIS Program is a social project and there is no discussion
                                            about what it gives to the children through intellectual development, social development and paedeological development. We
                                            have to give this to the children as a gift so that they grow up to be a better individual. FIDE aims to develop Chess at local,
                                            national and international level through CIS. FIDE aims to make the Federations richer, stronger and in turn make the game
                                            more popular. I believe you should aim for 5000 School till 2015 as you are a big country and have achieved remarkable things
                                            in a short span. FIDE believes that its Motto – ‘One Clever Billion’ can be fulfilled in India alone.”
                                        </p>
                                        <h2><span>- Extract from Ali Niaht Yazici’s Speech at a CIS function in Pune in 2013</span>&nbsp;&nbsp;</h2>
                                        <!-- <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        </ul> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- testimonial section End -->
    
    <!--trophy wrapper start-->
    <!-- <div class="trophy_wrapper float_left">
        <div class="dream_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="trophy_slider">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url();?>images/inner/tp1.png" alt="img" class="img-responsive">
                                    <p>2013</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url();?>images/inner/tp2.png" alt="img" class="img-responsive">
                                    <p>2014</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url();?>images/inner/tp3.png" alt="img" class="img-responsive">
                                    <p>2015</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url();?>images/inner/tp4.png" alt="img" class="img-responsive">
                                    <p>2016</p>

                                </div>
                            </div>
							<div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url();?>images/inner/tp4.png" alt="img" class="img-responsive">
                                    <p>2017</p>

                                </div>
                            </div>
							<div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url();?>images/inner/tp4.png" alt="img" class="img-responsive">
                                    <p>2018</p>

                                </div>
                            </div>
							<div class="item">
                                <div class="trophyslider_wrapper float_left">
                                    <img src="<?php echo base_url();?>images/inner/tp4.png" alt="img" class="img-responsive">
                                    <p>2019</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!--trophy wrapper end-->
    
    <!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <h1>our sponsors</h1>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Chitale.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Jain.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Amanora.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/nf.png" alt="patner_img">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs patner slider End -->
    
    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>