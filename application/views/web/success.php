<!DOCTYPE html>
<html lang="zxx">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <title>MCA- Registration Success</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="sport,sporteyz" />
    <meta name="keywords" content="sport,sporteyz" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>images/hockey/favicon.png" />
	<link rel="stylesheet" href="<?php echo base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url()?>flaticon/football/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/customScrollbar.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/inner_style.css">
</head>

<body>

    <!-- preloader Start -->
    <?php require('includes/preloader.php');?>

    <?php require('includes/sidebar.php');?>

    <!-- navi wrapper Start -->
    <?php require('includes/nav.php');?>
    <!-- navi wrapper End -->
    
    <!-- inner Title Start -->
    <div class="indx_title_main_wrapper float_left">
        <div class="title_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_left_wrapper">
                        <h2>Registration Success</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 full_width">
                    <div class="indx_title_right_wrapper">
                        <ul>
                            <li><a href="index_football.html">Home</a> &nbsp;&nbsp;&nbsp;> </li>
                            <li>Registration Success</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- inner Title End -->
   <!-- inner welcome Wrapper Start -->
    <div class="iner_about_wrapper float_left">
        <div class="container">
            <div class="row">
            
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="about_welcome_content float_left">
                        <h1>
				 Registration Successfull</h1>
                        <p style="text-align:justify;">You have sucessfully registered in the Maharashtra Chess Association.<br>
              Your confirmation have been send on registered Email and SMS. Please check your Email and SMS notifications.
</p>
						
               
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- inner welcome Wrapper end -->
    <!--our history wrapper start-->
   
    <!--our history wrapper end-->
	<!-- result wrapper start -->
   <!-- <div class="latest_result_wrappwer float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ft_left_heading_wraper">
                        <h1>Committee members of the MCA</h1>

                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="table_next_race overflow-scroll">
                        <table>
                            <tr>
								<th>Logo</th>
                                <th>Name</th>
                                <th>Post</th>
                                <th>City</th>
                            </tr>
                            <tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" ></span>

                                </td>
                                <td>Shri Siddharth Mayur</td>
                                <td>President</td>
                                <td>Jalgaon</td>
                                
                            </tr>
                            <tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Abhijit Kunte</td>
                                <td>Senior Vice President</td>
                                <td>Pune</td>
                                
                            </tr>
                            <tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Girish Chitale </td>
                                <td>Vice President</td>
                                <td>Pune</td>
                                

                            </tr>
                            <tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Girish Vyas</td>
                                <td>Vice President</td>
                                <td>Nagpur</td>
                                

                            </tr>
                            <tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Aniruddha Deshpande</td>
                                <td>Vice President</td>
                                <td>Pune</td>
                                
                            </tr>
                            <tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Narendra Firodia </td>
                                <td>Vice President </td>
                                <td>Ahmednagar</td>
                                

                            </tr>
							<tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Vinay Bele               </td>
                                <td>Vice President </td>
                                <td>Nashik</td>
                                

                            </tr>
							<tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri P.B. Bhilare</td>
                                <td>Vice President </td>
                                <td>Mumbai</td>
                                

                            </tr>
							<tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Ninant Pednekar </td>
                                <td>Interim Hon. Gen. Secretary  </td>
                                <td>Thane</td>
                                

                            </tr><tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Faruk Shaikh</td>
                                <td>Hon. Treasurer </td>
                                <td> Jalgaon</td>
                                

                            </tr><tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Pratik Ghogare      </td>
                                <td>Joint Secretary               </td>
                                <td>Amravati   </td>
                                

                            </tr>
							<tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Ankush Raktade   </td>
                                <td>Joint Secretary                       </td>
                                <td>Buldhana</td>
                                

                            </tr>
							<tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Yashwant Bapat    </td>
                                <td>Joint Secretary                       </td>
                                <td>Ahmednagar</td>
                                

                            </tr>
							<tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Chandrakant Walavade       </td>
                                <td>Joint Secretary                       </td>
                                <td>Sangli</td>
                                

                            </tr>
							<tr>
                                <td>
                                    <span class="team"><img src="<?php echo base_url();?>images/inner/favicon.png" width="40" height="40" alt="team-logo"></span>

                                </td>
                                <td>Shri Hemendra Patel    </td>
                                <td>Joint Secretary                       </td>
                                <td>Aurangabad</td>
                                

                            </tr>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
-->
    <!-- result wrapper end -->
   
	
	
  
	
  
    <!-- testimonial section End -->

    <!--patner slider Start -->
    <div class="patner_main_section_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pn_slider_wraper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Chitale.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Jain.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/Amanora.jpg" alt="patner_img">
                                </div>
                            </div>
                            <div class="item">
                                <div class="pn_img_wrapper">
                                    <img src="<?php echo base_url()?>images/chess/nf.png" alt="patner_img">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- prs patner slider End -->

    <!-- footer Wrapper start -->
    <?php require('includes/footer.php')?>
    <!--footer wrapper end-->

    <!-- js files start -->

    <script src="<?php echo base_url()?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/modernizr.js"></script>
    <script src="<?php echo base_url()?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url()?>js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url()?>js/customScrollbar.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url()?>js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url()?>js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url()?>js/cursor.js"></script>
    <script src="<?php echo base_url()?>js/main.js"></script>
    <!-- custom js-->
</body>

</html>