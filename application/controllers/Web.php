<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {
	public function index()
	{
		$this->load->view('web/home');
	}

	public function events()
	{
		$this->load->view('web/events');
	}
	
	public function about_us()
	{
		$this->load->view('web/about_us');
	}
	
	public function contact_us()
	{
		$this->load->view('web/contact_us');
	}
	public function cis()
	{
		$this->load->view('web/cis');
	}
	public function committee()
	{
		$this->load->view('web/committee');
	}
	public function chess_league()
	{
		$this->load->view('web/chess_league');
	}
	public function sponsors()
	{
		$this->load->view('web/sponsors');
	}
	public function cis_success_stories(){
		$this->load->view('web/cis-success-stories');
	}
	public function international_chess_calendar(){
		$this->load->view('web/international-chess-calendar');
	}
	public function wjcc(){
		$this->load->view('web/wjcc');
	}
	public function search_player()
	{
		$this->load->view('web/search_player');
	}
	public function champions()
	{
		$this->load->view('web/honoring-our-champions');
	}

	public function press_media()
	{
		$this->load->view('web/press-media');
	}
	public function jewels()
	{
		$this->load->view('web/jewels');
	}
	public function train_the_trainers()
	{
		$this->load->view('web/train-the-trainer');
	}
	public function sponsor_a_tournament()
	{
		$this->load->view('web/sponsor_a_tournament');
	}
}
