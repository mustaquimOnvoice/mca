<?php
require_once APPPATH . 'core/class.phpmailer.php';
require_once APPPATH . 'core/class.smtp.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	public function message_send_bulk($message1, $number) {
		//$numsStr = implode(',',$number);
		$postData = "{ 
			\"sender\": \"MHCHES\", 
			\"route\": \"4\", 
			\"country\": \"91\", 
			\"unicode\": \"1\",
			\"sms\": [ 
						{ 
							\"message\": \"$message1\", 
							\"to\": [ $number ] 
						}
					] 
		}";
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_HTTPHEADER => array(
			"authkey: 228445AqdIYICptZd5d36a66d",
			"content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		   "cURL Error #:" . $err;
		} else {
		  $response;
		}
	}


	public function index()
	{
		$this->load->view('web/registration');
	}

	public function validmno()
	{  
	   $mno = $_POST['mno'];
		 $data = $this->db->query("select *  from tbl_players where mno='$mno'")->result_array();
		 $cnt = count($data);
		 if($cnt>'0'){
		 	echo 'exist';
		 }else{
		 	echo 'not exist';
		 }
	}

	public function validemail()
	{  

	   $email = $_POST['email'];
		 $data = $this->db->query("select *  from tbl_players where email='$email'")->result_array();
		
		 $cnt1 = count($data);
		 if($cnt1>'0'){
		 	echo 'exist';
		 }else{
		 	echo 'not exist';
		 }
	}

	public function payment_fail(){
		$this->load->view('web/failure');
    }

	public function confirm_form()
	{ 
	/*echo "<pre>";
		print_r($_FILES); die;*/
         
         $file_name=time().$_FILES["image"]["name"];
		 $target_dir = './assets/player_photos/';
		 $target_file = $target_dir.$file_name;
		if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)){
		 
			   	$insert_array	=	array(
											'name'  						=> $this->input->post('name'),
											'parent_name'   				=> $this->input->post('parent_name'),
											'address'   					=> $this->input->post('address'),
											'mno'   						=> $this->input->post('mno'),
											'email'   						=> $this->input->post('email'),
											'dob'   						=> date('Y-m-d', strtotime($this->input->post('dob'))),
											'district' 						=> $this->input->post('district'),
											'fide_rating'					=> $this->input->post('fide_rating'),
											'fide_id_no'   					=> $this->input->post('fide_id_no'),
											'aicf_id_no'   					=> $this->input->post('aicf_id_no'),
											'mca_id_no'  	 				=> $this->input->post('mca_id_no'),
											'image'							=> $file_name,
											'titles'   						=> $this->input->post('titles'),
											'disciplinary_action'   		=> $this->input->post('disciplinary_action')
										 );
			  
			  /* 	echo "<pre>";
				print_r($insert_array);
				die;*/
		       $this->db->insert('tbl_players_temp',$insert_array);
			      $price_array= array('price' => '1');//150

			   $merge = array_merge($insert_array, $price_array);
			 
			   	$data['records'] = 	$merge;
			   	$this->load->view('web/confirmation_form',$data);
		}
	}


public function payment_success()
	{  
		 
    //  print_r($_POST);die;
    $email =  $_POST['udf2'];
	  $phone =	$_POST['udf3'];
	  $txnid =	$_POST['txnid'];
	   /*$email =  "danish@onevoicetransmedia.com";
	  $phone =	"9762379774";
	  $txnid =	"810356";*/
	  $datau = $this->db->query("select *  from tbl_players where mno='$phone' and email='$email' and txnId='$txnid' order by `player_id` desc")->result_array();
	 $cntu = count($datau); 
	 if($cntu>0){
	 	
	     	$this->load->view('web/success');
		
	 }else{
	 
	 $data = $this->db->query("select *  from tbl_players_temp  where  `mno`='$phone' and  `email`='$email' order by `player_temp_id` desc")->result_array();
	
	/* echo '<pre>';
	 print_r($data);
	 die;*/
	 $cnt = count($data);

	 	if($cnt>0){
	 			// code for bibcode
	 	
	//	$cat =$data[0]['category'];
		 $result = $this->db->query("SELECT MAX(`counter_entry_no`) AS `counter_entry_no` FROM `tbl_players`")->row();
		$entry_no = $result->counter_entry_no;
		
		if($entry_no==''){
             $registration_entry_no='0';
		}else{
			$registration_entry_no =$entry_no;
		}
         $final_registration_entry_no = $registration_entry_no+1;
         $count_entry_no = $entry_no+1; // counting entries
			$insert_array1	=	array(

										'name'  						=> $data[0]['name'],
										'parent_name'  					=> $data[0]['parent_name'],
										'address'   					=> $data[0]['address'],
										'mno'   						=>$data[0]['mno'],
										'email'   						=> $data[0]['email'],
										'dob'   						=> date("Y-m-d", strtotime($data[0]['dob'])),
										'district'     					=>$data[0]['district'],
										'fide_rating'   				=> $data[0]['fide_rating'],
										'fide_id_no'   					=> $data[0]['fide_id_no'],
										'aicf_id_no'   					=> $data[0]['aicf_id_no'],
										'mca_id_no'   					=> $data[0]['mca_id_no'],
										'image'   					    => $data[0]['image'],
										'titles'   						=> $data[0]['titles'],
										'disciplinary_action' 			=> $data[0]['disciplinary_action'],
										'txnId'  					 	=> $txnid,
										'membership_date'  				=> date("Y-m-d h:i:s"),
										'counter_entry_no'  			=> $count_entry_no,
										'registration_entry_no' 	  	=> 'M20'.$final_registration_entry_no
										
										);

			$pagedata['data']=$insert_array1;
      	$this->db->insert('tbl_players',$insert_array1);
       // $emailinvoice = $this->load->view('invoicePdf',$pagedata,true);
     
       	$pagedata1['data'] = $this->db->query("select *  from tbl_players where mno='$phone' and email='$email' and registration_entry_no='M20$final_registration_entry_no'")->result_array();
     
       
        
       
    
        $this->load->library('html2pdf');
	     
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./assets/pdfs/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('Idcard.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	  /*  $data = array(
	    	'title' => 'PDF Created',
	    	'message' => 'Hello World!'
	    );*/
	
	    //Load html view
	     /*echo  $emailinvoice1 = $this->load->view('web/Idcardpdf',$pagedata1,TRUE);
        die;*/
    
	    $this->html2pdf->html($this->load->view('web/Idcardpdf',$pagedata1,TRUE));
	
	       $path = $this->html2pdf->create('save'); 
	
       
       
        	 $name = $data[0]['name']; 
		  
	
		//	$ride = $data[0]['category'];
		    $mno = $phone;	
		    
		    $emailinvoice = "Congratulations! You Have Successfully Registered For Maharashtra Chess Association";
		    $msg = "Congratulations! You Have Successfully Registered For Maharashtra Chess Association. Your Registration ID is: M20$final_registration_entry_no  . Keep Playing! ";
			$this->message_send_bulk($msg, $mno);
			$insert_array=array(
							'number'=>$mno,
							'message'=>$msg
							);
			$this->db->insert('sms_sent',$insert_array);

	   
	  		  $subj = "Registration Successful ForMaharashtra Chess Association";
				$emailmsg = "You Have Done Successfully Registration For Maharashtra Chess Association";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
				$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			$mail->Host = "mail.maharashtrachess.org"; // specify main and backup server
	        $mail->Port = 587; // set the port to use
	        $mail->SMTPAuth = true; // turn on SMTP authentication
	        $mail->Username = "admin@maharashtrachess.org"; // your SMTP username or your gmail username
	        $mail->Password = "admin@123"; // your SMTP password or your gmail password
	        
	        $from = "admin@maharashtrachess.org"; // Reply to this email
			$name = "Maharashtra Chess Association"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Maharashtra Chess Association"; // Name to indicate where the email came from when the recepient received
			
			
  			
   				$mail->AddAddress ( $email, $name );
				$mail->AddReplyTo ( "admin@maharashtrachess.org", "Maharashtra Chess Association" );
				//$mail->AddCC("support@urjaacademy.com","Admin");
				$mail->IsHTML ( true ); // send as HTML 
				//print_r($this->input->post('message'));exit;
				$mail->Subject =  $subj;
				$mail->Body = $emailinvoice;
		      $mail->addAttachment($path); 
			

      $mail->Send();

		$Path = './assets/pdfs/Idcard.pdf';
		unlink($Path);
				//$mail->Send();
				$insert_array11=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array11);
			

		   	$this->load->view('web/success');
			//$this->load->view('footer');
	    	}
    	}
	}
}

