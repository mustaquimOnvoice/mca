<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {
	public function index()
	{
	    //print_r($_POST);exit;
	    if(isset($_POST['btn_contact']))
	    {
	        $full_name=$this->input->post('full_name');
	        $email=$this->input->post('email');
	        $phone=$this->input->post('phone');
	        $message=$this->input->post('message');
	        $insert_array=array(
	            'name'=>$full_name,
	            'email'=>$email,
	            'phone'=>$phone,
	            'message'=>$message
	            );
	            if($this->db->insert('tbl_contact',$insert_array))
	            {
	                redirect(base_url().'contact_us/thank_you');
	            }
	    }
		$this->load->view('web/contact_us');
	}
	public function thank_you()
	{
	    $this->load->view('web/thank_you');
	}
}
