<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class user_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	// getting all Item values
	function GetAllItemValues($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		 $this->db->order_by('id', 'DESC');
		$querys = $this->db->get ();
		return $querys->result_array ();
	}

		function get_users($select = '*', $id = '', $searchText = '', $page='', $segment='')
    {
        $this->db->select($select);
        $this->db->from('wwc_admin');
        if(!empty($searchText)) {
            $likeCriteria = "(username  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
                            OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('id'=>$id));
		}
        $this->db->where('status !=', 2);
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }

}
?>