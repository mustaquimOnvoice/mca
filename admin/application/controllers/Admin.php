<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
//include (APPPATH."/core/class.phpmailer.php");
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("user_models");
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	
	
	
	
	
	//-- Anuron ERP--//
	
	public function dashboard()
	{		

		$this->renderView('Admin/dashboard');
	}

	
	public function user_list()
		{			
            $select	 = array('*');
			$where = array();
			$pagedata['delete_link'] = 'Adminitems/delete_item';
			$category ='';
			//$imei ='';
			if($this->session->userdata('category')){
				
				$this->session->userdata('category');
			}
        
          //Pagination Start
			$config = array();
	        $config["base_url"] = site_url() . "/Admin/user_list";
	        $config["total_rows"] = $this->base_models->get_count('player_id','tbl_players', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('tbl_players', $where,'player_id',$config["per_page"], $page);   
	      //  print_r( $pagedata['results']);die;
           	//Pagination End

           //	$imei = (@$imei) ? $imei : '';
           	$scategory = (@$category) ? $category : null;
            @strcmp($category,$scategory);
			//$pagedata['select']=array('status'=>$status,'imei'=>$imei);  
			$pagedata['select']=array('category'=>$category);  
	        $this->renderView('Admin/User/user_list',$pagedata);
		}


		// with ci pagination in php
		public function user_list_sess()
		{

           // print_r($_POST);
			$select	 = array('contact_name', 'contact_mail', 'myip', 'created_by');
			$where = array();
			$pagedata['delete_link'] = 'Adminitems/delete_item';
			$category ='';
			//$imei ='';
			//Filter Process
		
           if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
           {
           		/*$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0])).' 00:00:00';
				$todate = date('Y-m-d', strtotime($ranges[1])).' 23:59:00';*/
           	  	$fdate = (@$this->input->post('fromdate')) ? $this->input->post('fromdate') : '';
           	  	$todate = (@$this->input->post('todate')) ? $this->input->post('todate') : '';

           	  /*	$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
           		$imei = trim($imei_no);*/
              $where = array();
               if($this->input->post('fromdate') != '' && $this->input->post('todate') != ''){
               			$final_start_date = date('Y-m-d', strtotime($fdate . ' -1 day'));
               			$final_end_date = date('Y-m-d', strtotime($todate . ' +1 day'));
               			//$this->session->set_userdata(array("fromdate"=>$fdate,"todate"=>$todate));
						$where = "created_by BETWEEN '$final_start_date' AND '$final_end_date'";
					}

           	    $category = (@$this->input->post('category')!= null) ? $this->input->post('category') : '';
              // $array_items = $this->session->set_userdata(array("imei"=>$imei,"status"=>$status));
               $array_items = $this->session->set_userdata(array("category"=>$category));
               /* if($imei !=''){
                	$filter =  array('imei'=> $imei);
                	$where = @array_merge($where,$filter);	
                }*/  
                if($category !=''){
               		$filter =  array('category'=>$category);
                	$where = @array_merge($where,$filter);	
                } 
             
           }else{
 				/*if($this->session->userdata('imei') != NULL){
					$imei = $this->session->userdata('imei'); 
					$filter =  array('imei'=> $imei);
					$where = array_merge($where,$filter);
 				} */
 				if($this->session->userdata('category') != NULL){
					$category = $this->session->userdata('category'); 
					$filter =  array('category'=>$category);
					$where = array_merge($where,$filter);
 				}
           	}
           		/*echo '<pre>';
			print_r($where); die();*/
           	 if(@$_POST['submit']=='createxls')
           	  {

           	  //	$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei');
				//$where .= array('status'=> '1');
				$data['data'] = $this->user_models->GetAllItemValues('tbl_players', $where, $select);
		     /* echo '<pre>';
		       print_r($data['data']);*/
				//Export xls
					$this->generate_user_excel($data['data']);			
					

              }
			//End Filter Process
        
          //Pagination Start
			$config = array();
	        $config["base_url"] = site_url() . "/Admin/user_list_sess";
	        $config["total_rows"] = $this->base_models->get_count('player_id','tbl_players', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('tbl_players', $where,'player_id',$config["per_page"], $page);     
           	//Pagination End
	      // echo  $this->db->last_query(); die;
           //	$imei = (@$imei) ? $imei : '';

           	$scategory = (@$category) ? $category : null;
            @strcmp($category,$scategory);
	      // $pagedata['select']=array('status'=>$status,'imei'=>$imei);  
	       $pagedata['select']=array('category'=>$category);  
	        $this->renderView('Admin/User/user_list',$pagedata);
		}	




//New function
		//generate to excel	
	public function generate_user_excel($param1){
		// create file name
		$fileName = 'UserList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Eamil');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'IP');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Submitted On');

		// set Row
		$rowCount = 2;
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['contact_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['contact_mail']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['myip']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, date('d-M-Y', strtotime($element['created_by'])));
		
				
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}	

	public function edit_users()
	{
		$id = base64_decode($_GET['id']); 
			$select	 = array('user_id','name','mno','email','dob','category','state','city','pincode','country','gender','blood_group','tshirt_size','school_name','class','form_filler_name','medical_issue','form_filler_name','relation_with_runner','name_of_guardian','inserted_on');
		$where = array('user_id'=> $id);
		$pagedata['data']=$this->user_models->GetAllItemValues('tbl_user', $where, $select);
		$this->renderView('Admin/User/edit_user',$pagedata);
	}

	public function update_users()
	{
		if (isset($_POST)) 
		{
			$user_id = $this->input->post('user_id');
			$update_array	=	array(
										'name'  				=> $this->input->post('name'),
										'mno'   				=> $this->input->post('mno'),
										'email'   				=> $this->input->post('email'),
										'dob'   				=> date("Y-m-d", strtotime($this->input->post('dob'))),
										'category'     			=> $this->input->post('category'),
										'state'   				=> $this->input->post('state'),
										'city'   				=> $this->input->post('city'),
										'pincode'   			=> $this->input->post('pincode'),
										'country'   			=> $this->input->post('country'),
										'gender'   				=> $this->input->post('gender'),
										'blood_group'   		=> $this->input->post('blood_group'),
										'tshirt_size'   		=> $this->input->post('tshirt_size'),
										'school_name'   		=> $this->input->post('school_name'),
										'class'   				=> $this->input->post('class'),
										'medical_issue'   		=> $this->input->post('medical_issue'),
										'form_filler_name'   	=> $this->input->post('form_filler_name'),
										'relation_with_runner'  => $this->input->post('relation_with_runner'),
										'name_of_guardian'   	=> $this->input->post('name_of_guardian')
									
									);
			$where_array	=	 array('user_id'=>$user_id);

			if($this->base_models->update_records('tbl_user',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
		}
			redirect(base_url('Admin/edit_users/?id='.base64_encode($user_id)));	
	}	
		
public function view_users()
	{
		   $id = $_POST['userid'];
			$select	 = array('user_id','name','mno','email','dob','category','state','city','pincode','country','gender','blood_group','tshirt_size','school_name','class','form_filler_name','medical_issue','form_filler_name','relation_with_runner','name_of_guardian','inserted_on','bib_id');
		$where = array('user_id'=> $id);
		$data=$this->user_models->GetAllItemValues('tbl_user', $where, $select);

		echo '<div class="row">
        <div class="col-sm-12">
        <div class="row">
				  <div class="form-group col-sm-4">
					<label for="name" class="control-label">Name</label>';
					echo '<input type="text" class="form-control" id="name" name="name" placeholder="Name" value="'. $data[0]['name'].'"  data-error="Name is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="mno" class="control-label">Mobile  Number</label>';
					echo '<input type="text" class="form-control" id="mno" name="mno" placeholder="Mobile  Number" value="'.$data[0]['mno'].'" data-error="Mobile  Number is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="email" class="control-label">Email</label>';
					echo '<input type="text" class="form-control" id="email" name="email" placeholder="Upload Date" value="'.$data[0]['email'].'" data-error="Email is required"  required>
					<div class="help-block with-errors"></div>
				  </div>
				 
			  </div>
			    <div class="row">
			     <div class="form-group col-sm-4">
					<label for="dob" class="control-label">DOB</label>
						 <input type="date" class="form-control" id="dob" name="dob" placeholder="DOB" value="'.$data[0]['dob'].'" data-error="Birthdate is required"  required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="Category" class="control-label">Category</label>
					<input type="text" class="form-control" id="category" name="category" placeholder="Category" value="'.$data[0]['category'].'" data-error="Category is required" required>
					<input type="hidden"   name="user_id"  value="'.$data[0]['user_id'].'" >
					<div class="help-block with-errors"></div>
					<div id="exist_error"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="State" class="control-label">State</label>
					<input type="text" class="form-control" id="state" name="state" placeholder="State" value="'.$data[0]['state'].'" data-error="State is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				 

			</div>
			 <div class="row">
			  <div class="form-group col-sm-4">
					<label for="City" class="control-label">City</label>
					<input type="text" class="form-control" id="city" name="city" placeholder="City" value="'.$data[0]['city'].'" data-error="City is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="Pincode" class="control-label">Pincode</label>
					<input type="text" class="form-control" id="pincode" name="pincode" placeholder="Pincode" value="'.$data[0]['pincode'].'" data-error="Pincode is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="Country" class="control-label">Country</label>
					<input type="text" class="form-control" id="country" name="country" placeholder="Country" value="'.$data[0]['country'].'" data-error="Country is required" required>
					<div class="help-block with-errors"></div>
					<div id="exist_error"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="Gender" class="control-label">Gender</label>
					<input type="text" class="form-control" id="gender" name="gender" placeholder="Gender" value="'.$data[0]['gender'].'" data-error="Gender is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="blood_group" class="control-label">Blood Group</label>
					<input type="text" class="form-control" id="blood_group" name="blood_group" placeholder="Blood Group" value="'.$data[0]['blood_group'].'" data-error="Blood Group is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="tshirt_size" class="control-label">T Shirt Size</label>
					<input type="text" class="form-control" id="tshirt_size" name="tshirt_size" placeholder="T Shirt Size" value="'.$data[0]['tshirt_size'].'" data-error="T Shirt Size is required" required>
					<div class="help-block with-errors"></div>
				  </div>

			</div>
				 <div class="row">
			 
				  <div class="form-group col-sm-4">
					<label for="Gender" class="control-label">Gender</label>
					<input type="text" class="form-control" id="gender" name="gender" placeholder="Gender" value="'.$data[0]['gender'].'" data-error="Gender is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="blood_group" class="control-label">Blood Group</label>
					<input type="text" class="form-control" id="blood_group" name="blood_group" placeholder="Blood Group" value="'.$data[0]['blood_group'].'" data-error="Blood Group is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="tshirt_size" class="control-label">T Shirt Size</label>
					<input type="text" class="form-control" id="tshirt_size" name="tshirt_size" placeholder="T Shirt Size" value="'.$data[0]['tshirt_size'].'" data-error="T Shirt Size is required" required>
					<div class="help-block with-errors"></div>
				  </div>

			</div>
			 <div class="row">
				  <div class="form-group col-sm-4">
					<label for="school_name" class="control-label">School Name</label>
					<input type="text" class="form-control" id="school_name" name="school_name" placeholder="School Name" value="'.$data[0]['school_name'].'" data-error="School Name is Required" required>
					<div class="help-block with-errors"></div>
					<div id="exist_error"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="class" class="control-label">Standard</label>
					<input type="text" class="form-control" id="class" name="class" placeholder="Standard" value="'.$data[0]['class'].'" data-error="Standard is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="medical_issue" class="control-label">Medical Issue</label>
					<input type="text" class="form-control" id="medical_issue" name="medical_issue" placeholder="Medical Issue" value="'.$data[0]['medical_issue'].'" data-error="Medical Issue is required">
					<div class="help-block with-errors"></div>
				  </div>
				 
			</div>
			<div class="row">
			 <div class="form-group col-sm-4">
					<label for="form_filler_name" class="control-label">Name Of Form Filling</label>
					<input type="text" class="form-control" id="form_filler_name" name="form_filler_name" placeholder="Name Of Form Filling" value="'.$data[0]['form_filler_name'].'" data-error="Name Of Form Filling is Required" required>
					<div class="help-block with-errors"></div>
				  </div>

				  <div class="form-group col-sm-4">
					<label for="relation_with_runner" class="control-label">Relation With Runner</label>
					<input type="text" class="form-control" id="relation_with_runner" name="relation_with_runner" placeholder="Relation With Runner" value="'.$data[0]['relation_with_runner'].' "data-error="Relation With Runner is required" required>
					<div class="help-block with-errors"></div>
					<div id="exist_error"></div>
				  </div>
				  <div class="form-group col-sm-4">
					<label for="name_of_guardian" class="control-label">Name Of Guardian</label>
					<input type="text" class="form-control" id="name_of_guardian" name="name_of_guardian" placeholder="Name Of Guardian" value="'.$data[0]['name_of_guardian'].'" data-error="Name Of Guardian is required" required>
					<div class="help-block with-errors"></div>
				  </div>
			 	 <div class="form-group col-sm-4">
					<label for="name_of_guardian" class="control-label">BIB ID</label>
					<input type="text" class="form-control" id="name_of_guardian" name="name_of_guardian" placeholder="Name Of Guardian" value="'.$data[0]['bib_id'].'" readonly="readonly" data-error="Name Of Guardian is required" required>
					<div class="help-block with-errors"></div>
				  </div>

			</div>
			</div>
      </div>';
	}
  // For Notification SMS
	public function send_sms_form(){
			$pagedata['data']=$this->db->query("select mno,name from tbl_user")->result_array();
		   $this->renderView('Admin/User/send_sms',$pagedata);
	}

	public function push_sms()
	{		
		if($_POST['users'][0] == 0) { // if selected all then fetch all users for db
			$select = array('mno');			
			$nums = array_values(array_unique(array_column($this->base_models->get_records('tbl_user',$select,FALSE),'mno')));
		}else{
			$nums = array_values(array_unique($_POST['users']));			
		}
		
		
		$subj = trim($this->input->post('subject'));
		$message = trim($this->input->post('message'));
		$msg = "$message";
		$this->message_send_bulk($msg, $nums);
		
		$numsStr = implode(',',$nums);
		$insert_array=array(
						'number'=>$numsStr,
						'message'=>$msg
						);
		if($this->base_models->add_records('sms_sent',$insert_array)){
			$this->session->set_flashdata('success','SMS Sent successfully');
		}else{
			$this->session->set_flashdata('error','SMS Not Send');
		}
		redirect(base_url('admin/send_sms_form'));
	}
  // End For Notification SMS

	// Start For Notification Email
	public function send_email_form(){
			$pagedata['data']=$this->db->query("select email,name from tbl_user")->result_array();
		   $this->renderView('Admin/User/send_email',$pagedata);
	}

	public function push_email()
	{		
		
		if($_POST['users'][0] == '0') { // if selected all then fetch all users for db
			$select = array('email');			
			$emails = array_values(array_unique(array_column($this->base_models->get_records('tbl_user',$select,FALSE),'email')));
		}else{
			
			$emails = array_values(array_unique($_POST['users']));			
		}
	
		//$val = implode(',',$emails);
		$subj = trim($this->input->post('subject'));
		$message = trim($this->input->post('message'));
		$msg = "$message";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
				$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			  $mail->Host = "mail.deltainfomedia.com"; // specify main and backup server
        $mail->Port = 25; // set the port to use
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
        
        $from = "support@nagarcycling.com"; // Reply to this email
			$name = "Cyclothon"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Cyclothon"; // Name to indicate where the email came from when the recepient received
			
			
  			 foreach ($emails as $key => $value) {
   				$mail->AddAddress ( $value, $name );
				$mail->AddReplyTo ( $from, "Cyclothon" );
				$mail->IsHTML ( true ); // send as HTML 
				//print_r($this->input->post('message'));exit;
				$mail->Subject = $this->input->post('subject');
				$mail->Body = '<html><head><title>';
			/*	$mail->Body = $this->input->post('subject');*/
				$mail->Body .= '</title></head><body>';
				$mail->Body .= '<p style="color:#202124;font-size:18px;">';
				$mail->Body .= $this->input->post('message');
				$mail->Body .= '</p>';
				$mail->Body .= '</body></html>';
				$mail->Send();
				$insert_array=array(
					'email'=>$value,
					'subject'=> $this->input->post('subject'),
					'message'=>$this->input->post('message')
					);
				if($this->base_models->add_records('email_sent',$insert_array)){
					
					$this->session->set_flashdata('success','Email Sent successfully');
				}else{
					
					$this->session->set_flashdata('error','Email Not Send');
				}
			}

		 redirect(base_url('admin/send_email_form'));
	}
	 // End For Notification Email
	//END KIds Marathon --//
	
	// public function dashboard()
	// {		
		// $pagedata['total_users'] = $this->base_models->count_users();
		// $pagedata['total_clients'] = $this->base_models->count_clients();
		// $pagedata['total_pending_adv'] = $this->base_models->count_adv('0');
		// $pagedata['total_rejected_adv'] = $this->base_models->count_adv('1');
		// $pagedata['total_approved_adv'] = $this->base_models->count_adv('2');
		// $pagedata['total_publish_adv'] = $this->base_models->count_adv('3');
		// $pagedata['total_pending_invoice'] = $this->base_models->count_invoice('2');
		// $pagedata['total_approved_invoice'] = $this->base_models->count_invoice('1');
		
		// $this->renderView('Admin/dashboard',$pagedata);
	// }
	
	//---------- Business cat ------------//
	public function business_cat()
	{
		$status_str = 'Business Category';
		$where_array = array('status =' => '1');
		$data['data'] = $this->base_models->get_records('business_cat',array('id','bname'),$where_array);
		$pagedata = array('data'=>$data['data'],'delete_link'=>'Admin/delete_business_cat', 'title' => $status_str);
		$this->renderView('Admin/business_cat',$pagedata);
	}
	
	public function add_business(){
		$this->form_validation->set_rules('bname', 'Business name', 'trim|required');
		if($this->form_validation->run())
		{
			$insert_array=array(
						'bname'=>$this->input->post('bname'),
						'user_id'=>$this->session->userdata('user_type'),
						'added_on'=>date("Y-m-d H:i:s")
						);
			if($this->base_models->add_records('business_cat',$insert_array)){
				$this->session->set_flashdata('success','Added successfully');
			}else{
				$this->session->set_flashdata('error','Not added Please try again');
			}
		}
		redirect(base_url('Admin/business_cat'));
	}
	
	public function edit_business_cat()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('business_cat', array('id' => $id), array('id','bname'));
		$this->renderView('Admin/edit-business-cat',$pagedata);
	}
	
	public function update_business_cat()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('admin/business_cat')); 
		}		
		$this->form_validation->set_rules('bname', 'Business name', 'trim|required');	
		$error='';
			if($this->form_validation->run())
			{				
				$update_array=array(
						'bname'=>$this->input->post('bname')
					);
				$where_array = array('id'=>$id);
				if($this->base_models->update_records('business_cat',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not Edited Please try again');
				}
			}
				redirect(site_url('admin/edit_business_cat/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function delete_business_cat()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2'
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('business_cat',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- End Business cat--//
	
	//---------- User ------------//
	public function users()
	{		
		$pagedata['results'] = $this->base_models->get_users();
		$pagedata['delete_link'] = 'Admin/delete_user';
		$this->renderView('Admin/users',$pagedata);
	}
	
	public function add_user()
	{
		$this->renderView('Admin/add-user');
	}
	
	public function insert_user()
	{
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            if(!empty($_FILES['image']['name'])){
                $config['upload_path'] = 'uploads/admin/users/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
				}else{
                    $imageerrors = $this->upload->display_errors();
					$this->form_validation->set_message('image', $imageerrors);					
                }
			}
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('inputPassword')),
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						'profile_pic'=>$data['file_name'],
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('wwc_admin',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(base_url('admin/users'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Admin/add-user');
	}
	
	public function edit_user()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->get_users('',$id);
		$this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function update_user()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('admin/users')); 
		}
		
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>$password,
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(base_url('admin/edit_user/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_user()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- End User ------------//
	
public function add_file()
	{
		$pagedata['category_details']=$this->db->get('tbl_category')->result_array();
		$this->load->view('Admin/add-file',$pagedata);
	}
	
public function upload_file()
	{
		if(isset($_POST['submit']))
		{
			
			$file=$_FILES['file']['name'];
			$category_id=$_POST['category_id'];
			$upload_date=@date('d-m-Y');
			$status=0;
			
			$file=$file.time();
			$string = str_replace(" ","-", $file);
			// echo $string;
			// die();
			$data['file_name']=$string;
			$data['category_id']=$category_id;
			$data['upload_date']=$upload_date;
			$data['status']=$status;
			
			move_uploaded_file($_FILES['file']['tmp_name'],'uploads/'.$string.'.zip');
			$this->db->insert('tbl_uploads',$data);
			$this->session->set_flashdata('success', 'File Upload Successfully..!!');
			redirect('Admin/add_file/');
		}
	}

public function view_file()
	{
		$pagedata['file_details']=$this->db->query('select * from tbl_uploads as tu JOIN tbl_category as tc on tu.category_id=tc.id order by tu.file_id desc')->result_array();
		$this->load->view('Admin/view-file',$pagedata);
	}	

public function edit_file($file_id)
	{
		$pagedata['file_details']=$this->db->query("select * from tbl_uploads as tu JOIN tbl_category as tc on tu.category_id=tc.id where tu.file_id='$file_id'")->result_array();
		$pagedata['category_details']=$this->db->get('tbl_category')->result_array();
		$this->load->view('Admin/edit-file',$pagedata);
	}	

public function update_file()
	{
		if(isset($_POST['submit']))
		{
			
			$file_id=$_POST['file_id'];
			$old_file=$_POST['old_file'];
			$category_id=$_POST['category_id'];

			if($_FILES['file']['name']!='')
				{
					$file=$_FILES['file']['name'];
					$file=$file.time();
					$string = str_replace(" ","-", $file);
					move_uploaded_file($_FILES['file']['tmp_name'],'uploads/'.$string.'.zip');
					unlink($base_url.'uploads/'.$old_file.'.zip');
				}
			else
				{
					$file=$old_file;
				}
			
			if($_POST['status']!='')
				{
					$status=$_POST['status'];
				}
			else
				{
					$status=$_POST['old_status'];
				}
				
			$data['file_name']=$file;
			$data['category_id']=$category_id;
			$data['status']=$status;
			
			$this->db->where('file_id',$file_id);
			$this->db->update('tbl_uploads',$data);
			$this->session->set_flashdata('success', 'File Update Successfully..!!');
			redirect('Admin/view_file/');
		}
	}
	
public function delete_file($old_file,$file_id)
	{
		
		$this->db->where('file_id',$file_id);
		$this->db->delete('tbl_uploads');
		unlink($base_url.'uploads/'.$old_file.'.zip');
		$this->session->set_flashdata('delete', 'File Delete Successfully..!!');
		redirect('Admin/view_file/');
	}

// public function profile()
	// {
		// $user_id=$this->session->userdata('user_id');
		// $this->db->where('user_id',$user_id);
		// $pagedata['profile_details']=$this->db->get('tbl_users')->result_array();
		// $this->load->view('Admin/profile',$pagedata);
	// }
	
// public function edit_profile()
	// {
		// if(isset($_POST['submit']))
		  // {
			// $user_id=$_POST['user_id'];
			// $name=$_POST['name'];
			// $mno=$_POST['mno'];
			// $email=$_POST['email'];
			// $address=$_POST['address'];
			
			// $data['name']=$name;
			// $data['mno']=$mno;
			// $data['email']=$email;
			// $data['address']=$address;
		
			// $this->db->where('user_id',$user_id);
			// $this->db->update('tbl_users',$data);
			// $this->session->set_flashdata('success', 'Profile Update Successfully..!!');
			// redirect('Admin/profile/');
		  // }
			
	// }

// public function change_password()
	// {
		// if(isset($_POST['submit']))
		  // {
				// //User Id Take From Sesssion
				// $users_id=$_POST['users_id'];
				// $old_password=$_POST['old_password'];
				// $password1=$_POST['old-password'];
				
				// $new_password=$_POST['new_password'];
				// $confirm_password=$_POST['confirm_password'];
				
				// $data['password']=$new_password;
				
				// $this->db->where('user_id',$users_id);
				// $this->db->update('tbl_users',$data);
				// $this->session->set_flashdata('success', 'Password Change Successfully!!!');
				// redirect('Admin/profile/');

				
		  // }
	// }

	 	
		
}
?>
