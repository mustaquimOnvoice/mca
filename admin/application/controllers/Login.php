<?php
//User_type : 1=superAdmin, 2=Admin, 3=ND, 4=Distributor, 5=Retailer, 6=Client
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Login extends CI_Controller 
{
public function __construct()
	{
		date_default_timezone_set('Asia/Kolkata');
		parent::__construct();
		//	$this->load->library('session');
		$this->load->database();
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	
	public function check_login()
	{
		if(isset($_POST['submit']))
			{	
				// $this->db->where('username',trim($this->input->post('username')));
				// $this->db->where('password',md5($this->input->post('password')));
				// $this->db->where('status','1');	
			/*	print_r($_POST); die;*/
				$where = array('username'=>trim($this->input->post('username')),'password'=>md5($this->input->post('password')));
				
				$get_admin_details_qry = $this->db->get_where('wwc_admin',$where);	
				$cnt = $get_admin_details_qry->num_rows();
				$get_admin_details_res = $get_admin_details_qry->row_array();
		/*		if($cnt == 0){
					//National Distributor
					$get_admin_details_qry = $this->db->get_where('ndistributor',$where);
					$cnt = $get_admin_details_qry->num_rows();
					if($cnt >= 1){
						$get_admin_details_res = $get_admin_details_qry->row_array();
						$get_admin_details_res['id'] = $get_admin_details_res['nd_id'];
						$get_admin_details_res['code'] = $get_admin_details_res['nd_code'];
					}else{
						//Distributor
						$get_admin_details_qry = $this->db->get_where('distributor',$where);
						$cnt = $get_admin_details_qry->num_rows();
						if($cnt >= 1){
							$get_admin_details_res = $get_admin_details_qry->row_array();
							$get_admin_details_res['id'] = $get_admin_details_res['d_id'];
							$get_admin_details_res['code'] = $get_admin_details_res['d_code'];
						}else{
							//Retailer
							$get_admin_details_qry = $this->db->get_where('retailer',$where);
							$cnt = $get_admin_details_qry->num_rows();
							if($cnt >= 1){
								$get_admin_details_res = $get_admin_details_qry->row_array();
								$get_admin_details_res['id'] = $get_admin_details_res['rt_id'];
								$get_admin_details_res['code'] = $get_admin_details_res['rt_code'];
							}
						}
					}
				}*/				
				// echo'<pre>';
				// print_r($get_admin_details_res);
				// die();
				
				if($cnt >= 1){
					if($get_admin_details_res['status'] == 1){
						$newdata = array(
										'id' => $get_admin_details_res['id'],
										'code' => @$get_admin_details_res['code'],
										'user_fname' => $get_admin_details_res['fname'],
										'user_mname' => $get_admin_details_res['mname'],
										'user_lname' => $get_admin_details_res['lname'],
										'user_username' => $get_admin_details_res['username'],
										'user_email' => $get_admin_details_res['email'],
										'user_contact' => $get_admin_details_res['contact'],
										'user_profile' => $get_admin_details_res['profile_pic'],
										'state' => @$get_admin_details_res['state'],
										'city' => @$get_admin_details_res['city'],
										'user_type' => $get_admin_details_res['type'],
										'user_status' => $get_admin_details_res['status']
										);
						switch($newdata['user_type']){
							case 1 :
								$newdata['login_type'] =  'Admin';
								break;
							/*case 3 :
								$newdata['login_type'] =  'National_distributor';
								$newdata['user_firname'] =  $get_admin_details_res['firmname'];
								break;
							case 4 :
								$newdata['login_type'] =  'Distributor';
								$newdata['user_firname'] =  $get_admin_details_res['firmname'];
								break;
							case 5 :
								$newdata['login_type'] =  'Retailer';
								$newdata['user_firname'] =  $get_admin_details_res['firmname'];
								break;*/
						}						
						$this->session->set_userdata($newdata);	
						redirect($newdata['login_type'].'/user_list/');
					}else{
						$this->session->set_flashdata('suspend', 'You Account Is Suspended...!');
						redirect('Login');
					}
				}else{
					$this->session->set_flashdata('error', 'Enter Correct Username & Password...!');
					redirect('Login');
				}						
						
			}
			
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_flashdata('success', 'You Have Successfully Logout!!');
		redirect('login');
	}
}
?>
