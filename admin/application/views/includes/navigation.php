 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Preloader --
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
 Preloader	
<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div> -->
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse slimscrollsidebar">
		<ul class="nav" id="side-menu">
			<?php if($this->session->userdata('login_type') == 'Admin'){?>
				<li><a href="<?php echo site_url('admin/user_list/');?>">Players List</a></li>
			<!-- 	<li><a href="<?php echo site_url('admin/contact_list/');?>">Contact Us List</a></li>
				<li><a href="<?php echo site_url('admin/enquiry_list/');?>">Enquiry List</a></li> -->
			<!--	<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Send Notification<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php echo site_url('admin/send_email_form/');?>">Send Email</a> </li>
						<li> <a href="<?php echo site_url('admin/send_sms_form/');?>">Send SMS</a> </li>
					
					</ul>
				</li>-->
				<!--<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Item<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Adminitems/uploads_items/');?>">Upload Stock</a> </li>
						<li> <a href="<?php echo site_url('Adminitems/add_items/');?>">Add Item</a> </li>
						<li> <a href="<?php echo site_url('Adminitems/item_list/');?>">Items List</a> </li>
					<li> <a href="<?php // echo site_url('Adminitems/pagination_item_list/');?>">Pagination Items List</a> </li>-->
				<!--	</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Sale<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php echo site_url('Adminsales/sale_items/');?>">Sale Items</a> </li>
						<li> <a href="<?php echo site_url('Adminsales/sale_list/');?>">Sales List to ND</a> </li>
						<li> <a href="<?php echo site_url('Adminsales/sale_return/');?>">Sale Return</a> </li>
						<li> <a href="<?php echo site_url('Adminsales/faulty_return_list/');?>">Confirm DOA</a> </li>
						<li> <a href="<?php echo site_url('Adminsales/faulty_return_list_of_admin/');?>">DOA List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Reports<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php //echo site_url('Adminsales/sale_items/');?>">Sale Items</a> </li>
						<li> <a href="<?php// echo site_url('admin/Admin/users/');?>">Search IMEI</a> </li>

						 <li> <a href="<?php echo site_url('Adminreport/report_list/');?>">Report List</a> </li>
						 <li> <a href="<?php //echo site_url('Adminreport/sales_detail_report/');?>">Search IMEI</a> </li> -->
					<!--	<li> <a href="<?php echo site_url('Adminreport/sales_detail_report_history/');?>">Search IMEI Tracking</a> </li>
						 <li> <a href="<?php //echo site_url('Adminreport/bulk_imei/');?>">Bulk IMEI Search</a> </li> -->
					<!--	<li> <a href="<?php echo site_url('Adminstock/admin/');?>">Stock</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Products<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Products/add_product/');?>">Add Product</a> </li>
						<li> <a href="<?php echo site_url('Products/product_list/');?>">Products List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Schemes<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Products/add_scheme/');?>">Add Scheme</a> </li>
						<li> <a href="<?php echo site_url('Products/scheme_list/');?>">Scheme List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">RT Visit<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Rtvisit/rt_visit_list/');?>">Visit List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Backup<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Admin/backup');?>">Database</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'National_distributor'){?>
				<li><a href="<?php echo site_url('National_distributor/dashboard/');?>">Dashboard</a></li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Master<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Ndmaster/add_d/');?>">Add Distributor</a> </li>
						<li> <a href="<?php echo site_url('Ndmaster/distributor/');?>">Distributor List</a> </li>
						<li> <a href="<?php echo site_url('Ndmaster/retailer_list/');?>">Retailer List</a> </li>
					</ul>
				</li> -->
				<!--<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Stock<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php //echo site_url('Ndstock/stock_list/');?>">National Distributor</a> </li>
						<li> <a href="<?php //echo site_url('Ndsales/sale_list/');?>">Distributor's</a> </li>
						<li> <a href="<?php //echo site_url('Ndstock/rt_stock/');?>">Retailer's</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Sales<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php echo site_url('Ndsales/sale_to_d/');?>">Sales To Distributer</a> </li>	
						<li> <a href="<?php  echo site_url('Ndsales/sale_list/');?>">Sale List</a> </li>
						<li> <a href="<?php echo site_url('Ndsales/sale_return/');?>">Sale Return</a> </li>
						<li> <a href="<?php echo site_url('Ndsales/faulty_return_list/');?>">Confirm DOA</a> </li>
						<li> <a href="<?php echo site_url('Ndsales/faulty_return_list_of_nd/');?>">DOA List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Reports<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php //echo site_url('Adminsales/sale_items/');?>">Sale Items</a> </li>
						<li> <a href="<?php// echo site_url('admin/Admin/users/');?>">Search IMEI</a> </li> 
						<li> <a href="<?php echo site_url('Ndreport/report_list/');?>">Report List</a> </li>
						<li> <a href="<?php // echo site_url('Ndreport/sales_detail_report/');?>">Search IMEI</a> </li> 
						<li> <a href="<?php echo site_url('Ndreport/sales_detail_report_history/');?>">Search IMEI Tracking</a> </li>
						<li> <a href="<?php echo site_url('Ndstock/nd/');?>">Stock</a> </li>
					</ul>
				</li>
			<?php }?>-->
		
		</ul>
	</div>
</div>
<!-- Left navbar-header end -->