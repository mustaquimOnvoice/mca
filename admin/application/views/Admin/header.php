 
<nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> 
			<a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="<?php echo site_url('Admin/dashboard/');?>"><span class="hidden-xs"><img src="<?php echo base_url();?>assets/plugins/images/logo/letsup.png" alt="home" height="50px" alt="home" /></span></a></div>
                
                <ul class="nav navbar-top-links navbar-right pull-right">
                   <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void();"> <img src="<?php echo base_url();?>assets/plugins/images/users/dummy-profile.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $this->session->userdata('user_name');?></b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="<?php echo site_url('Admin/profile/');?>"><i class="ti-user"></i> My Profile</a></li>
							<li role="separator" class="divider"></li>
                            <li><a href="<?php echo site_url('login/logout/');?>"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
					<li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            
        </nav>