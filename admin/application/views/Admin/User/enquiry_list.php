 <title>Enquiry list</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- Page plugins css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<!-- Color picker plugins css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
<!-- Date picker plugins css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Select2 css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Popup CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
<link href="<?php echo base_url();?>assets/css/common/listing.css"   rel="stylesheet">
<script>
  var tblexport = true;
  console.log('tblexport: '+tblexport);
</script>
<!-- Listing CSS -->
<link href="<?php echo base_url();?>assets/css/common/listing.css"   rel="stylesheet">
</head>

<body>
<!-- Preloader -->
<!-- <div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div> -->
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Enquiry List</h4>
        </div>
        <!--<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Table</a></li>
            <li class="active">Data Table</li>
          </ol>
        </div>-->
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->

  <div class="row">
    <div class="white-box col-sm-12">
      <form method="post" name="date-form" id="date-form" action="<?php echo site_url('Admin/enquiry_list_sess'); ?>" data-toggle="validator">       
         <div class="col-sm-2">
                  <input class="form-control input-datepicker" type="date" name="fromdate" value="<?php echo set_value('fromdate', $this->session->userdata('fromdate')); ?>"/>
          </div>
          <div class="col-sm-2">
                  <input class="form-control input-datepicker" type="date" name="todate" value="<?php echo set_value('todate', $this->session->userdata('todate')); ?>"/>
              </div>
      <!--   <div class="col-sm-2">
            <input type="text" class="form-control" id="imei" name="imei" placeholder="IMEI" value="<?php// @$imei = $select['imei']; echo ($imei == '') ? '' : $imei ; ?>">
        </div> -->
        
        
           
        <div class="col-sm-2">
          <button type="submit" name="submit" value="filter" class="pull-left btn-xs btn-success">Search</button>       
          <button type="submit" name="submit" value="createxls" class="pull-left btn-xs btn-primary"><i class="fa fa-file-excel-o"></i> Export Excel</button>
        </div>
      </form>
     
    </div>
  </div>      
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <div class="row">
		<div class="col-sm-12">
          <div class="white-box">
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Enquiry Type</th>
                            <th>Name</th>
                            <th>Address </th>
                            <th>City </th>
                            <th>State Province</th>
                            <th>Email</th>
                            <th>Company Name</th>
                            <th>Organisation</th>
                         	<th>Message</th>
                         	<th>Date</th>
                        </tr>
                    </thead>
                 
                    <tbody>
                        <?php $i = 1;
							if(count($results)>0){
								foreach($results as $row){ ?>
									<tr>
                 		<td><?php echo $i;?></td>
										<td><?php echo $row['enquiry_type'];?></td>
										<td><?php echo $row['fullName'];?></td>
										<td><?php echo $row['address'];?></td>
										<td><?php echo $row['city'];?></td>
										<td><?php echo $row['stateProvince'];?></td>
										<td><?php echo $row['email'];?></td>
										<td><?php echo $row['cname'];?></td>
										<td><?php echo $row['organisation'];?></td>
										<td><?php echo $row['message'];?></td>
										<td><?php echo date("d-M-Y", strtotime($row['created_by']));?></td>
                    
								
                  <!-- 	<td>-->
                        
                         <!--  <a style="display:inline-block;color:#000;" id="trash" title="view"   onclick="return checkuser(<?php  echo $row['id'];?>);" data-id ="<?php  echo $row['id'];?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a>-->
                       <!--  <a style="display:inline-block;color:#000;" title="view" href="<?php // echo site_url(); ?>/Admin/view_users?id=<?php  //echo base64_encode($row['user_id']); ?>" ><i class="fa fa-eye" aria-hidden="true"></i></a> -->
										<!--	<a style="display:inline-block;color:#000;" title="Edit" href="<?php  echo site_url(); ?>/Admin/edit_users?id=<?php  echo base64_encode($row['id']); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>-->
											
									<!--	</td> -->
									</tr>
						<?php $i++; } } ?>                    
                    </tbody>
                    <tfoot>
                      <tr>
                          <td colspan="11" class="text-left">
                            <span class="pagination"><?php echo $links; ?></span>
                          </td>                        
                      </tr>
                    </tfoot>                     
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>

<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!-- Magnific popup JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- Custom Select2 JavaScript -->
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/switchery/dist/switchery.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
<!-- Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/moment/moment.js"></script>
<!-- Clock Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- Color Picker Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
jQuery(document).ready(function() {
    // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
    // For select 2

    $(".select2").select2();
    $('.selectpicker').selectpicker();
     
     //Bootstrap-TouchSpin
              $(".vertical-spin").TouchSpin({
                verticalbuttons: true,
                verticalupclass: 'ti-plus',
                verticaldownclass: 'ti-minus'
            });
            var vspinTrue = $(".vertical-spin").TouchSpin({
                verticalbuttons: true
            });
            if (vspinTrue) {
                $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
            }
    
            $("input[name='tch1']").TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%'
            });
            $("input[name='tch2']").TouchSpin({
                min: -1000000000,
                max: 1000000000,
                stepinterval: 50,
                maxboostedstep: 10000000,
                prefix: '$'
            });
            $("input[name='tch3']").TouchSpin();
           
            $("input[name='tch3_22']").TouchSpin({
                initval: 40
            });
    
            $("input[name='tch5']").TouchSpin({
                prefix: "pre",
                postfix: "post"
            });
           
      // For multiselect

      $('#pre-selected-options').multiSelect();      
      $('#optgroup').multiSelect({ selectableOptgroup: true });

      $('#public-methods').multiSelect();
      $('#select-all').click(function(){
        $('#public-methods').multiSelect('select_all');
        return false;
      });
      $('#deselect-all').click(function(){
        $('#public-methods').multiSelect('deselect_all');
        return false;
      });
      $('#refresh').on('click', function(){
      $('#public-methods').multiSelect('refresh');
        return false;
      });
      $('#add-option').on('click', function(){
        $('#public-methods').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
        return false;
      });
              
 });
 
// Clock pickers
$('#single-input').clockpicker({
  placement: 'bottom',
  align: 'left',
  autoclose: true,
  'default': 'now'

});

$('.clockpicker').clockpicker({
    donetext: 'Done',
    
})
  .find('input').change(function(){
    console.log(this.value);
});

$('#check-minutes').click(function(e){
  // Have to stop propagation here
  e.stopPropagation();
  input.clockpicker('show')
      .clockpicker('toggleView', 'minutes');
});
if (/mobile/i.test(navigator.userAgent)) {
  $('input').prop('readOnly', true);
}
// Colorpicker

$(".colorpicker").asColorPicker();
$(".complex-colorpicker").asColorPicker({
    mode: 'complex'
});
$(".gradient-colorpicker").asColorPicker({
    mode: 'gradient'
});
// Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
      });
      
    jQuery('#date-range').datepicker({
        toggleActive: true
      });
    jQuery('#datepicker-inline').datepicker({
        
        todayHighlight: true
      });

// Daterange picker

$('.input-daterange-datepicker').daterangepicker({
  buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-danger',
    cancelClass: 'btn-inverse'
});
$('.input-daterange-timepicker').daterangepicker({
  timePicker: true,
  format: 'MM/DD/YYYY h:mm A',
  timePickerIncrement: 30,
  timePicker12Hour: true,
  timePickerSeconds: false,
  buttonClasses: ['btn', 'btn-sm'],
  applyClass: 'btn-danger',
  cancelClass: 'btn-inverse'
});
$('.input-limit-datepicker').daterangepicker({
  format: 'MM/DD/YYYY',
  minDate: '01/01/2019',
  maxDate: '02/01/2019',
  buttonClasses: ['btn', 'btn-sm'],
  applyClass: 'btn-danger',
  cancelClass: 'btn-inverse',
  dateLimit: {
    days: 6
  }
});
</script>
<script type="text/javascript">
 /*$('#trash').on('click', function(){
    //get cover id
    var id=$(this).attr("data-id");
    alert(id);
  $.ajax({
       type: "POST",
       url: "<?php //echo base_url()?>index.php/admin/view_users",
       data: {'userid':+id},
       success: function(result)
       {
         console.log(result);
          $("#responseBody").html(result);
           $("#myModal").modal('show');
       } 
    });
});*/
   function checkuser(id){
    $.ajax({
       type: "POST",
       url: "<?php echo base_url()?>index.php/admin/view_users",
       data: {'userid':+id},
       success: function(result)
       {
         console.log(result);
          $("#responseBody").html(result);
           $("#myModal").modal('show');
       } 
    });
   }

</script>
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
     
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">view User</h4>
        </div>
        <div class="modal-body">
           <div id="responseBody"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/common/listing.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

</body>
</html>
