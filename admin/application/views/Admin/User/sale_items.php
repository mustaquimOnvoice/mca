 <title>Add user</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
<?php //die('s');?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Add User</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <!-- .row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <form method="post" name="add-form" id="add-form" action="<?php echo site_url(); ?>/admin/insert_user" enctype="multipart/form-data" data-toggle="validator" >
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="fname" class="control-label">First Name</label>
					<input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" data-error="First Name is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="mname" class="control-label">Middle Name</label>
					<input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" data-error="Middle Name is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="lname" class="control-label">Last Name</label>
					<input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" data-error="Last Name is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="username" class="control-label">Username</label>
					<input type="text" class="form-control" id="username" name="username" placeholder="User Name" data-error="User Name is required" required>
					<div class="help-block with-errors"></div>
				  </div>
			  </div>
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="email" class="control-label">Email</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="Email" data-error="Bruh, that email address is invalid" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="contact" class="control-label">Mobile Number</label>
					<input type="number" class="form-control" id="contact" name="contact" placeholder="Mobile No." data-error="Mobile Number required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="type" class="control-label">User Type</label>
					<select class="form-control" id="type" name="type" data-error="User type required" required>
						<option value="">Select</option>
						<option value="1">Admin</option>
						<!--<option value="2">Marketing Head</option>-->
						<option value="3">Marketing</option>
						<option value="4">Accounts</option>
						<option value="5">Operation</option>
						<!--<option value="6">Analyzer</option>-->
					</select>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="status" class="control-label">Login Status</label>
					<div class="radio">
					  <input type="radio" name="status" id="out" value="1" required>
					  <label for="out"> Active </label>
					</div>
					<div class="radio">
					  <input type="radio" name="status" id="in" value="0" required>
					  <label for="in"> Inactive </label>
					</div>
				  </div>
              </div>
              <div class="form-group col-sm-6">
                <label for="inputPassword" class="control-label">Password</label>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <input type="password" data-toggle="validator" data-minlength="6" name="inputPassword" class="form-control" id="inputPassword" placeholder="Password" required>
                    <span class="help-block">Minimum of 6 characters</span> </div>
                  <div class="form-group col-sm-6">
                    <input type="password" class="form-control" name="inputPasswordConfirm" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm Password" required>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
              </div>
			  <div class="form-group col-sm-6">
                <label class="col-sm-12">Profile Pic</label>
                <div class="col-sm-12">
                	<input type="file" class="form-control" id="image" name="image">
					<div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="form-group">
				<div class="row">
                  <div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->      
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/admin/users.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
