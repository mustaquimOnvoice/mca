 <title>Edit User</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<!-- <div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div> -->
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
<?php //die('s');?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Edit User</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <!-- .row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <form method="post" name="edit-form" id="edit-form" action="<?php echo site_url(); ?>/Admin/update_users" enctype="multipart/form-data" data-toggle="validator" >
            	<?php  foreach($data as $row){?>
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="name" class="control-label">Name</label>
					<input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $row['name'];?>" data-error="Name is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="mno" class="control-label">Mobile  Number</label>
					<input type="text" class="form-control" id="mno" name="mno" placeholder="Mobile  Number" value="<?php echo $row['mno'];?>" data-error="Mobile  Number is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="email" class="control-label">Email</label>
					<input type="text" class="form-control" id="email" name="email" placeholder="Upload Date" value="<?php echo $row['email'];?>" data-error="Email is required"  required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="dob" class="control-label">DOB</label>
						<input type="date" class="form-control" id="dob" onchange="return countage();" name="dob" id="dob" placeholder="DOB" value="<?php echo $row['dob'];?>" data-error="Birthdate is required"  required>
						<p class="min_age_err" style="display:none;color:red;"><b>Age Should Be Atlist 4..!</b></p>
						<p class="max_age_err" style="display:none;color:red;"><b>Age Limit Is 12..!</b></p>
					<div class="help-block with-errors"></div>
				  </div>
			  </div>
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="Category" class="control-label">Category</label>
					<input type="text" class="form-control" id="category" name="category" placeholder="Category" value="<?php echo $row['category'];?>" data-error="Category is required" required>
					<input type="hidden"   name="user_id"  value="<?php echo $row['user_id'];?>" >
					<div class="help-block with-errors"></div>
					<div id="exist_error"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="State" class="control-label">State</label>
					<input type="text" class="form-control" id="state" name="state" placeholder="State" value="<?php echo $row['state'];?>" data-error="State is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="City" class="control-label">City</label>
					<input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo $row['city'];?>" data-error="City is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="Pincode" class="control-label">Pincode</label>
					<input type="text" class="form-control" id="pincode" name="pincode" placeholder="Pincode" value="<?php echo $row['pincode'];?>" data-error="Pincode is required" required>
					<div class="help-block with-errors"></div>
				  </div>

			</div>
			 <div class="row">
				  <div class="form-group col-sm-3">
					<label for="Country" class="control-label">Country</label>
					<input type="text" class="form-control" id="country" name="country" placeholder="Country" value="<?php echo $row['country'];?>" data-error="Country is required" required>
					<div class="help-block with-errors"></div>
					<div id="exist_error"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="Gender" class="control-label">Gender</label>
					<input type="text" class="form-control" id="gender" name="gender" placeholder="Gender" value="<?php echo $row['gender'];?>" data-error="Gender is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="blood_group" class="control-label">Blood Group</label>
					<input type="text" class="form-control" id="blood_group" name="blood_group" placeholder="Blood Group" value="<?php echo $row['blood_group'];?>" data-error="Blood Group is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="tshirt_size" class="control-label">T Shirt Size</label>
					<input type="text" class="form-control" id="tshirt_size" name="tshirt_size" placeholder="T Shirt Size" value="<?php echo $row['tshirt_size'];?>" data-error="T Shirt Size is required" required>
					<div class="help-block with-errors"></div>
				  </div>

			</div>
			 <div class="row">
				  <div class="form-group col-sm-3">
					<label for="school_name" class="control-label">School Name</label>
					<input type="text" class="form-control" id="school_name" name="school_name" placeholder="School Name" value="<?php echo $row['school_name'];?>" data-error="School Name is Required" required>
					<div class="help-block with-errors"></div>
					<div id="exist_error"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="class" class="control-label">Standard</label>
					<input type="text" class="form-control" id="class" name="class" placeholder="Standard" value="<?php echo $row['class'];?>" data-error="Standard is required" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="medical_issue" class="control-label">Medical Issue</label>
					<input type="text" class="form-control" id="medical_issue" name="medical_issue" placeholder="Medical Issue" value="<?php echo $row['medical_issue'];?>" data-error="Medical Issue is required">
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="form_filler_name" class="control-label">Name Of Form Filling</label>
					<input type="text" class="form-control" id="form_filler_name" name="form_filler_name" placeholder="Name Of Form Filling" value="<?php echo $row['form_filler_name'];?>" data-error="Name Of Form Filling is Required" required>
					<div class="help-block with-errors"></div>
				  </div>

			</div>
			<div class="row">
				  <div class="form-group col-sm-3">
					<label for="relation_with_runner" class="control-label">Relation With Runner</label>
					<input type="text" class="form-control" id="relation_with_runner" name="relation_with_runner" placeholder="Relation With Runner" value="<?php echo $row['relation_with_runner'];?>" data-error="Relation With Runner is required" required>
					<div class="help-block with-errors"></div>
					<div id="exist_error"></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="name_of_guardian" class="control-label">Name Of Guardian</label>
					<input type="text" class="form-control" id="name_of_guardian" name="name_of_guardian" placeholder="Name Of Guardian" value="<?php echo $row['name_of_guardian'];?>" data-error="Name Of Guardian is required" required>
					<div class="help-block with-errors"></div>
				  </div>
			

			</div>
				<?php } ?>
             <div class="form-group">
				<div class="row">
                  <div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->      
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
	var d = new Date();
var year = d.getFullYear() - 18;
d.setFullYear(year);

$("#dob").datepicker({ dateFormat: "dd/mm/yy",
		    changeMonth: true,
		    changeYear: true,
		    maxDate: year,
		    minDate: "-100Y",
            yearRange: '-100:' + year + '',
            defaultDate: d
		 });

function countage(){
        var dob = $("#dob").val();

        var birthdate = new Date(dob); // 
		var cur = new Date("2019/09/17");
		var diff = cur-birthdate; // This is the difference in milliseconds
		var age = Math.floor(diff/31557600000); // Divide by 1000*60*60*24*365.25
     
        if(age <=3){
       		 $('.min_age_err').show();
        }
        else if(age >= 12){
        	$('.max_age_err').show();
        }
        else if(age>=4 && age<=6 ){
        	document.getElementById("category").value = "1Km";
        	 $('.min_age_err').hide();
        	 $('.max_age_err').hide();
        }
          else if(age>=7  && age<=9 ){
        	document.getElementById("category").value = "3Km";
        	 $('.min_age_err').hide();
        	 $('.max_age_err').hide();
        }
          else if(age>=10  && age<=12 ){
        	document.getElementById("category").value = "5Km";
        	 $('.min_age_err').hide();
        	 $('.max_age_err').hide();
        }
}
</script>>
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/admin/item.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
