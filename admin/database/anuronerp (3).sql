-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2019 at 12:58 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anuronerp`
--

-- --------------------------------------------------------

--
-- Table structure for table `asm`
--

CREATE TABLE `asm` (
  `asm_id` int(11) NOT NULL,
  `asm_code` text,
  `nd_id` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `pan_no` varchar(25) DEFAULT NULL,
  `gst_no` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `ip_address` text,
  `device_token` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asm`
--

INSERT INTO `asm` (`asm_id`, `asm_code`, `nd_id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `contact`, `profile_pic`, `pan_no`, `gst_no`, `status`, `ip_address`, `device_token`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 'ASM1', 1, 'asm1', NULL, NULL, 'ASM1', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Triggers `asm`
--
DELIMITER $$
CREATE TRIGGER `ASM code` BEFORE INSERT ON `asm` FOR EACH ROW SET NEW.asm_code = CONCAT("ASM", COALESCE((SELECT MAX(asm_id)+1 from asm), 1)),
NEW.username = CONCAT("ASM", COALESCE((SELECT MAX(asm_id)+1 from asm), 1))
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `c_id` int(11) NOT NULL,
  `c_code` text,
  `d_code` text NOT NULL,
  `rt_id` int(11) NOT NULL DEFAULT '0',
  `tsm_id` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `pan_no` varchar(25) DEFAULT NULL,
  `gst_no` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `ip_address` text,
  `device_token` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`c_id`, `c_code`, `d_code`, `rt_id`, `tsm_id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `contact`, `profile_pic`, `pan_no`, `gst_no`, `status`, `ip_address`, `device_token`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 'C1', 'D1', 0, 0, NULL, NULL, NULL, 'C1', '', NULL, '9175917762', NULL, NULL, NULL, 1, NULL, NULL, '2019-04-27 00:00:00', '2019-04-27 11:58:28', '0000-00-00 00:00:00');

--
-- Triggers `client`
--
DELIMITER $$
CREATE TRIGGER `C Code` BEFORE INSERT ON `client` FOR EACH ROW SET NEW.c_code = CONCAT("C", COALESCE((SELECT MAX(c_id)+1 from client), 1)),
NEW.username = CONCAT("C", COALESCE((SELECT MAX(c_id)+1 from client), 1))
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `distributor`
--

CREATE TABLE `distributor` (
  `d_id` int(11) NOT NULL,
  `d_code` text,
  `nd_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `email2` varchar(25) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `contact2` varchar(25) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '4' COMMENT '4=Distributor',
  `pan_no` varchar(25) DEFAULT NULL,
  `gst_no` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `state` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `acnt_name` varchar(25) DEFAULT NULL,
  `acnt_email` varchar(25) DEFAULT NULL,
  `acnt_contact` varchar(25) DEFAULT NULL,
  `ip_address` text,
  `device_token` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distributor`
--

INSERT INTO `distributor` (`d_id`, `d_code`, `nd_id`, `admin_id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `email2`, `contact`, `contact2`, `profile_pic`, `type`, `pan_no`, `gst_no`, `status`, `state`, `city`, `address`, `acnt_name`, `acnt_email`, `acnt_contact`, `ip_address`, `device_token`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 'D1', 1, 0, 'Demo Dist1', '', 'ghgj', 'D1', 'e10adc3949ba59abbe56e057f20f883e', 'fh2gm@gm.com', '', '8585858585', '', NULL, 4, '74pop88888', 'uidom8989', 1, 'MH', 'Pune', 'ldodj kslsj lsjkskk', '', '', '', NULL, NULL, '0000-00-00 00:00:00', '2019-05-03 16:06:16', '0000-00-00 00:00:00'),
(2, 'D2', 5, 0, 'dist23', 'Ddd', 'asd', 'D2', 'e10adc3949ba59abbe56e057f20f883e', 'soham1@gmail.com', '', '7878787878', '', NULL, 4, '45445454', '22121', 1, 'MH', 'City', '606 vimangar, pune', '', '', '', NULL, NULL, '2019-05-03 12:07:02', '2019-05-03 16:19:34', '2019-05-03 16:20:03');

--
-- Triggers `distributor`
--
DELIMITER $$
CREATE TRIGGER `Distributor code` BEFORE INSERT ON `distributor` FOR EACH ROW SET NEW.d_code = CONCAT("D", COALESCE((SELECT MAX(d_id)+1 from distributor), 1)),
NEW.username = CONCAT("D", COALESCE((SELECT MAX(d_id)+1 from distributor), 1))
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ndistributor`
--

CREATE TABLE `ndistributor` (
  `nd_id` int(11) NOT NULL,
  `nd_code` text,
  `admin_id` int(11) NOT NULL,
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` text,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `email2` varchar(25) NOT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `contact2` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '3' COMMENT '3=NationalDistributor',
  `pan_no` varchar(25) DEFAULT NULL,
  `gst_no` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `ip_address` text,
  `state` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `acnt_name` varchar(25) NOT NULL,
  `acnt_email` varchar(25) NOT NULL,
  `acnt_contact` varchar(25) NOT NULL,
  `device_token` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ndistributor`
--

INSERT INTO `ndistributor` (`nd_id`, `nd_code`, `admin_id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `email2`, `contact`, `contact2`, `profile_pic`, `type`, `pan_no`, `gst_no`, `status`, `ip_address`, `state`, `city`, `address`, `acnt_name`, `acnt_email`, `acnt_contact`, `device_token`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 'ND1', 1, 'demo1', NULL, NULL, 'ND1', '', NULL, '', NULL, NULL, NULL, 3, NULL, NULL, 2, NULL, NULL, NULL, NULL, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-30 18:46:40'),
(2, 'ND2', 1, 'demo2', NULL, NULL, 'ND2', '', NULL, '', NULL, NULL, NULL, 3, NULL, NULL, 1, NULL, NULL, NULL, NULL, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'ND3', 1, 'Soham', 'Ddd', 'Bildcon', 'ND3', 'e10adc3949ba59abbe56e057f20f883e', 'soham@gmail.com', 'Soham1@Gmail.Com', '9175917762', '8888888888', '', 3, '55896sasd', 'asd 213', 1, NULL, 'MH', 'Pune', '606 vimangar, pune', 'Dfofko', 'acnt@Gmail.Com', '8888888888', NULL, '2019-04-30 11:01:46', '2019-05-02 10:31:02', '2019-04-30 11:42:52');

--
-- Triggers `ndistributor`
--
DELIMITER $$
CREATE TRIGGER `ND code` BEFORE INSERT ON `ndistributor` FOR EACH ROW SET NEW.nd_code = CONCAT("ND", COALESCE((SELECT MAX(nd_id)+1 from ndistributor), 1)),
NEW.username = CONCAT("ND", COALESCE((SELECT MAX(nd_id)+1 from ndistributor), 1))
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `recent_login_user`
--

CREATE TABLE `recent_login_user` (
  `id` int(11) NOT NULL,
  `user_code` text NOT NULL,
  `ip_address` text NOT NULL,
  `date_time` datetime NOT NULL,
  `session_id` text NOT NULL,
  `type` enum('1','2') NOT NULL COMMENT '1= login, 2= logout'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recent_login_user`
--

INSERT INTO `recent_login_user` (`id`, `user_code`, `ip_address`, `date_time`, `session_id`, `type`) VALUES
(1, 'ND1', '', '2019-04-28 00:00:00', '', '1'),
(2, 'ND1', '', '2019-04-28 00:00:00', '', '2'),
(3, 'ND1', '', '2019-04-29 00:00:00', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `retailer`
--

CREATE TABLE `retailer` (
  `rt_id` int(11) NOT NULL,
  `rt_code` text,
  `d_id` int(11) NOT NULL DEFAULT '0',
  `tsm_id` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `email2` varchar(20) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `contact2` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '5' COMMENT '5=Retailer',
  `pan_no` varchar(25) DEFAULT NULL,
  `gst_no` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `state` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `acnt_name` varchar(25) DEFAULT NULL,
  `acnt_email` varchar(25) DEFAULT NULL,
  `acnt_contact` varchar(25) DEFAULT NULL,
  `ip_address` int(11) DEFAULT NULL,
  `device_token` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retailer`
--

INSERT INTO `retailer` (`rt_id`, `rt_code`, `d_id`, `tsm_id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `email2`, `contact`, `contact2`, `profile_pic`, `type`, `pan_no`, `gst_no`, `status`, `state`, `city`, `address`, `acnt_name`, `acnt_email`, `acnt_contact`, `ip_address`, `device_token`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 'RT1', 2, 0, 'ret1', '', 'Bildcon', 'RT1', 'e10adc3949ba59abbe56e057f20f883e', 'ret1@gmail.com', '', '5566556655', '', NULL, 5, '45sasd', '78asdasd', 1, 'MH', 'Mumbai', 'sad asdfasd', '', '', '', NULL, NULL, '0000-00-00 00:00:00', '2019-05-03 16:11:44', '2019-05-03 16:19:11'),
(2, 'RT2', 2, 0, 'rtrter', '', 'ksisi', 'RT2', 'e10adc3949ba59abbe56e057f20f883e', 'rt@sa.com', '', '7575757575', '', NULL, 5, '45olfk5', 'jkuf99', 1, 'MH', 'Pune', 'kbi sovi ,pune', '', '', '', NULL, NULL, '2019-05-03 13:31:09', '2019-05-03 13:54:34', '2019-05-03 13:57:14');

--
-- Triggers `retailer`
--
DELIMITER $$
CREATE TRIGGER `Rt code` BEFORE INSERT ON `retailer` FOR EACH ROW SET NEW.rt_code = CONCAT("RT", COALESCE((SELECT MAX(rt_id)+1 from retailer), 1)),
NEW.username = CONCAT("RT", COALESCE((SELECT MAX(rt_id)+1 from retailer), 1))
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_items`
--

CREATE TABLE `tbl_items` (
  `item_id` int(11) NOT NULL,
  `company_code` text,
  `company_name` text,
  `upload_date` date DEFAULT NULL,
  `item_code` text,
  `item_name` text,
  `imei` text NOT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `item_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=Not sale,1=Sale',
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_items`
--

INSERT INTO `tbl_items` (`item_id`, `company_code`, `company_name`, `upload_date`, `item_code`, `item_name`, `imei`, `profile_pic`, `status`, `item_status`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 'Anuron', 'Anuron TV', '2019-04-26', 'HMLT32', '55 (HOMS4K-5500) SMART 4K LED TV SENSI Next Gen', 'H552SN11A553RI282552', NULL, 1, '1', '2019-04-26 00:00:00', '2019-04-27 12:41:44', '0000-00-00 00:00:00'),
(2, 'Anuron', 'Anuron TV', '2019-04-27', 'HMLT34', '34 (HOMS4K-5500) SMART 4K LED TV SENSI Next Gen', 'H552SN11A553RI282034', NULL, 1, '1', '2019-04-27 00:00:00', '2019-04-27 12:41:44', '0000-00-00 00:00:00'),
(3, 'Anuron', 'Anuron TV', '2019-04-01', 'HOMSLT55SI', '555 (HOMS4K-5500) SMART 4K LED TV SENSI Next Gen', 'H39B1PU1AA358SB274894', NULL, 1, '1', '2019-04-01 00:00:00', '2019-04-27 15:20:12', '0000-00-00 00:00:00');

--
-- Triggers `tbl_items`
--
DELIMITER $$
CREATE TRIGGER `Insert_New_Items_to_tbl_item_sales` AFTER INSERT ON `tbl_items` FOR EACH ROW INSERT INTO tbl_item_sales (item_id, item_code, imei, inserted_on) VALUES(
    	COALESCE((SELECT item_id from tbl_items WHERE item_id = NEW.item_id)),
    	COALESCE((SELECT item_code from tbl_items WHERE item_id = NEW.item_id)),    
    	COALESCE((SELECT imei from tbl_items WHERE item_id = NEW.item_id)),
    	COALESCE((SELECT inserted_on from tbl_items WHERE item_id = NEW.item_id))
	)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_sales`
--

CREATE TABLE `tbl_item_sales` (
  `is_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` text,
  `imei` text NOT NULL,
  `level_type` enum('0','1','2','3','4') NOT NULL DEFAULT '0' COMMENT '0=Admin, 1=ND, 2=D, 3=RT, 4=Client',
  `nd_id` int(11) DEFAULT '0',
  `nd_code` text,
  `nd_date` datetime DEFAULT NULL,
  `d_id` int(11) DEFAULT '0',
  `d_code` text,
  `d_date` datetime DEFAULT NULL,
  `rt_id` int(11) NOT NULL DEFAULT '0',
  `rt_code` text,
  `rt_date` datetime DEFAULT NULL,
  `c_id` int(11) NOT NULL DEFAULT '0',
  `c_code` text,
  `c_date` datetime DEFAULT NULL,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_item_sales`
--

INSERT INTO `tbl_item_sales` (`is_id`, `item_id`, `item_code`, `imei`, `level_type`, `nd_id`, `nd_code`, `nd_date`, `d_id`, `d_code`, `d_date`, `rt_id`, `rt_code`, `rt_date`, `c_id`, `c_code`, `c_date`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 1, 'HMLT32', 'H552SN11A553RI282552', '1', 0, '0', NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, '2019-04-26 00:00:00', '2019-04-27 12:41:44', '0000-00-00 00:00:00'),
(2, 2, 'HMLT34', 'H552SN11A553RI282034', '0', 0, '0', NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, '2019-04-27 00:00:00', '2019-04-27 12:41:44', '0000-00-00 00:00:00'),
(3, 3, 'HOMSLT55SI', 'H39B1PU1AA358SB274894', '2', 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, '2019-04-01 00:00:00', '2019-04-27 15:20:12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales_to_c`
--

CREATE TABLE `tbl_sales_to_c` (
  `stc_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` text,
  `imei` text NOT NULL,
  `c_id` int(11) DEFAULT '0',
  `c_code` text,
  `item_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=NotSale, 1=Sale',
  `upload_date` datetime DEFAULT NULL,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sales_to_c`
--

INSERT INTO `tbl_sales_to_c` (`stc_id`, `item_id`, `item_code`, `imei`, `c_id`, `c_code`, `item_status`, `upload_date`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 1, 'HOMSLT55SI', 'H39B1PU1AA358SB274894', 1, 'C1', '0', NULL, '2019-04-06 00:00:00', '2019-04-27 15:24:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales_to_d`
--

CREATE TABLE `tbl_sales_to_d` (
  `std_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` text,
  `imei` text NOT NULL,
  `d_id` int(11) DEFAULT '0',
  `d_code` text,
  `item_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=NotSale, 1=Sale',
  `upload_date` datetime DEFAULT NULL,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sales_to_d`
--

INSERT INTO `tbl_sales_to_d` (`std_id`, `item_id`, `item_code`, `imei`, `d_id`, `d_code`, `item_status`, `upload_date`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 1, 'HOMSLT55SI', 'H39B1PU1AA358SB274894', 1, 'D1', '1', NULL, '2019-04-04 00:00:00', '2019-04-27 15:22:56', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales_to_nd`
--

CREATE TABLE `tbl_sales_to_nd` (
  `stnd_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` text,
  `imei` text NOT NULL,
  `nd_id` int(11) DEFAULT '0',
  `nd_code` text,
  `item_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=NotSale, 1=Sale',
  `upload_date` datetime DEFAULT NULL,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sales_to_nd`
--

INSERT INTO `tbl_sales_to_nd` (`stnd_id`, `item_id`, `item_code`, `imei`, `nd_id`, `nd_code`, `item_status`, `upload_date`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 1, 'HOMSLT55SI', 'H39B1PU1AA358SB274894', 1, 'ND1', '0', NULL, '2019-04-02 00:00:00', '2019-04-27 15:21:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales_to_rt`
--

CREATE TABLE `tbl_sales_to_rt` (
  `strt_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` text,
  `imei` text NOT NULL,
  `rt_id` int(11) DEFAULT '0',
  `rt_code` text,
  `item_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=NotSale, 1=Sale',
  `upload_date` datetime DEFAULT NULL,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sales_to_rt`
--

INSERT INTO `tbl_sales_to_rt` (`strt_id`, `item_id`, `item_code`, `imei`, `rt_id`, `rt_code`, `item_status`, `upload_date`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 1, 'HOMSLT55SI', 'H39B1PU1AA358SB274894', 1, 'RT1', '1', NULL, '2019-04-05 00:00:00', '2019-04-27 15:24:14', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tsm`
--

CREATE TABLE `tsm` (
  `tsm_id` int(11) NOT NULL,
  `tsm_code` text,
  `nd_id` int(11) NOT NULL DEFAULT '0',
  `asm_id` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `pan_no` varchar(25) DEFAULT NULL,
  `gst_no` varchar(25) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `ip_address` text,
  `device_token` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tsm`
--

INSERT INTO `tsm` (`tsm_id`, `tsm_code`, `nd_id`, `asm_id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `contact`, `profile_pic`, `pan_no`, `gst_no`, `status`, `ip_address`, `device_token`, `inserted_on`, `updated_on`, `deleted_on`) VALUES
(1, 'TSM1', 1, 1, 'tsm01', NULL, NULL, 'TSM1', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'TSM2', 2, 1, 'tsam sa', NULL, NULL, 'TSM2', '', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Triggers `tsm`
--
DELIMITER $$
CREATE TRIGGER `TSM code` BEFORE INSERT ON `tsm` FOR EACH ROW SET NEW.tsm_code = CONCAT("TSM", COALESCE((SELECT MAX(tsm_id)+1 from tsm), 1)),
NEW.username = CONCAT("TSM", COALESCE((SELECT MAX(tsm_id)+1 from tsm), 1))
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `wwc_admin`
--

CREATE TABLE `wwc_admin` (
  `id` int(11) NOT NULL,
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1=SuperAdmin,2=Admin',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `ip_address` text,
  `inserted_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL,
  `recent_login` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wwc_admin`
--

INSERT INTO `wwc_admin` (`id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `contact`, `profile_pic`, `type`, `status`, `ip_address`, `inserted_on`, `updated_on`, `deleted_on`, `recent_login`) VALUES
(1, 'Narendra', '', 'Firodia', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'sadmin@gmail.com', '9175917762', 'letsup.png', 1, 1, NULL, '2018-11-26 00:00:00', '2019-01-02 12:19:52', '2019-04-29 11:25:22', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asm`
--
ALTER TABLE `asm`
  ADD PRIMARY KEY (`asm_id`),
  ADD KEY `nd_id` (`nd_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`c_id`),
  ADD KEY `Distributor` (`rt_id`);

--
-- Indexes for table `distributor`
--
ALTER TABLE `distributor`
  ADD PRIMARY KEY (`d_id`),
  ADD KEY `National Distributor` (`nd_id`);

--
-- Indexes for table `ndistributor`
--
ALTER TABLE `ndistributor`
  ADD PRIMARY KEY (`nd_id`),
  ADD KEY `admin` (`admin_id`);

--
-- Indexes for table `recent_login_user`
--
ALTER TABLE `recent_login_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retailer`
--
ALTER TABLE `retailer`
  ADD PRIMARY KEY (`rt_id`),
  ADD KEY `Distributor` (`d_id`);

--
-- Indexes for table `tbl_items`
--
ALTER TABLE `tbl_items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `tbl_item_sales`
--
ALTER TABLE `tbl_item_sales`
  ADD PRIMARY KEY (`is_id`);

--
-- Indexes for table `tbl_sales_to_c`
--
ALTER TABLE `tbl_sales_to_c`
  ADD PRIMARY KEY (`stc_id`);

--
-- Indexes for table `tbl_sales_to_d`
--
ALTER TABLE `tbl_sales_to_d`
  ADD PRIMARY KEY (`std_id`);

--
-- Indexes for table `tbl_sales_to_nd`
--
ALTER TABLE `tbl_sales_to_nd`
  ADD PRIMARY KEY (`stnd_id`);

--
-- Indexes for table `tbl_sales_to_rt`
--
ALTER TABLE `tbl_sales_to_rt`
  ADD PRIMARY KEY (`strt_id`);

--
-- Indexes for table `tsm`
--
ALTER TABLE `tsm`
  ADD PRIMARY KEY (`tsm_id`),
  ADD KEY `asm_id` (`asm_id`),
  ADD KEY `nd_id` (`nd_id`);

--
-- Indexes for table `wwc_admin`
--
ALTER TABLE `wwc_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asm`
--
ALTER TABLE `asm`
  MODIFY `asm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `distributor`
--
ALTER TABLE `distributor`
  MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ndistributor`
--
ALTER TABLE `ndistributor`
  MODIFY `nd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `recent_login_user`
--
ALTER TABLE `recent_login_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `retailer`
--
ALTER TABLE `retailer`
  MODIFY `rt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_items`
--
ALTER TABLE `tbl_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_item_sales`
--
ALTER TABLE `tbl_item_sales`
  MODIFY `is_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_sales_to_c`
--
ALTER TABLE `tbl_sales_to_c`
  MODIFY `stc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_sales_to_d`
--
ALTER TABLE `tbl_sales_to_d`
  MODIFY `std_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_sales_to_nd`
--
ALTER TABLE `tbl_sales_to_nd`
  MODIFY `stnd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_sales_to_rt`
--
ALTER TABLE `tbl_sales_to_rt`
  MODIFY `strt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tsm`
--
ALTER TABLE `tsm`
  MODIFY `tsm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wwc_admin`
--
ALTER TABLE `wwc_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `asm`
--
ALTER TABLE `asm`
  ADD CONSTRAINT `asm_ibfk_1` FOREIGN KEY (`nd_id`) REFERENCES `ndistributor` (`nd_id`);

--
-- Constraints for table `distributor`
--
ALTER TABLE `distributor`
  ADD CONSTRAINT `National Distributor` FOREIGN KEY (`nd_id`) REFERENCES `ndistributor` (`nd_id`);

--
-- Constraints for table `ndistributor`
--
ALTER TABLE `ndistributor`
  ADD CONSTRAINT `admin` FOREIGN KEY (`admin_id`) REFERENCES `wwc_admin` (`id`);

--
-- Constraints for table `retailer`
--
ALTER TABLE `retailer`
  ADD CONSTRAINT `Distributor` FOREIGN KEY (`d_id`) REFERENCES `distributor` (`d_id`);

--
-- Constraints for table `tsm`
--
ALTER TABLE `tsm`
  ADD CONSTRAINT `tsm_ibfk_1` FOREIGN KEY (`asm_id`) REFERENCES `asm` (`asm_id`),
  ADD CONSTRAINT `tsm_ibfk_2` FOREIGN KEY (`nd_id`) REFERENCES `ndistributor` (`nd_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
